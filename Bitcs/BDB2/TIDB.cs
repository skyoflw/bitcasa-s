﻿using BDll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDB2
{
    public static class TIDB
    {
        public static void InsertToDB(this TaskInfo f)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                db.TaskInfos.InsertOnSubmit(f);
                
                db.WriteEnd();
            }
        }
        public static void InsertToDB(this IEnumerable<TaskInfo> f)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();
                db.TaskInfos.InsertAllOnSubmit(f);
                db.SubmitChanges();
                db.WriteEnd();
            }
        }

        public static List<TaskInfo> ListAll()
        {
            List<TaskInfo> r = new List<TaskInfo>();
            using (var db = AppDataContext2.New)
            {
                db.ReadStart();
                var all = from TaskInfo cf in db.TaskInfos
                          select cf;
                r = all.ToList();
                db.ReadEnd();
            }
            return r;
        }
        public static List<TaskInfo> ListByType(TaskType type)
        {
            List<TaskInfo> r = new List<TaskInfo>();
            using (var db = AppDataContext2.New)
            {
                db.ReadStart();
                var all = from TaskInfo cf in db.TaskInfos
                          where cf.Type == type
                          select cf;
                r = all.ToList();
                db.ReadEnd();
            }
            return r;
        }
        public static List<TaskInfo> ListByStatus(BDll.TaskStatus status)
        {
            List<TaskInfo> r = new List<TaskInfo>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from TaskInfo cf in db.TaskInfos
                          where cf.Status == status
                          select cf;
                r = all.ToList();

                db.ReadEnd();
            }

            return r;
        }

        public static void UpdateToDB(this TaskInfo f)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();
                var all = from TaskInfo cf in db.TaskInfos
                          where cf.Id == f.Id
                          select cf;
                foreach (var cf in all)
                    cf.Update(f);
                db.WriteEnd();
            }
        }
        public static void UpdateToDB(this IEnumerable<TaskInfo> cfs)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var all = (from TaskInfo cf in db.CacheFilesTable
                           select cf).ToList();

                TaskInfo tmp = null;
                var dic = all.ToDictionary<TaskInfo, Guid>(z => z.Id);

                foreach (var cf in cfs)
                    if (dic.TryGetValue(cf.Id, out tmp))
                        tmp.Update(cf);
#if DEBUG
                    else throw new ArgumentException();
#endif

                db.WriteEnd();
            }
        }

        public static void DeleteFromDB(Guid taskId)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                db.TaskInfos.DeleteAllOnSubmit(
                    from TaskInfo ki in db.TaskInfos
                    where ki.Id == taskId
                    select ki);

                db.WriteEnd();

                db.WriteEnd();
            }
        }
        public static void DeleteFromDB(this IEnumerable<Guid> taskIds)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                db.TaskInfos.DeleteAllOnSubmit(
                    from TaskInfo ki in db.TaskInfos
                    from Guid taskId in taskIds
                    where ki.Id == taskId
                    select ki);

                db.WriteEnd();

                db.WriteEnd();
            }
        }
        public static void DeleteFromDB(this TaskInfo ti)
        {
            DeleteFromDB(ti.Id);
        }
        public static void DeleteFromDB(this IEnumerable<TaskInfo> tis)
        {
            DeleteFromDB(tis.Select<TaskInfo, Guid>(z => z.Id));
        }
    }
}
