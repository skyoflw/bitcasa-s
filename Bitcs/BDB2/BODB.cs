﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDll;

namespace BDB2
{
    /// <summary>
    /// BatchOpera 数据库操作辅助类
    /// </summary>
    public static class BODB
    {
        public static void InsertToDB(this BatchOpera bo)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                db.BatchOperas.InsertOnSubmit(bo);

                db.WriteEnd();
            }
        }
        public static void InsertToDB(this IEnumerable<BatchOpera> bos)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                db.BatchOperas.InsertAllOnSubmit(bos);

                db.WriteEnd();
            }
        }

        /// <summary>
        /// 列出所有 BatchOpera。
        /// 排序按照 CreateTime 升序（从老到新？）。
        /// </summary>
        public static List<BatchOpera> ListAll()
        {
            List<BatchOpera> result = null;

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                result = (from BatchOpera bo in db.BatchOperas
                          select bo)
                          .OrderBy(z => z.CreateTime)
                          .ToList();

                db.ReadEnd();
            }

            return result;
        }

        /// <summary>
        /// 从数据库中移除对象
        /// </summary>
        /// <param name="bo"></param>
        public static void DeleteFromDB(this BatchOpera bo)
        {
            DeleteFromDB(bo.Id);
        }
        /// <summary>
        /// 从数据库中移除对象
        /// </summary>
        /// <param name="bos"></param>
        public static void DeleteFromDB(this IEnumerable<BatchOpera> bos)
        {
            DeleteFromDB(bos.Select<BatchOpera, Guid>(z => z.Id));
        }
        /// <summary>
        /// 从数据库中移除对象
        /// </summary>
        /// <param name="boid"></param>
        public static void DeleteFromDB(Guid boid)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var j = from BatchOpera bo in db.BatchOperas
                        where bo.Id == boid
                        select bo;

                db.BatchOperas.DeleteAllOnSubmit(j);

                db.WriteEnd();
            }
        }
        /// <summary>
        /// 从数据库中移除对象
        /// </summary>
        /// <param name="boids"></param>
        public static void DeleteFromDB(IEnumerable<Guid> boids)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var j = from BatchOpera bo in db.BatchOperas
                        where boids.ToList().Contains(bo.Id)
                        select bo;
                
                db.BatchOperas.DeleteAllOnSubmit(j);

                db.WriteEnd();
            }
        }
    }
}
