﻿using BCache;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP;

namespace BDB2
{
    public static class CFDB
    {
        /// <summary>
        /// 以指定文件名创建或获取该实例。
        /// 在下载或打开文件前请调用此方法。
        /// 此方法会自动为 LinkCounter 加一。
        /// 此外，如果是新创建的，此实例的 Type 为 Temp，Status 为 Created。
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public static CacheFile GetOrCreate(string objId, string filename)
        {
            CacheFile result = null;

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                result = (from CacheFile cf in db.CacheFilesTable
                         where cf.ObjId == objId
                         select cf).FirstOrDefault();

                if (result == null)
                {
                    result = new CacheFile(objId);
                    result.Type = CacheType.Temp;
                    result.Status = CacheStatus.Created;
                    db.CacheFilesTable.InsertOnSubmit(result);
                }
                result.LinkCounter++;

                if (result.FilePath == null)
                    result.FilePath = result.GenerateFilePath(result.Type, filename);
                else if (result.FileName != filename)
                    if (result.MoveFile(result.Type, filename))
                        result.FilePath = result.GenerateFilePath(result.Type, filename);

                db.WriteEnd();
            }

            return result;
        }

        /// <summary>
        /// 适用于获取文件以便打开
        /// </summary>
        /// <param name="objId"></param>
        /// <returns></returns>
        public static CacheFile FromId(string objId)
        {
            List<CacheFile> r = new List<CacheFile>();
            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from CacheFile cf in db.CacheFilesTable
                          where cf.ObjId == objId
                          select cf;
                r = all.ToList();

                db.ReadEnd();
            }
            return r.FirstOrDefault();
        }
        /// <summary>
        /// 适用于获取文件以便打开
        /// </summary>
        /// <param name="objIds"></param>
        /// <returns></returns>
        public static Dictionary<string, CacheFile> FromId(IEnumerable<string> objIds)
        {
            var r = new Dictionary<string, CacheFile>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = (from CacheFile cf in db.CacheFilesTable
                           where objIds.ToList().Contains(cf.ObjId)
                           select cf).ToList();

                db.ReadEnd();

                r = all.ToDictionary<CacheFile, string>(z => z.ObjId);
            }

            return r;
        }

        /// <summary>
        /// 列出所有 Cache
        /// </summary>
        /// <returns></returns>
        public static List<CacheFile> ListAll()
        {
            List<CacheFile> r = new List<CacheFile>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from CacheFile cf in db.CacheFilesTable
                          select cf;
                r.AddRange(all);

                db.ReadEnd();
            }

            return r;
        }

        /// <summary>
        /// 将对象更新到数据库
        /// </summary>
        /// <param name="f"></param>
        public static void UpdateToDB(this CacheFile f)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var fdb = (from CacheFile cf in db.CacheFilesTable
                          where cf.Id == f.Id
                          select cf).FirstOrDefault();

#if DEBUG
                fdb.CheckNotNull();
#endif
                if (fdb != null) fdb.Update(f);

                db.WriteEnd();
            }
        }
        /// <summary>
        /// 将对象更新到数据库
        /// </summary>
        /// <param name="cfs"></param>
        public static void UpdateToDb(this IEnumerable<CacheFile> cfs)
        {
            Task.Run(() =>
                {
                    using (var db = AppDataContext2.New)
                    {
                        db.WriteStart();

                        var all = (from CacheFile cf in db.CacheFilesTable
                                   where cfs.Select(z => z.Id).Contains(cf.Id)
                                   select cf).ToList();

                        CacheFile tmp = null;
                        var dic = all.ToDictionary<CacheFile, Guid>(z => z.Id);

                        foreach (var cf in cfs)
                            if (dic.TryGetValue(cf.Id, out tmp))
                                tmp.Update(cf);
#if DEBUG
                            else throw new ArgumentException("对象不在数据库中");
#endif

                        db.WriteEnd();
                    }
                });
        }

        /// <summary>
        /// 返回值用于删除实体文件
        /// 如果返回 null，说明不需要删除
        /// </summary>
        /// <param name="cf"></param>
        /// <returns></returns>
        public static CacheFile DeleteFromDB(string objId)
        {
            CacheFile r = null;

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var all = (from CacheFile c in db.CacheFilesTable
                           where c.ObjId == objId
                           select c).ToList();

                all.ForEach(z => z.LinkCounter--);

                var del = all.Where(z => z.LinkCounter < 1);

                r = del.FirstOrDefault();

                db.CacheFilesTable.DeleteAllOnSubmit(del);

                db.WriteEnd();
            }

            return r;
        }
        /// <summary>
        /// 返回值指示是否需要删除实体文件
        /// </summary>
        /// <param name="objIds"></param>
        /// <returns></returns>
        public static List<CacheFile> DeleteFromDB(IEnumerable<string> objIds)
        {
            List<CacheFile> r = new List<CacheFile>();

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var all = (from CacheFile cf in db.CacheFilesTable
                           where objIds.ToList().Contains(cf.ObjId)
                           select cf).ToList();

                all.ForEach(z => z.LinkCounter--);

                r = all.Where(z => z.LinkCounter < 1).ToList();

                //CacheFile tmp = null;

                //var dic = all.ToDictionary<CacheFile, string>(z => z.ObjId);

                //foreach (var objId in objIds)
                //    if (dic.TryGetValue(objId, out tmp))
                //    {
                //        tmp.LinkCounter--;
                //        if (tmp.LinkCounter < 1) r.Add(tmp);
                //    }

                db.CacheFilesTable.DeleteAllOnSubmit(r);
                db.WriteEnd();
            }

            return r;
        }

        /// <summary>
        /// <para/>强制删除缓存
        /// <para/>返回值用于删除实体文件
        /// </summary>
        /// <param name="cf"></param>
        /// <returns></returns>
        public static CacheFile ForceDeleteFromDB(string objId)
        {
            CacheFile r = null;

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var all = (from CacheFile c in db.CacheFilesTable
                           where c.ObjId == objId
                           select c).ToList();

                r = all.FirstOrDefault();

                db.CacheFilesTable.DeleteAllOnSubmit(all);

                db.WriteEnd();
            }

            return r;
        }
        /// <summary>
        /// <para/>强制删除缓存
        /// <para/>返回值指示删除实体文件
        /// </summary>
        /// <param name="objIds"></param>
        /// <returns></returns>
        public static List<CacheFile> ForceDeleteFromDB(IEnumerable<string> objIds)
        {
            List<CacheFile> r = new List<CacheFile>();

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var all = (from CacheFile cf in db.CacheFilesTable
                           where objIds.ToList().Contains(cf.ObjId)
                           select cf).ToList();

                r = all.ToList();

                db.CacheFilesTable.DeleteAllOnSubmit(r);

                db.WriteEnd();
            }

            return r;
        }
    }
}
