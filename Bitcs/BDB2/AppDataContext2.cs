﻿using BCache;
using BDB2;
using BDll;
using BitcasaSDK_WP.BObj;
using System;
using System.Data.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BDB2
{
    public partial class AppDataContext2 : DataContext
    {
        private const string DATABASE_SOURCE = "Data Source=isostore:/BS2.sdf";

        private AppDataContext2()
            : base(DATABASE_SOURCE)
        {
        }

        public Table<BObject> ObjectsTable;
        public Table<CacheFile> CacheFilesTable;
        public Table<TaskInfo> TaskInfos;
        public Table<BatchOpera> BatchOperas;

        #region waiter

        private static ReaderWriterLockSlim RWLocker = 
            new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        public void ReadStart()
        {
            RWLocker.EnterReadLock();
        }

        public void ReadEnd()
        {
            RWLocker.ExitReadLock();
        }

        public void WriteStart()
        {
            RWLocker.EnterWriteLock();
        }

        public void WriteEnd()
        {
            this.SubmitChanges();
            RWLocker.ExitWriteLock();
        }

        #endregion

        public static AppDataContext2 New
        {
            get 
            { 
                var db = new AppDataContext2();
                if (!db.DatabaseExists())
                    db.CreateDatabase();
                return db;
            }
        }

        /// <summary>
        /// 重建数据库
        /// </summary>
        public static void Rebuild()
        {
            using (var db = New)
            {
                db.WriteStart();

                if (db.DatabaseExists())
                {
                    db.DeleteDatabase();
                    db.CreateDatabase();
                }

                db.WriteEnd();
            }
        }
    }
}
