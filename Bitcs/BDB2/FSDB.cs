﻿using BCache;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP;

namespace BDB2
{
    /// <summary>
    /// BFileSystem db helper
    /// </summary>
    public static class FSDB
    {
        public static void InsertToDB(this BFileSystem f, BObject parent)
        {
            f.From(parent);

            InsertBObject(f);
        }
        public static void InsertToDB(this IEnumerable<BFileSystem> f, BObject parent)
        {
            f.From(parent);

            InsertBObject(f);
        }
        public static void InsertToDB(this BRoot f)
        {
            InsertBObject(f);
        }
        public static void InsertToDB(this IEnumerable<BRoot> r)
        {
            InsertBObject(r);
        }

        private static void InsertBObject(this BObject obj)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();
                db.ObjectsTable.InsertOnSubmit(obj);
                db.SubmitChanges();
                db.WriteEnd();
            }
        }
        private static void InsertBObject(IEnumerable<BObject> objs)
        {
            using (var db = AppDataContext2.New)
            {
                db.WriteStart();
                db.ObjectsTable.InsertAllOnSubmit(objs);
                db.SubmitChanges();
                db.WriteEnd();
            }
        }
        
        public static List<BFileSystem> GetSubFileSystem(this BObject dir)
        {
#if DEBUG
            var fs = dir as BFileSystem;
            if (fs != null && fs.IsFile) throw new ArgumentException();
#endif
            return FromParent(dir.Path64);
        }

        public static BObject FromPath(string path64)
        {
            BObject r = null;

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from BFileSystem fs in db.ObjectsTable
                          where fs.Path64 == path64
                          select fs;
                r = all.FirstOrDefault();

                db.ReadEnd();
            }

            return r;
        }

        /// <summary>
        /// 获取对象的父目录人类可读路径
        /// </summary>
        /// <param name="fs"></param>
        /// <returns></returns>
        public static string GetParentHumanPath(this BFileSystem fs)
        {
            string parent = null;

            var p64s = fs.Path64.LevelPath64();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from BObject obj in db.ObjectsTable
                          where p64s.Contains(obj.Path64)
                          select obj;

                var r = all.ToList();

                r.Sort((a, b) => a.Path64.Length - b.Path64.Length);

                parent = String.Join("\\", r.Select(z => z.Name));

                db.ReadEnd();
            }

            return parent;
        }
        
        /// <summary>
        /// 根据目录的 Path64 获取该目录的子文件夹和文件列表
        /// </summary>
        /// <param name="parentPath64"></param>
        /// <returns></returns>
        public static List<BFileSystem> FromParent(string parentPath64)
        {
            List<BFileSystem> r = new List<BFileSystem>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from BFileSystem fs in db.ObjectsTable
                          where fs.ObjectType == BObject.ObjectTypeType.FileSystem &&
                                fs.Parent64 == parentPath64
                          select fs;
                r = all.ToList();

                db.ReadEnd();
            }

            return r;
        }
        /// <summary>
        /// 根据目录的 Path64 获取该目录，并返回子文件夹和文件列表
        /// </summary>
        /// <param name="parentPath64"></param>
        /// <param name="self"></param>
        /// <returns></returns>
        public static List<BFileSystem> FromParent(string parentPath64, out BObject self)
        {
            List<BFileSystem> r = new List<BFileSystem>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from BFileSystem fs in db.ObjectsTable
                          where fs.ObjectType == BObject.ObjectTypeType.FileSystem &&
                                fs.Parent64 == parentPath64
                          select fs;
                r = all.ToList();

                var selfs = from BObject p in db.ObjectsTable
                            where p.Path64 == parentPath64
                            select p;
                self = selfs.FirstOrDefault();

                db.ReadEnd();
            }

            return r;
        }

        /// <summary>
        /// 列出所有 Root 对象
        /// </summary>
        /// <returns></returns>
        public static List<BRoot> ListRoot()
        {
            List<BRoot> r = new List<BRoot>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                var all = from BRoot fs in db.ObjectsTable
                          where fs.ObjectType == BObject.ObjectTypeType.Root
                          select fs;
                r = all.ToList();

                db.ReadEnd();
            }

            return r;
        }

        /// <summary>
        /// 根据名字搜索对象。
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static List<BFileSystem> QueryByName(string name)
        {
            name = name.ToLower();
            List<BFileSystem> r = new List<BFileSystem>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                r = (from BFileSystem fs in db.ObjectsTable
                     where fs.ObjectType == BObject.ObjectTypeType.FileSystem &&
                           fs.Name.ToLower().Contains(name)
                     select fs).ToList();

                db.ReadEnd();
            }

            return r;
        }
        /// <summary>
        /// 根据名字搜索对象。
        /// 参数指示是否一定是文件，否则为仅目录
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isFile">
        /// 指定是否查询文件，否则查询目录，如果需要查询两者，请使用 QueryByName(string name)
        /// </param>
        /// <returns></returns>
        public static List<BFileSystem> QueryByName(string name, bool isFile)
        {
            name = name.ToLower();
            List<BFileSystem> r = new List<BFileSystem>();

            using (var db = AppDataContext2.New)
            {
                db.ReadStart();

                r = (from BFileSystem fs in db.ObjectsTable
                     where fs.ObjectType == BObject.ObjectTypeType.FileSystem &&
                           fs.Name.ToLower().Contains(name) &&
                           fs.IsFile == isFile
                     select fs).ToList();
                
                db.ReadEnd();
            }

            return r;
        }
        
        /// <summary>
        /// 将对象更新到数据库。
        /// 返回值为被更新了的 Id。
        /// </summary>
        /// <param name="obj"></param>
        public static string UpdateToDB(this BObject obj)
        {
            string oldId = null;

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var all = from BObject bo in db.ObjectsTable
                          where bo.Path64 == obj.Path64
                          select bo;

                var ins = all.First();

                if (ins != null)
                {
                    var zzz = ins as BFileSystem;

                    if (zzz != null)
                    {
                        var aaa = obj as BFileSystem;
                        if (aaa == null || aaa.Id != zzz.Id)
                            if (zzz.Id != null) oldId = zzz.Id;
                    }

                    ins.Update(obj);
                }                

                db.SubmitChanges();
                db.WriteEnd();
            }

            return oldId;
        }
        /// <summary>
        /// 将对象更新到数据库。
        /// 返回值为被更新了的 Id。
        /// </summary>
        /// <param name="objs"></param>
        public static List<string> UpdateToDB(this IEnumerable<BObject> objs)
        {
            List<string> oldId = new List<string>();

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                var all = from BObject r in db.ObjectsTable
                          where objs.Select(z => z.Path64).Contains(r.Path64)
                          select r;

                var dic = all.ToDictionary<BObject, string>(r => r.Path64);

                BObject ins = null;
                foreach (var r in objs)
                    if (dic.TryGetValue(r.Path64, out ins))
                    {
                        var zzz = ins as BFileSystem;
                        if (zzz != null)
                        {
                            var aaa = r as BFileSystem;
                            if (aaa == null || aaa.Id != zzz.Id)
                                if (zzz.Id != null) oldId.Add(zzz.Id);
                        }

                        ins.Update(r);
                    }
#if DEBUG
                    else throw new ArgumentException();
#endif

                db.SubmitChanges();
                db.WriteEnd();
            }

            return oldId;
        }

        /// <summary>
        /// 返回所有被删除的文件的 Id
        /// 返回值适用于删除对应缓存文件
        /// 如果对象为目录，返回 null
        /// </summary>
        /// <param name="fs"></param>
        /// <returns></returns>
        public static string DeleteFromDB(this BObject obj)
        {
            return DeleteFromDB(new BObject[] { obj }).FirstOrDefault();
        }
        /// <summary>
        /// 返回所有应当被删除的 CacheFile 的 ObjId
        /// 返回值适用于删除对应缓存文件
        /// </summary>
        /// <param name="fss"></param>
        /// <returns></returns>
        public static List<string> DeleteFromDB(this IEnumerable<BObject> fss)
        {
            return DeleteFromDB(fss.Select<BObject, string>(z => z.Path64));
        }
        /// <summary>
        /// 返回所有应当被删除的 CacheFile 的 ObjId
        /// 返回值适用于删除对应缓存文件
        /// </summary>
        /// <param name="path64s"></param>
        /// <returns></returns>
        public static List<string> DeleteFromDB(IEnumerable<string> path64s)
        {
            List<string> result = new List<string>();

            using (var db = AppDataContext2.New)
            {
                db.WriteStart();

                foreach (var oz in path64s)
                {
                    var fs = (from BObject k in db.ObjectsTable // k in sub
                              where k.Path64.StartsWith(oz + "/")
                              select k).ToList();

                    result.AddRange(fs.Select(obj => obj as BFileSystem)
                        .Where(obj => obj.IsFile)
                        .Select(obj => obj.Id));

                    db.ObjectsTable.DeleteAllOnSubmit(fs);
                }

                var sf = (from BObject z in db.ObjectsTable // z is parent
                         where path64s.Contains(z.Path64)
                         select z).ToList();

                result.AddRange(sf.Where(obj => obj.ObjectType == BObject.ObjectTypeType.FileSystem)
                    .Select(obj => obj as BFileSystem)
                    .Where(obj => obj.IsFile)
                    .Select(obj => obj.Id));

                db.ObjectsTable.DeleteAllOnSubmit(sf);

//                foreach (var path64 in path64s)
//                {
//                    var obj = (from BObject bo in db.ObjectsTable
//                               where bo.Path64 == path64
//                               select bo).First();

//#if DEBUG
//                    if (obj == null) throw new ArgumentException();
//#else
//                    if (obj == null) continue;
//#endif

//                    if (obj.ObjectType == BObject.ObjectTypeType.FileSystem &&
//                        obj.IsFile)
//                    {
//                        var file = obj as BFileSystem;
//                        result.Add(file.Id);
//                    }
//                    else
//                    {
//                        var subobjs = (from BFileSystem fs in db.ObjectsTable
//                                       where fs.Path64.StartsWith(path64 + "/")
//                                       select fs);

//                        result.AddRange(subobjs.Where(z => z.IsFile).Select<BFileSystem, string>(z => z.Id));

//                        db.ObjectsTable.DeleteAllOnSubmit(subobjs);
//                    }
//                }

                db.WriteEnd();
            }

            return result;
        }
    }
}
