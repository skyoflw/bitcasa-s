﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace LockScreenImage
{
    /// <summary>
    /// 为锁屏壁纸提供方法支持
    /// </summary>
    public class Provider
    {
        /// <summary>
        /// 指示应用程序是否为当前锁定屏幕的提供程序
        /// </summary>
        public static bool IsProvider
        {
            get { return Windows.Phone.System.UserProfile.LockScreenManager.IsProvidedByCurrentApplication; }
        }

        /// <summary>
        /// 请求作为锁屏壁纸来源
        /// <para/>因为似乎会弹出对话框，所以觉得应该由 UI 调用才对。。。
        /// </summary>
        /// <returns></returns>
        public static async Task<bool> RequestProviderAsync()
        {
            // If you're not the provider, this call will prompt the user for permission.
            // Calling RequestAccessAsync from a background agent is not allowed.
            var op = await Windows.Phone.System.UserProfile.LockScreenManager.RequestAccessAsync();

            // Only do further work if the access was granted.
            return op == Windows.Phone.System.UserProfile.LockScreenRequestResult.Granted;
        }

        /// <summary>
        /// 设置当前背景
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="isAppResource"></param>
        /// <returns></returns>
        private static bool SetLockScreenImage(string imagePath, bool isAppResource)
        {
            try
            {
                if (IsProvider)
                {
                    // At this stage, the app is the active lock screen background provider.

                    // The following code example shows the new URI schema.
                    // ms-appdata points to the root of the local app data folder.
                    // ms-appx points to the Local app install folder, to reference resources bundled in the XAP package.
                    var schema = isAppResource ? "ms-appx:///" : "ms-appdata:///Local/";

                    var uri = new Uri(schema + imagePath, UriKind.Absolute);

                    // Set the lock screen background image.
                    Windows.Phone.System.UserProfile.LockScreen.SetImageUri(uri);

#if DEBUG
                    // Get the URI of the lock screen background image.
                    var currentImage = Windows.Phone.System.UserProfile.LockScreen.GetImageUri();
                    System.Diagnostics.Debug.WriteLine("The new lock screen background image is set to {0}", currentImage.ToString());
#endif
                    return true;
                }
            }
            catch (System.Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine(ex.ToString());
#endif
            }

            return false;
        }

        /// <summary>
        /// 从应用内跳转至锁定屏幕设置屏幕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static async void NavToLockScreenSetting(object sender, RoutedEventArgs e)
        {
            // Launch URI for the lock screen settings screen.
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-lock:"));
        }
    }
}
