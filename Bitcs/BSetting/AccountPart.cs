﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSetting
{
    public static partial class Setting
    {
        public static class Account
        {
            private const string KeyIsLastLogin = "{F00C556F-E318-46CA-B256-E4A1E6AFA1FF}";
            private const string KeyStorageTotal = "{2C6B2A60-6009-4DDE-80B8-26371F7B7219}";
            private const string KeyReferralLink = "{F95DF0DD-7997-45F4-8276-2C97B3C19A01}";
            private const string KeyId = "{4B27E329-667F-4AAC-8C9C-5E2A57628F1D}";
            private const string KeyDisplayName = "{8399ECA1-E4C0-4539-BC83-77534B143268}";
            private const string KeyStorageDisplay = "{C9F0D02C-8623-4628-B26F-6E8416259BAF}";
            private const string KeyStorageUsed = "{273C1B8B-A712-4E4C-8BC8-0BB3683DD7C6}";

            /// <summary>
            /// 提供一个值指示如果当前存有 AccountId，则此用户 Id 是否是上一个用户的信息。
            /// 如果 true，表示是上一个用户的，否则为 false
            /// 
            /// <para/>
            /// 策略是，如果是上一个用户的，则不进行显示，只进行比较，
            /// 如果下一个用户和上一个用户不同，则进行数据库的清理
            /// <para/>默认值为 true
            /// </summary>
            public static bool IsLastLogin
            {
                get { return Setting.GetValue<bool>(KeyIsLastLogin, true); }
                set { Setting.SetValue<bool>(KeyIsLastLogin, value); }
            }

            /// <summary>
            /// 显示名称
            /// </summary>
            public static string DisplayName
            {
                get { return GetValue<string>(KeyDisplayName, null); }
                set { SetValue<string>(KeyDisplayName, value); }
            }
            /// <summary>
            /// 账户 Id
            /// </summary>
            public static string Id
            {
                get { return GetValue<string>(KeyId, null); }
                set { SetValue<string>(KeyId, value); }
            }
            /// <summary>
            /// 邀请链接
            /// </summary>
            public static string ReferralLink
            {
                get { return GetValue<string>(KeyReferralLink, null); }
                set { SetValue<string>(KeyReferralLink, value); }
            }
            /// <summary>
            /// 用户总空间（无限空间为 0）
            /// </summary>
            public static long StorageTotal
            {
                get { return GetValue<long>(KeyStorageTotal, 0); }
                set { SetValue<long>(KeyStorageTotal, value); }
            }
            /// <summary>
            /// 用于显示的用户空间大小
            /// </summary>
            public static string StorageDisplay
            {
                get { return GetValue<string>(KeyStorageDisplay, null); }
                set { SetValue<string>(KeyStorageDisplay, value); }
            }
            /// <summary>
            /// 用户已经使用的总空间大小
            /// </summary>
            public static long StorageUsed
            {
                get { return GetValue<long>(KeyStorageUsed, 0); }
                set { SetValue<long>(KeyStorageUsed, value); }
            }
        }
    }
}
