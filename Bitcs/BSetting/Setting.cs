﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSetting
{
    public static partial class Setting
    {
        private static IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        private static T GetValue<T>(string key, T def)
        {
            try
            {
                T a;
                if (settings.TryGetValue<T>(key, out a))
                    return a;
                return def;
            }
            catch (Exception)
            {
                if (settings.Contains(key))
                    return (T)settings[key];
                else
                    return def;
            }
        }
        private static bool SetValue<T>(string key, T value)
        {
            T a;

            if (settings.TryGetValue(key, out a))
            {
                if (a == null)
                {
                    if (value == null)
                        return false;
                }
                else if (a.Equals(value))
                    return false;
            }

            settings[key] = value;
            settings.Save();
            return true;
        }

#if DEBUG
        // time : 14/4/21
        public static string Token = "US1_2c154723201e451b87d8395e18ed00e6_acbcda21";
#else
        public static string Token
        {
            get { return GetValue<string>(KeyToken, null); }
            set { SetValue<string>(KeyToken, value); }
        }
#endif

        #region auto upload

        public static bool IsEnableAutoUpload
        {
            get { return GetValue<bool>(KeyIsEnableAutoUpload, false); }
            set { SetValue<bool>(KeyIsEnableAutoUpload, value); }
        }

        public static string AutoUploadFolder
        {
            get { return GetValue<string>(KeyAutoUploadFolder, null); }
            private set { SetValue<string>(KeyAutoUploadFolder, value); }
        }

        public static string AutoUploadFolderForHuman
        {
            get { return GetValue<string>(KeyAutoUploadFolderForHuman, null); }
            private set { SetValue<string>(KeyAutoUploadFolderForHuman, value); }
        }

        public static void SetAutoUploadFolder(string base64, string forhuman)
        {
            if (base64 == null || forhuman == null)
#if DEBUG
                throw new ArgumentNullException();
#else
                return;
#endif
            AutoUploadFolder = base64;
            AutoUploadFolderForHuman = forhuman;
        }

        public static void ClearAutoUploadFolder()
        {
            AutoUploadFolder = null;
            AutoUploadFolderForHuman = null;
        }

        public static bool IsAutoUploadAsSystemSerive
        {
            get { return GetValue<bool>(KeyIsAutoUploadAsSystemSerive, false); }
            set { SetValue<bool>(KeyIsAutoUploadAsSystemSerive, value); }
        }

        public static bool IsAutoUploadAllPhoto
        {
            get { return GetValue<bool>(KeyIsAutoUploadAllPhoto, false); }
            set { SetValue<bool>(KeyIsAutoUploadAllPhoto, value); }
        }

        public static DateTime LastAutoUploadCameraRollPhotoTime
        {
            get { return GetValue<DateTime>(KeyLastAutoUploadCameraRollPhotoTime, DefaultTime); }
            set { SetValue<DateTime>(KeyLastAutoUploadCameraRollPhotoTime, value); }
        }

        public static DateTime LastAutoUploadOtherPhotoTime
        {
            get { return GetValue<DateTime>(KeyLastAutoUploadOtherPhotoTime, DefaultTime); }
            set { SetValue<DateTime>(KeyLastAutoUploadOtherPhotoTime, value); }
        }

        /// <summary>
        /// 至今上载成功照片的数量
        /// </summary>
        public static int AllUploadPhotoCount
        {
            get { return GetValue<int>(KeyAllUploadPhotoCount, 0); }
            set { SetValue<int>(KeyAllUploadPhotoCount, value); }
        }

        /// <summary>
        /// 后台代理运行上传照片的次数
        /// </summary>
        public static int AutoUploadAsSystemSeriveRunTime
        {
            get { return GetValue<int>(KeyAutoUploadAsSystemSeriveRunTime, 0); }
            set { SetValue<int>(KeyAutoUploadAsSystemSeriveRunTime, value); }
        }

        #endregion

        #region password

        /// <summary>
        /// 获取或设置应用密码
        /// </summary>
        public static string AppPassword
        {
            get { return GetValue<string>(KeyAppPassword, null); }
            set { SetValue<string>(KeyAppPassword, value); }
        }

        /// <summary>
        /// 获取或设置是否启用对象密码锁
        /// 此功能仅付费用户可用
        /// </summary>
        public static bool IsEnableObjectPassword
        {
            get { return GetValue<bool>(KeyIsEnableObjectPassword, false); }
            set { SetValue<bool>(KeyIsEnableObjectPassword, value); }
        }

        #endregion

        public static readonly DateTime DefaultTime = new DateTime(1990, 1, 1);

        /// <summary>
        /// 获取或设置应用上次版本号。默认版本号为 0。
        /// </summary>
        public static int LastVersion
        {
            get { return GetValue<int>(KeyLastVersion, 0); }
            set { SetValue<int>(KeyLastVersion, value); }
        }
    }
}
