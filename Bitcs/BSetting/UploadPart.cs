﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSetting
{
    public static partial class Setting
    {
        public static class UploadOption
        {
            private const string KeyUploadOptionExistsMode = "{9C28EC2E-7BB9-46FC-B0F3-CC2A4E6BF275}";

            /// <summary>
            /// 配置 BUploadFileRequest 的 ExistsMode 选项
            /// </summary>
            public static int UploadOptionExistsMode
            {
                get { return GetValue<int>(KeyUploadOptionExistsMode, 0); }
                set { SetValue<int>(KeyUploadOptionExistsMode, value); }
            }
        }
    }
}
