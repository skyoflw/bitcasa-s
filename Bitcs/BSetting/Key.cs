﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BSetting
{
    public static partial class Setting
    {
        private const string KeyToken = "Token";

        #region auto upload

        private const string KeyIsEnableAutoUpload = "{5EC7EFB8-8C3D-4027-8841-D6234F2FF090}";

        private const string KeyAutoUploadFolder = "{3DD38633-BBD9-4D05-BB18-085EF07A4D0C}";

        private const string KeyAutoUploadFolderForHuman = "{74A529E1-FFAC-4D32-9AF5-BBFFCEC96457}";

        private const string KeyIsAutoUploadAsSystemSerive = "{2C938D62-B59D-4FEE-BAFF-1390261D9467}";

        private const string KeyIsAutoUploadAllPhoto = "{C4209B6A-FFDC-436A-AFDA-695254B2A539}";

        private const string KeyLastAutoUploadCameraRollPhotoTime = "{C79D210D-D951-44A0-93CF-210A1C3DD3E4}";

        private const string KeyLastAutoUploadOtherPhotoTime = "{5D2A2A37-25A5-46F4-BA41-3E10E94750D8}";

        private const string KeyAllUploadPhotoCount = "{7DF5E632-293F-49A4-8179-D25384E0E5C3}";

        private const string KeyAutoUploadAsSystemSeriveRunTime = "{E1640493-38C2-4421-A441-186184F366CF}";

        #endregion

        #region password

        private const string KeyAppPassword = "{D9ECC3E1-95B3-4910-A2D4-04706183D6DE}";

        private const string KeyIsEnableObjectPassword = "{0A05D54D-4C4E-45C5-A979-480117CCA89D}";

        #endregion

        private const string KeyLastVersion = "{68FAA91F-2F63-4317-9158-64EA6DD90388}";
    }
}
