﻿using BitcasaSDK_WP;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BObj;
using BitcasaSDK_WP.BRequest;
using BitcasaSDK_WP.Parameter;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BDll
{
    [Table]
    public class BatchOpera
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sources">所有源都应该位于同一目录下</param>
        /// <param name="destDir"></param>
        /// <param name="sourcesParentHPath"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static BatchOpera[] Move(
            IEnumerable<BFileSystem> sources,
            BFileSystem destDir,
            string sourcesParentHPath,
            string destDirHPath)
        {
            return sources.Select<BFileSystem, BatchOpera>(
                fs => new BatchOpera(
                    OperaType.Move,
                    fs, destDir,
                    sourcesParentHPath + "/" + fs.Name,
                    destDirHPath))
                .ToArray();
        }
        /// <summary>
        /// 所有源都应该位于同一目录下
        /// </summary>
        /// <param name="sources"></param>
        /// <param name="destDir"></param>
        /// <param name="sourcesParentHPath"></param>
        /// <param name="destDirHPath"></param>
        /// <returns></returns>
        public static BatchOpera[] Copy(
            IEnumerable<BFileSystem> sources,
            BFileSystem destDir,
            string sourcesParentHPath,
            string destDirHPath)
        {
            return sources.Select<BFileSystem, BatchOpera>(
                fs => new BatchOpera(
                    OperaType.Copy,
                    fs, destDir,
                    sourcesParentHPath + "/" + fs.Name,
                    destDirHPath)).ToArray();
        }
        public static BatchOpera[] Delete(
            IEnumerable<BFileSystem> sources,
            string sourcesParentHPath)
        {
            return sources.Select<BFileSystem, BatchOpera>(
                fs => new BatchOpera(
                    OperaType.Copy,
                    fs, sourcesParentHPath + "/" + fs.Name))
                .ToArray();
        }

        /// <summary>
        /// 仅供数据库调用，不允许手动调用
        /// </summary>
        public BatchOpera()
        {
        }

        private BatchOpera(OperaType type,
            BFileSystem source, BFileSystem destDir,
            string sourceHPath, string destDirHPath)
            : this(type, source, sourceHPath)
        {
            this.DestPath64 = destDir.Path64;
            this.DestHPath = destDirHPath;
        }
        private BatchOpera(OperaType type, BFileSystem source, string sourceHPath)
        {
            this.Id = Guid.NewGuid();
            this.CreateTime = DateTime.UtcNow;
            this.Type = type;

            this.SourcePath64 = source.Path64;
            this.SourceHPath = sourceHPath;

            this.FileName = source.Name;
            
            this.SourceIsFile = source.IsFile;
        }

        /// <summary>
        /// [Column(IsPrimaryKey = true)]
        /// 对象在数据库中的唯一 Id
        /// </summary>
        [Column(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// [Column]
        /// 对象的类型
        /// </summary>
        [Column]
        public OperaType Type { get; set; }

        /// <summary>
        /// [Column(CanBeNull = false)]
        /// base64 源路径。
        /// 此项不允许为 null。
        /// </summary>
        [Column(CanBeNull = false)]
        public string SourcePath64 { get; set; }

        /// <summary>
        /// [Column]
        /// base64 目标路径。
        /// 在删除操作时此项为 null。
        /// </summary>
        [Column]
        public string DestPath64 { get; set; }

        /// <summary>
        /// [Column]
        /// 文件名。
        /// 移动、复制、删除操作时此项为对象文件名；
        /// 在文件重命名时此项将为目标名。
        /// </summary>
        [Column]
        public string FileName { get; set; }

        /// <summary>
        /// [Column(CanBeNull = false)]
        /// 人类可以看懂的源路径。
        /// 此项不允许为 null。
        /// </summary>
        [Column(CanBeNull = false)]
        public string SourceHPath { get; set; }

        /// <summary>
        /// [Column]
        /// 人类可以看懂的目标路径。
        /// 在删除操作时此项为 null。
        /// </summary>
        [Column]
        public string DestHPath { get; set; }

        /// <summary>
        /// [Column]
        /// </summary>
        [Column]
        public bool SourceIsFile { get; set; }

        /// <summary>
        /// [Column]
        /// 任务创建的时间，此项为 UTC 时间
        /// </summary>
        [Column]
        public DateTime CreateTime { get; set; }

        public bool IsSuccess { get; set; }

        public enum OperaType : int
        {
            Copy = 0,
            Move = 1,
            Delete = 2,
            Rename = 3
        }

        public static void RunAsync(
            BitcasaClient client,
            BatchOpera o,
            BParameter<List<BFileSystem>> callback,
            ExistsOperationType exist = ExistsOperationType.Fail)
        {

#if DEBUG
            if (o == null)
                throw new ArgumentNullException();
#else
            if (o == null)
                return;
#endif
            Task.Run(() =>
                {
                    switch (o.Type)
                    {
                        case OperaType.Copy:
                            if (o.SourceIsFile)
                                client.CopyFile(
                                    o.SourcePath64,
                                    o.DestPath64,
                                    o.FileName,
                                    callback,
                                    exist);
                            else
                                client.CopyFolder(
                                    o.SourcePath64,
                                    o.DestPath64,
                                    o.FileName,
                                    callback,
                                    exist);
                            break;
                        case OperaType.Delete:
                            if (o.SourceIsFile)
                                client.DeleteFile(
                                    o.SourcePath64,
                                    callback);
                            else
                                client.DeleteFolder(
                                    o.SourcePath64,
                                    callback);
                            break;
                        case OperaType.Move:
                            if (o.SourceIsFile)
                                client.MoveFile(
                                    o.SourcePath64,
                                    o.DestPath64,
                                    o.FileName,
                                    callback,
                                    exist);
                            else
                                client.MoveFolder(
                                    o.SourcePath64,
                                    o.DestPath64,
                                    o.FileName,
                                    callback,
                                    exist);
                            break;
#if DEBUG
                        default:
                            throw new NotImplementedException();
#endif
                    }
                });
        }
    }
}
