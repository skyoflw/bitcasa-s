﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDll
{
    [Table]
    [Index(Columns = "Path", IsUnique = false)]
    public class TaskInfo
    {
        /// <summary>
        /// [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        /// </summary>
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public Guid Id;

        /// <summary>
        /// path64
        /// </summary>
        [Column]
        public string Path;

        /// <summary>
        /// [Column]
        /// may have this, like upload and download task
        /// </summary>
        [Column]
        public Guid CacheFileId;

        /// <summary>
        /// TaskStatus
        /// </summary>
        [Column]
        public TaskStatus Status;

        /// <summary>
        /// TaskType
        /// </summary>
        [Column]
        public TaskType Type;

        /// <summary>
        /// other info may save in here
        /// </summary>
        [Column]
        public string Note;

        public void Update(TaskInfo f)
        {
            this.Path = f.Path;
            this.CacheFileId = f.CacheFileId;
            this.Status = f.Status;
            this.Type = f.Type;
            this.Note = f.Note;
        }
    }

    public enum TaskType : int
    {
        DownLoad,

        UploadLoad
    }

    public enum TaskStatus : int
    {
        NotBegin = 0,

        InCompleted = 1,

        Completed = 2
    }
}
