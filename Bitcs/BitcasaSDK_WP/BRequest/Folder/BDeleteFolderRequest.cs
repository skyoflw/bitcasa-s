﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BDeleteFolderRequest : BDeleteFileSystemRequest
    {
        public BDeleteFolderRequest(string token, string path)
            : base(token, path)
        {
        }

        protected override string BaseUrl
        {
            get { return BUrl.FOLDER_URL; }
        }
    }
}
