﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BListFolderRequest : BListRequest<List<BFileSystem>>
    {
        public BListFolderRequest(string access_token, string path)
            : base(access_token, path)
        {
        }

        protected override void RequestSucceededCallback()
        {
            var json = GetJson<BListFolderJson>();

            if (json == null) return;

            OnRequestSucceeded(json.Result.Items);
        }
    }
}
