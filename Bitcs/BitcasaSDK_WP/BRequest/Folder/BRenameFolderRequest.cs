﻿using BitcasaSDK_WP.BConst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BRenameFolderRequest : BRenameFileSystemRequest
    {
        public BRenameFolderRequest(
            string token, string from, string filename)
            : base(token, from, filename)
        {
        }

        protected override string BaseUrl
        {
            get { return BConst.BUrl.FOLDER_URL; }
        }
    }
}
