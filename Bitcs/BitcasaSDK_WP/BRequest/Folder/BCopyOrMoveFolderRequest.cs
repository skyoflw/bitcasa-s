﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BCopyOrMoveFolderRequest : BCopyOrMoveFileSystemRequest
    {
        public BCopyOrMoveFolderRequest(
            string token, string from, string to, 
            string filename, bool isMove)
            : base(token, from, to, filename, isMove)
        {
        }

        protected override string BaseUrl
        {
            get { return BConst.BUrl.FOLDER_URL; }
        }
    }
}
