﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BAddFolderRequest : BAuthenticationRequest<List<BFileSystemOperationResult>>
    {
        public BAddFolderRequest(string token, string parentPath, string folderName)
            : base(token, parentPath)
        {
            this.NewFolderName = folderName;
        }

        private string NewFolderName { get; set; }

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();

            var url = BUrl.FOLDER_URL +
                (Path != null ? Path : "") + "/" +
                this.ConstructURLParameters(p);
            this.entity = ("folder_name=" + NewFolderName).ToUtf8();
            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_POST;

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            var json = GetJson<BAddFolderJson>();

            if (json == null) return;

            OnRequestSucceeded(json.Result.Items);
        }
    }
}
