﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BDeleteFolderRequest : BAuthenticationRequest<BDeleteFolderJson.Content>
    {
        public BDeleteFolderRequest(string token, string path)
            : base(token, path)
        {
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException(
                    "BDeleteFolderRequest_BDeleteFolderRequest(): path can not be null or white space.");
        }

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();

            var url = BUrl.FOLDER_URL + this.ConstructURLParameters(p);
            this.entity = ("path=" + this.Path).ToUtf8();
            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_DELETE;
            //this.CurrentRequest.ContentType = CONTENT_TYPE_UTF8_PLAIN_TEXT;

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            var data = GetResponseString();

            if (data == null)
                throw new NotImplementedException();

            if (data.Length == 0)
                throw new NotImplementedException();

            var json = BDeleteFolderJson.FromJsonString(data);

            if (json.Error == null)
                OnRequestSucceeded(json.Result);
            else
                OnRequestFailed(new BitcasaException(json.Error));
        }
    }
}
