﻿using BitcasaSDK_WP.BConst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BDeleteFileRequest : BAuthenticationRequest<object>
    {
        public BDeleteFileRequest(string token, string path)
            : base(token, path)
        {
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException(
                    "BDeleteFolderRequest_BDeleteFolderRequest(): path can not be null or white space.");
        }

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();

            this.entity = ("path=" + this.Path).ToUtf8();

            var url = BUrl.FILE_URL + this.ConstructURLParameters(p);
            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_DELETE;

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            var data = GetResponseString();

            if (data == null)
                throw new NotImplementedException();

            if (data.Length == 0)
                throw new NotImplementedException();

            //var json = ?.FromJsonString(data);

            //if (json.Error == null)
            //    OnRequestSucceeded(json.Result);
            //else
            //    OnRequestFailed(new BitcasaException(json.Error));
        }
    }
}
