﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BListFolderRequest : BListRequest<List<BFileSystem>>
    {
        public BListFolderRequest(string access_token, string path)
            : base(access_token, path)
        {
        }

        protected override void RequestSucceededCallback()
        {
            var data = GetResponseString();

            if (data == null)
                throw new NotImplementedException();

            if (data.Length == 0)
                throw new NotImplementedException();
            
            var json = BListFolderJson.FromJsonString(data);

            if (json.Error == null)
                OnRequestSucceeded(json.Result.Items);
            else
                OnRequestFailed(new BitcasaException(json.Error));
        }
    }
}
