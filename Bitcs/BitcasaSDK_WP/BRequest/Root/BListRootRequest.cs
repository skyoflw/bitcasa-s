﻿using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BListRootRequest : BListRequest<List<BRoot>>
    {
        public BListRootRequest(string token)
            : base(token, null)
        {
        }

        protected override void RequestSucceededCallback()
        {
            var json = GetJson<BListRootJson>();

            if (json == null) return;

            OnRequestSucceeded(json.Result.Items);
        }
    }
}
