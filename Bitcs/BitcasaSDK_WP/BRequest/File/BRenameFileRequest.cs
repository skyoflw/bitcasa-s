﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BRenameFileRequest : BRenameFileSystemRequest
    {
        public BRenameFileRequest(
            string token, string from, string filename)
            : base(token, from, filename)
        {
        }

        protected override string BaseUrl
        {
            get { return BConst.BUrl.FILE_URL; }
        }
    }
}
