﻿using BitcasaSDK_WP.BConst;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    public class BStreamFileRequest : BAuthenticationRequest<Stream>
    {
        /// <summary>
        /// 流指定文件
        /// </summary>
        /// <param name="token"></param>
        /// <param name="fileId"></param>
        public BStreamFileRequest(string token, string fileId)
            : base(token, null)
        {
            if (String.IsNullOrWhiteSpace(fileId))
                throw new ArgumentException(
                    "BDownloadFileRequest_BDownloadFileRequest(): fileId can not be null or white space.");

            this.FileId = fileId;
        }
        /// <summary>
        /// 将多个文件流为一个 zip 文件
        /// </summary>
        /// <param name="token"></param>
        /// <param name="paths"></param>
        /// <param name="outputstream"></param>
        public BStreamFileRequest(string token, string[] paths)
            : base(token, null)
        {
            if (paths == null)
                throw new ArgumentNullException(
                    "BDownloadFileRequest_BDownloadFileRequest(): paths can not be null.");

            if (paths.Length == 0)
                throw new ArgumentException(
                    "BDownloadFileRequest_BDownloadFileRequest(): paths can not be empty.");
            
            foreach (var path in paths)
                if (String.IsNullOrWhiteSpace(path))
                    throw new ArgumentException(
                        "BDownloadFileRequest_BDownloadFileRequest(): any items in paths can not be null or white space.");

            this.Paths = paths;
        }
        /// <summary>
        /// 流指定文件
        /// </summary>
        /// <param name="token"></param>
        /// <param name="path"></param>
        /// <param name="outputstream"></param>
        public BStreamFileRequest(string token, string path, string fileName)
            : base(token, path)
        {
            if (String.IsNullOrWhiteSpace(fileName))
                throw new NotSupportedException(
                    "BDownloadFileRequest_BDownloadFileRequest(): fileName can not be null or white space.");

            this.FileName = fileName;
        }

        private string FileId;

        private string[] Paths;

        private string FileName;

        /// <summary>
        /// 配置将要获取的内容长度，用来比较文件是否下载完整。
        /// 默认为 0，表示不比较，直接认为文件是完整的
        /// 也可以配置为：EndPosition - BeginPosition + 1
        /// </summary>
        public long ContentLength = 0;

        private long __beginPos = 0;
        /// <summary>
        /// 获取或设置流开始位置
        /// 如果小于 0，则会抛出异常
        /// 如果大于等于文件实际长度，则服务器会返回请求失败
        /// </summary>
        public long BeginPosition
        {
            get
            {
                return __beginPos;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("BStreamFileRequest_BeginPosition: value must >= 0");

                __beginPos = value;
            }
        }

        private long __endPos = 0;
        /// <summary>
        /// 获取或设置流结束位置
        /// 如果小于 0，则会抛出异常
        /// 如果等于 0，表示读到文件结束
        /// 如果大于文件实际长度，则服务器会返回请求失败
        /// 实际返回的字节数：EndPosition - BeginPosition + 1
        /// </summary>
        public long EndPosition
        {
            get
            {
                return __endPos;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("BStreamFileRequest_EndPosition: value must >= 0");

                __endPos = value;
            }
        }

        /// <summary>
        /// 提供 Uri 以便更新壁纸
        /// </summary>
        public Uri FileUri
        {
            get
            {
                string url = BUrl.FILE_URL;
                Dictionary<string, string> p = new Dictionary<string, string>();

                if (FileId != null)
                    url += ("/" + FileId + "/");
                else if (this.Path != null)
                {
                    p["path"] = this.Path;
                }
                else if (this.Paths != null) // 暂时不理解
                {
                    throw new NotImplementedException("BStreamFileRequest_FileUri");
                }
#if DEBUG
                else // 三个都没有，为啥呢？
                    throw new Exception("BStreamFileRequest_FileUri");
#endif

                if (this.FileName != null)
                    url += ("/" + FileName);

                url += ConstructURLParameters(p);

                return new Uri(url);
            }
        }

        public override void SendRequestAsync()
        {
            this.CurrentRequest = HttpWebRequest.Create(FileUri) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_GET;
            this.CurrentRequest.AllowReadStreamBuffering = false;

            if (BeginPosition > 0 || EndPosition > 0)
            {
                if (BeginPosition >= EndPosition && EndPosition > 0)
                    throw new ArgumentException(
                        "BStreamFileRequest_SendRequestAsync(): BeginPosition must < EndPosition.");

                this.CurrentRequest.Headers["Range"] =
                        "bytes=" + BeginPosition.ToString() +
                        "-" + (EndPosition > 0 ? EndPosition.ToString() : "");
            }

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            if (IsCancelled)
            {
                OnRequestCancelled();
                return;
            }
            var s = CurrentResponse.GetResponseStream();
            OnRequestSucceeded(s);
        }
    }
}
