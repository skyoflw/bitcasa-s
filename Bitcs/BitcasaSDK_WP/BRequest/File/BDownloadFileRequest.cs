﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    public class BDownloadFileRequest : BStreamFileRequest
    {
        /// <summary>
        /// 下载指定文件
        /// </summary>
        /// <param name="token"></param>
        /// <param name="outputstream">一个可写流以便让请求写入内容</param>
        /// <param name="fileId"></param>
        public BDownloadFileRequest(string token, Stream outputstream, string fileId)
            : base(token, fileId)
        {
            if (!outputstream.CanWrite)
                throw new NotSupportedException(
                    "BDownloadFileRequest_BDownloadFileRequest(): stream must can write.");

            this.BuffStream = outputstream;
        }
        /// <summary>
        /// 将多个文件下载为一个 zip 文件
        /// </summary>
        /// <param name="token"></param>
        /// <param name="paths"></param>
        /// <param name="outputstream">一个可写流以便让请求写入内容</param>
        public BDownloadFileRequest(string token, Stream outputstream, string[] paths)
            : base(token, paths)
        {
            if (!outputstream.CanWrite)
                throw new NotSupportedException(
                    "BDownloadFileRequest_BDownloadFileRequest(): stream must can write.");

            this.BuffStream = outputstream;
        }
        /// <summary>
        /// 下载指定文件
        /// </summary>
        /// <param name="token"></param>
        /// <param name="path"></param>
        /// <param name="outputstream">一个可写流以便让请求写入内容</param>
        public BDownloadFileRequest(string token, string path, string fileName, Stream outputstream)
            : base(token, path, fileName)
        {
            if (!outputstream.CanWrite)
                throw new NotSupportedException(
                    "BDownloadFileRequest_BDownloadFileRequest(): stream must can write.");

            this.BuffStream = outputstream;
        }

        private Stream BuffStream;

        private const int BUFF_LENGTH = 4096;

        protected override void RequestSucceededCallback()
        {
            long len = ReadStream(CurrentResponse.GetResponseStream());

            if (ContentLength != 0 &&
                len != ContentLength)
            {
                base.OnRequestFailed(null);
            }
            else
            {
                base.OnRequestSucceeded(null);
            }
        }

        private long ReadStream(Stream responseStream)
        {
            long currentReadedBytes = 0;
            long time_100 = 0;

            try
            {
                using (BinaryWriter bw = new BinaryWriter(this.BuffStream))
                {
                    using (BinaryReader reader = new BinaryReader(responseStream))
                    {
                        byte[] buffer = new byte[BUFF_LENGTH];
                        while (true)
                        {
                            if (IsCancelled)
                            {
                                bw.Flush();
                                this.BuffStream.Close();
                                return currentReadedBytes;
                            }

                            int n = reader.Read(buffer, 0, BUFF_LENGTH);
                            if (n <= 0)
                                break;
                            bw.Write(buffer, 0, n);
                            currentReadedBytes += n;

                            if (currentReadedBytes > (BUFF_LENGTH * 100 * time_100))
                            {
                                time_100++;
                                bw.Flush();
                            }

                            OnBytesChanged(currentReadedBytes);
                        }
                    }
                    bw.Flush();
                }
            }
            catch 
            {
                return currentReadedBytes;
            }

            return currentReadedBytes;
        }

        private void OnBytesChanged(long currentReadedBytes)
        {
            if (BytesChanged != null)
                BytesChanged(this, currentReadedBytes);
        }
        /// <summary>
        /// 下载的进度改变，此值介于 0 到 100 之间
        /// 此值为文件当前进度除以文件总大小乘以 100 得出，与断点续传无关
        /// </summary>
        public event EventHandler<long> BytesChanged;
    }
}
