﻿using BitcasaSDK_WP.BConst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BDeleteFileRequest : BDeleteFileSystemRequest
    {
        public BDeleteFileRequest(string token, string path)
            : base(token, path)
        {
        }

        protected override string BaseUrl
        {
            get { return BUrl.FILE_URL; }
        }
    }
}
