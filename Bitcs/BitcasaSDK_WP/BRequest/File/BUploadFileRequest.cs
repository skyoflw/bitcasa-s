﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace BitcasaSDK_WP.BRequest
{
    internal class BUploadFileRequest : BAuthenticationRequest<BFileSystem>
    {
        //private const string ENCODING = "UTF-8";
	    public const string CRLF = "\r\n";
	    public const string TWO_HYPHENS = "--";
        public const string BOUNDARY = "----------*****----------bitcasa";

        public BUploadFileRequest(
            string token, string folder_path, 
            string filename,
            Stream stream)
            : base(token, folder_path)
        {
            if (stream == null)
                throw new ArgumentNullException();

            if (!stream.CanRead)
                throw new ArgumentException();

            if (String.IsNullOrWhiteSpace(filename))
                throw new ArgumentException();

            this.NeedUploadStream = stream;

            this.FileName = filename;
        }

        private Stream NeedUploadStream { get; set; }

        private string FileName { get; set; }

        public ExistsStatus Mode = ExistsStatus.Rename;

        public override void SendRequestAsync()
        {
            Task.Run(() => { SendRequest(); });
        }

        private void SendRequest()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();

            p["exists"] = Mode.ToString().ToLower();

            var url = BUrl.FILE_URL + this.Path + "/" +
                this.ConstructURLParameters(p);

            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_POST;

            this.CurrentRequest.Headers["Connection"] = "Keep-Alive";
            this.CurrentRequest.Headers["Cache-Control"] = "no-cache";
            this.CurrentRequest.ContentType = "multipart/form-data;boundary=" + BOUNDARY;

            this.CurrentRequest.AllowWriteStreamBuffering = false;

            using (var ms_header = new MemoryStream())
            {
                byte[] tmp;

                tmp = (TWO_HYPHENS + BOUNDARY + CRLF).ToUtf8();
                ms_header.Write(tmp, 0, tmp.Length);

                tmp = "Content-Disposition: form-data; name=\"file\"; filename=\"".ToUtf8();
                ms_header.Write(tmp, 0, tmp.Length);

                tmp = FileName.ToUtf8();
                ms_header.Write(tmp, 0, tmp.Length);

                tmp = ("\"" + CRLF).ToUtf8();
                ms_header.Write(tmp, 0, tmp.Length);

                tmp = (CRLF).ToUtf8();
                ms_header.Write(tmp, 0, tmp.Length);

                Header = ms_header.ToArray();
            }

            using (var ms_tail = new MemoryStream())
            {
                byte[] tmp;

                tmp = (CRLF).ToUtf8();
                ms_tail.Write(tmp, 0, tmp.Length);

                tmp = (TWO_HYPHENS + BOUNDARY + TWO_HYPHENS + CRLF).ToUtf8();
                ms_tail.Write(tmp, 0, tmp.Length);

                Tail = ms_tail.ToArray();
            }
            
            CurrentRequest.ContentLength =
                Header.Length + Tail.Length + NeedUploadStream.Length;
            CurrentRequest.BeginGetRequestStream(
                new AsyncCallback(GetRequestCallBack), null);
        }

        private byte[] Header;

        private byte[] Tail;

        protected new void GetRequestCallBack(IAsyncResult result)
        {
            if (IsCancelled)
            {
                OnRequestCancelled();
                return;
            }

            try
            {
                int buff_len = 40960;
                byte[] buff = new byte[buff_len];
                int buff_cur = 0;
                bool is_time_out = false;
                using (Stream requestStream = CurrentRequest.EndGetRequestStream(result))
                {
                    ManualResetEvent waiter = new ManualResetEvent(false);

                    long totsend = 0;
                    if (!IsCancelled)
                    {
                        waiter.Reset();
                        Task.Run(() =>
                        {
                            requestStream.Write(Header, 0, Header.Length);
                            waiter.Set();
                        });
                        is_time_out = !waiter.WaitOne(10000);
                    }
                    NeedUploadStream.Position = 0;

                    if (!is_time_out)
                    {
                        while (true)
                        {
                            buff_cur = NeedUploadStream.Read(buff, 0, buff_len);
                            if (buff_cur <= 0 || IsCancelled)
                                break;

                            waiter.Reset();
                            Task.Run(() =>
                                {
                                    requestStream.Write(buff, 0, buff_cur);
                                    waiter.Set();
                                });


                            if (!waiter.WaitOne(10000))
                            {
                                is_time_out = true;
                                break;
                            }

                            totsend += buff_cur;
                            OnBytesChanged(totsend);
                        }
                    }

                    if (!(IsCancelled || is_time_out))
                    {
                        waiter.Reset();
                        Task.Run(() =>
                        {
                            requestStream.Write(Tail, 0, Tail.Length);
                            waiter.Set();
                        });
                        is_time_out = !waiter.WaitOne(10000);
                    }
                }

                if (!(IsCancelled || is_time_out))
                    CurrentRequest.BeginGetResponse(new AsyncCallback(GetResponseCallBack), null);
                else
                {
                    CurrentRequest.Abort();
                    throw new WebException(
                        ErrorId,
                        WebExceptionStatus.Timeout);
                }
            }
            catch (WebException e)
            {
                if (e.Message == ErrorId &&
                    e.Status == WebExceptionStatus.Timeout)
                {
                    this.SendRequestAsync();
                    return;
                }

                Catch(e);
            }
        }

        private const string ErrorId = 
            "in app: write stream time out - {8B212C16-71D7-4674-B05F-6A1FC8DFB399}";

        protected override void RequestSucceededCallback()
        {
            var json = GetJson<BListFolderJson>();

            if (json == null) return;

            if (json.Result.Items.Count > 0)
                OnRequestSucceeded(json.Result.Items[0]);
            else
                OnRequestCompleted();
        }

        private void OnBytesChanged(long bytes)
        {
            if (BytesChanged != null)
                BytesChanged(this, bytes);
        }
        public event EventHandler<long> BytesChanged;
    }

    public enum ExistsStatus : int
    {
        /// <summary>
        /// new file will get (N) appended to the filename, 
        /// where N is an unused integer 
        /// (file extensions are dealt with correctly, 
        /// it is not a naive append).
        /// </summary>
        Rename = 0,

        /// <summary>
        /// request will return a 409 if a filename conflict occurs.
        /// </summary>
        Fail = 1,

        /// <summary>
        /// existing file with matching name will be replaced with file being uploaded.
        /// </summary>
        Overwrite = 2
    }
}
