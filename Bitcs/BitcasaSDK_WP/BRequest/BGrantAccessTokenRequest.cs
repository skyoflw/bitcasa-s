﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BGrantAccessTokenRequest : BRequest<string>
    {
        public BGrantAccessTokenRequest(string clientSecret, string authenticationCode)
        {
            if (clientSecret == null)
                throw new ArgumentNullException(
                    "BGrantAccessTokenRequest_BGrantAccessTokenRequest(): clientSecret can not be null.");

            if (authenticationCode == null)
                throw new ArgumentNullException(
                    "BGrantAccessTokenRequest_BGrantAccessTokenRequest(): authenticationCode can not be null.");

            this.ClientSecret = clientSecret;
            this.AuthenticationCode = authenticationCode;
        }

        private string ClientSecret;

        private string AuthenticationCode;

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>()
            {
                { BUrl.FIELD_NAME_SECRET, ClientSecret      },
                { BUrl.FIELD_NAME_CODE  , AuthenticationCode},
            };

            var url = BUrl.GRANT_ACCESS_TOKEN_URL + this.ConstructURLParameters(p);
            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            var json = GetJson<BAccessTokenJson>();

            if (json == null) return;

            OnRequestSucceeded(json.Result.AccessToken);
        }
    }
}
