﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal abstract class BFolderOperationRequest<TSucceeded> : 
        BAuthenticationRequest<TSucceeded>
    {
        public BFolderOperationRequest(string access_token)
            : base(access_token)
        {
        }
    }
}
