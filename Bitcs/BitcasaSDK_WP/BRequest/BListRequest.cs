﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal abstract class BListRequest<TSucceeded> : 
        BAuthenticationRequest<TSucceeded>
    {
        public BListRequest(string access_token, string path)
            : base(access_token, path)
        {
        }

        /// <summary>
        /// 可选参数，表示返回的文件类型
        /// </summary>
        public CategoryType Category = CategoryType.Everything;

        /// <summary>
        /// 可选参数，表示列表返回的深度
        /// </summary>
        public DepthType Depth = DepthType.CurrentChildren;

        /// <summary>
        /// 小于等于 0 表示没有限制
        /// </summary>
        public int Latest = 0;

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();

            if (Category != CategoryType.Everything)
                p[BUrl.FIELD_NAME_CATEGORY] = CategoryTypeToString(this.Category);
            if (Depth != DepthType.CurrentChildren)
                p[BUrl.FIELD_NAME_DEPTH] = ((int)Depth).ToString();
            if (Latest > 0)
                p[BUrl.FIELD_NAME_LATEST] = Latest.ToString();

            var url = BUrl.FOLDER_URL +
                (Path != null ? Path : "") + "/" +
                this.ConstructURLParameters(p);

            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_GET;

            base.SendRequestAsync();
        }

        private static string CategoryTypeToString(CategoryType type)
        {
            switch (type)
            {
                case CategoryType.Documents:
                    return "documents";
                case CategoryType.Everything:
                    return "documents";
                case CategoryType.MusicAlbums:
                    return "music_albums";
                case CategoryType.MusicArtists:
                    return "music_artists";
                case CategoryType.MusicTracks:
                    return "music_tracks";
                case CategoryType.PhotoAlbums:
                    return "photo_albums";
                case CategoryType.Photos:
                    return "photos";
                case CategoryType.Videos:
                    return "videos";
                default:
                    throw new ArgumentException(
                        "BListRequest_CategoryTypeToString(): arg:[" + type.ToString() + "]");
            }
        }
    }

    public enum CategoryType
    {
        MusicArtists,
        MusicAlbums,
        MusicTracks,
        PhotoAlbums,
        Photos,
        Documents,
        Videos,
        Everything
    }

    /// <summary>
    /// 列表的深度
    /// </summary>
    public enum DepthType : int
    {
        /// <summary>
        /// 无限递归返回列表
        /// </summary>
        Infinite = 0,

        /// <summary>
        /// 仅当前目录列表
        /// </summary>
        CurrentChildren = 1
    }
}
