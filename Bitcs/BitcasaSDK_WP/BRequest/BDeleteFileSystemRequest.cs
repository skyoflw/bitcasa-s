﻿using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal abstract class BDeleteFileSystemRequest
        : BAuthenticationRequest<List<BFileSystem>>
    {
        public BDeleteFileSystemRequest(string token, string path)
            : base(token, path)
        {
#if DEBUG
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException(
                    "BDeleteFileSystemRequest():" + 
                    "path can not be null or white space.");
#endif
        }

        protected abstract string BaseUrl { get; }

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();

            this.entity = ("path=" + this.Path).ToUtf8();

            var url = BaseUrl + this.ConstructURLParameters(p);
            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_DELETE;
            this.CurrentRequest.ContentType = CONTENT_TYPE_URLENCODE;

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            var json = this.GetJson<BDeleteFileSystemJson>();

            if (json.Result.Deleted.ObjectsCount > 0)
                OnRequestSucceeded(json.Result.Items);
            else
                OnRequestCompleted();
        }
    }
}
