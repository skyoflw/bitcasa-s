﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    public abstract class BAuthenticationRequest<TSucceeded> : BRequest<TSucceeded>
    {
        public BAuthenticationRequest(string access_token, string path)
        {
            if (access_token == null)
                throw new ArgumentNullException(
                    "BAuthenticationRequest<>_BAuthenticationRequest: access_token can not be null.");

            this.Path = path;
            this.AccessToken = access_token;
        }

        protected string Path { get; private set; }

        private string AccessToken { get; set; }

        protected override string ConstructURLParameters(Dictionary<string, string> parameters)
        {
            parameters[BUrl.FIELD_NAME_ACCESS_TOKEN] = this.AccessToken;
            return base.ConstructURLParameters(parameters);
        }
    }
}
