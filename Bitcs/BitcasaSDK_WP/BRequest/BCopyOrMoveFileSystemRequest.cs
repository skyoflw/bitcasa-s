﻿using BitcasaSDK_WP.BConst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal abstract class BCopyOrMoveFileSystemRequest : BOperationFileSystemRequest
    {
        public BCopyOrMoveFileSystemRequest(
            string token, string from, string to, 
            string filename, bool isMove)
            : base(token, from, filename)
        {
#if DEBUG
            if (to == null)
                throw new ArgumentException(
                    "BCopyOrMoveFolderRequest(): to can not be null.");
#endif
            this.To = to;
            this.IsMove = isMove;
        }

        private bool IsMove;

        private string To;

        protected override System.IO.MemoryStream GenerateBody()
        {
            var ms = base.GenerateBody();

            StringBuilder sb = new StringBuilder();
            sb.Append("&");
            sb.Append("to");
            sb.Append("=");
            sb.Append(this.To);
            var b = sb.ToString().ToUtf8();
            ms.Write(b, 0, b.Length);

            return ms;
        }

        protected override string Operation
        {
            get { return IsMove ? "move" : "copy"; }
        }
    }
}
