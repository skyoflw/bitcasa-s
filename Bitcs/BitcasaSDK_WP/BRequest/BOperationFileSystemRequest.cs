﻿using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal abstract class BOperationFileSystemRequest
        : BAuthenticationRequest<List<BFileSystem>>
    {
        public BOperationFileSystemRequest(
            string token, string from, string filename)
            : base(token, from)
        {
#if DEBUG
            if (from == null)
                throw new ArgumentNullException();
            if (filename == null)
                throw new ArgumentNullException();
#endif
            this.FileName = filename;
        }

        protected abstract string Operation { get; }

        protected abstract string BaseUrl { get; }

        private string FileName;

        public ExistsOperationType ExistsOperation = ExistsOperationType.Rename;

        public override void SendRequestAsync()
        {
            this.entity = GenerateBody().ToArray();

            Dictionary<string, string> p = new Dictionary<string, string>();
            p["operation"] = Operation;
            var url = BaseUrl + this.ConstructURLParameters(p);

            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_POST;
            this.CurrentRequest.ContentType = CONTENT_TYPE_URLENCODE;

            base.SendRequestAsync();
        }

        /// <summary>
        /// 包括 from、filename、exists
        /// </summary>
        /// <returns></returns>
        protected virtual MemoryStream GenerateBody()
        {
            MemoryStream ms = new MemoryStream();

            StringBuilder sb = new StringBuilder();
            sb.Append("from");
            sb.Append("=");
            sb.Append(this.Path);
            sb.Append("&");
            sb.Append("filename");
            sb.Append("=");
            sb.Append(HttpUtility.UrlEncode(FileName));
            sb.Append("&");
            sb.Append("exists");
            sb.Append("=");
            sb.Append(ExistsOperation.ToString().ToLower());
            var b = sb.ToString().ToUtf8();
            ms.Write(b, 0, b.Length);

            return ms;
        }

        protected override void RequestSucceededCallback()
        {
            var json = GetJson<BListFolderJson>();

            if (json == null) return;

            OnRequestSucceeded(json.Result.Items);
        }
    }

    public enum ExistsOperationType
    {
        /// <summary>
        /// request will return a 409 if a filename conflict occurs.
        /// </summary>
        Fail,

        /// <summary>
        /// existing file with matching name will be replaced with file being uploaded.
        /// </summary>
        Overwrite,

        /// <summary>
        /// new file will get (N) appended to the filename, 
        /// where N is an unused integer 
        /// (file extensions are dealt with correctly, it is not a naive append).
        /// </summary>
        Rename
    }
}
