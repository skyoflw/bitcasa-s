﻿using BitcasaSDK_WP.BConst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BRenameFolderRequest : BAuthenticationRequest<object>
    {
        public BRenameFolderRequest(string token, string path, string newName)
            : base(token, path)
        {
            /// 看上去 api 文档不对劲，卧槽！！！
            /// 
            throw new NotImplementedException();

            if (String.IsNullOrWhiteSpace(newName))
                throw new ArgumentException(
                    "BRenameFolderRequest_BRenameFolderRequest(): newName can not be null or white space.");

            this.NewName = newName;
        }

        private string NewName;

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();
            p["operation"] = "rename";

            var url = BUrl.FOLDER_URL + this.ConstructURLParameters(p);
            // ---
            throw new NotImplementedException("还没配置 body ！");
            this.entity = null; // 需要配置 body
            // ---
            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            this.CurrentRequest.Method = METHOD_POST;
            this.CurrentRequest.ContentType = CONTENT_TYPE_UTF8_PLAIN_TEXT;

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            throw new NotImplementedException();
        }
    }
}
