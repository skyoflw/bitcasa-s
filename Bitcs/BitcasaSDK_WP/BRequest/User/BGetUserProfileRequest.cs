﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal class BGetUserProfileRequest : BAuthenticationRequest<BUserProfile>
    {
        public BGetUserProfileRequest(string token)
            : base(token, null)
        {
        }

        public override void SendRequestAsync()
        {
            Dictionary<string, string> p = new Dictionary<string, string>();

            var url = BUrl.USER_URL + "/profile" +
                this.ConstructURLParameters(p);

            this.CurrentRequest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;

            this.CurrentRequest.Method = METHOD_GET;

            base.SendRequestAsync();
        }

        protected override void RequestSucceededCallback()
        {
            var json = GetJson<BUserProfileJson>();

            if (json == null) return;

            OnRequestSucceeded(json.Result);
        }
    }
}
