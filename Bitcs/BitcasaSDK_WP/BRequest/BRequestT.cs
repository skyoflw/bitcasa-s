﻿using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    public abstract class BRequest<TSucceeded> : BRequest
    {
        public virtual void SendRequestAsync()
        {
            if (CurrentRequest == null)
                throw new NotImplementedException(
                    "BRequest<T>_SendRequestAsync(): CurrentRequest can not be null from " + 
                    this.GetType().ToString());

            if (entity == null)
                CurrentRequest.BeginGetResponse(
                    new AsyncCallback(GetResponseCallBack), null);
            else
                CurrentRequest.BeginGetRequestStream(
                    new AsyncCallback(GetRequestCallBack), null);
        }

        protected void GetRequestCallBack(IAsyncResult result)
        {
            if (IsCancelled)
            {
                OnRequestCancelled();
                return;
            }

            try
            {
                using (Stream requestStream = CurrentRequest.EndGetRequestStream(result))
                    requestStream.Write(entity, 0, entity.Length);

                CurrentRequest.BeginGetResponse(new AsyncCallback(GetResponseCallBack), null);
            }
            catch (WebException e) { Catch(e); }
        }

        protected void GetResponseCallBack(IAsyncResult result)
        {
            if (IsCancelled)
            {
                OnRequestCancelled();
                return;
            }

            bool is_succeed = false;
            try
            {
                CurrentResponse = (HttpWebResponse)CurrentRequest.EndGetResponse(result);
                is_succeed = true;
            }
            catch (WebException e) { Catch(e); }

            if (is_succeed)
                RequestSucceededCallback();
        }

        /// <summary>
        /// 设置一个标志指示如果请求意外中断，是否自动重新请求
        /// </summary>
        public bool IsAutoReset = true;

        protected void Catch(WebException e)
        {
            if (e.Status == WebExceptionStatus.RequestCanceled &&
                !this.IsCancelled && IsAutoReset) // 非应用内调用请求取消
            {
                this.SendRequestAsync();
                return;
            }

            if (e.Response != null && e.Response.ContentLength > 0)
            {
                using (StreamReader stream = new StreamReader(e.Response.GetResponseStream()))
                {
                    var json = stream.ReadToEnd();
                    
                    BitcasaJson j = null;
                    BitcasaException be = null;
                    try { j = BitcasaJson.FromJsonString(json); }
                    catch { be = BitcasaException.FromJsonParseError(json); }
                    OnRequestFailed(be == null ? BitcasaException.FromBitcasa(j.Error) : be);
                }
            }
            else
            {
                OnRequestFailed(BitcasaException.FromWebException(e));
            }
        }

        protected abstract void RequestSucceededCallback();

        public T GetJson<T>() where T : BaseJson
        {
            string data;

            using (StreamReader stream = new StreamReader(CurrentResponse.GetResponseStream()))
                data = stream.ReadToEnd();

#if DEBUG
            if (data == null)
                throw new NotImplementedException();
            if (data.Length == 0)
                throw new NotImplementedException();
#endif
            if (data == null || data.Length == 0)
            {
                OnRequestFailed(BitcasaException.FromJsonParseError(data));
                return null;
            }

            T json = null;

            try
            {
                json = BaseJson<T>.FromJsonString(data);
            }
            catch (SerializationException)
            {
                json = null;
            }

            if (json == null)
            {
                OnRequestFailed(BitcasaException.FromJsonParseError(data));
                return null;
            }

            if (json.Error != null)
            {
                OnRequestFailed(BitcasaException.FromBitcasa(json.Error));
                return null;
            }

            return json;
        }

        #region event

        protected void OnRequestSucceeded(TSucceeded args)
        {
            if (IsCancelled)
            {
                OnRequestCancelled();
                return;
            }

            if (RequestSucceeded != null)
                RequestSucceeded(this, args);

            OnRequestCompleted();
        }
        public event EventHandler<TSucceeded> RequestSucceeded;

        protected void OnRequestFailed(BitcasaException args)
        {
            if (IsCancelled)
            {
                OnRequestCancelled();
                return;
            }

            if (RequestFailed != null)
                RequestFailed(this, args);

            OnRequestCompleted();
        }
        public event EventHandler<BitcasaException> RequestFailed;

        protected void OnRequestCancelled()
        {
            if (RequestCancelled != null)
                RequestCancelled(this, null);

            OnRequestCompleted();
        }
        public event EventHandler RequestCancelled;

        /// <summary>
        /// 若回调方法中直接调用，则说明不需要任何改变
        /// </summary>
        protected void OnRequestCompleted()
        {
            if (RequestCompleted != null)
                RequestCompleted(this, null);
        }
        public event EventHandler RequestCompleted;

        #endregion
    }
}
