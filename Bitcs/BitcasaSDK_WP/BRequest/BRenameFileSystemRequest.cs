﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BRequest
{
    internal abstract class BRenameFileSystemRequest
        : BOperationFileSystemRequest
    {
        public BRenameFileSystemRequest(
            string token, string from, string filename)
            : base(token, from, filename)
        {
        }

        protected override string Operation
        {
            get { return "rename"; }
        }
    }
}
