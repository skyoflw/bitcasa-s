﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BConst
{
    internal static class BUrl
    {
        public const string BASE_API_URL = "https://developer.api.bitcasa.com/v1";
        public const string AUTHENTUCATION_URL = BASE_API_URL + "/oauth2/authenticate?client_id={0}&redirect={1}";
        public const string GRANT_ACCESS_TOKEN_URL = BASE_API_URL + "/oauth2/access_token";

        /// <summary>
        /// "https://developer.api.bitcasa.com/v1/folders"
        /// </summary>
        public const string FOLDER_URL = BASE_API_URL + "/folders";
        /// <summary>
        /// "https://developer.api.bitcasa.com/v1/files"
        /// </summary>
        public const string FILE_URL = BASE_API_URL + "/files";
        /// <summary>
        /// "https://developer.api.bitcasa.com/v1/user"
        /// </summary>
        public const string USER_URL = BASE_API_URL + "/user";

        public const string FIELD_NAME_ACCESS_TOKEN = "access_token";

        public const string FIELD_NAME_SECRET = "secret";
        public const string FIELD_NAME_CODE = "code";

        public const string FIELD_NAME_PATH = "path";
        public const string FIELD_NAME_DEPTH = "depth";
        public const string FIELD_NAME_LATEST = "latest";
        public const string FIELD_NAME_CATEGORY = "category";
    }
}
