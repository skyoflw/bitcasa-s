﻿using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BitcasaSDK_WP
{
    public static class ExtMethod
    {
        /// <summary>
        /// 返回一个新的大写 Guid
        /// System.Guid.NewGuid().ToString().ToUpper();
        /// </summary>
        public static string NewGuid
        {
            get { return System.Guid.NewGuid().ToString().ToUpper(); }
        }

        #region string

        /// <summary>
        /// 指定的 path 为 Root 还是 FileSystem
        /// 如果是 Root，返回 true，否则为 FileSystem，返回 false
        /// </summary>
        /// <param name="path64"></param>
        /// <returns></returns>
        public static bool IsRootOrFileSystem(this string path64)
        {
            return path64.LastIndexOf('/') == 0;
        }

        public static string ToBase64(this string str)
        {
            return Convert.ToBase64String(str.ToUtf8());
        }

        public static string FromBase64(this string str)
        {
            return Convert.FromBase64String(str).ToStr();
        }

        public static byte[] ToUtf8(this string str)
        {
            return UTF8Encoding.UTF8.GetBytes(str);
        }

        /// <summary>
        /// 判断文件名/文件夹名是否有效
        /// doc from: https://developer.bitcasa.com/docs/api
        /// In the future we may relax the restrictions, 
        /// however at the moment, 
        /// the API has very strict requirements in an attempt to ensure no problems are caused in other clients.
        /// 1. All folder/file names are case insensitive, 
        ///    however the filesystem will present the folder/file name in the original case when possible.
        /// 1. 所有文件文件夹名都是不敏感的，但是不管怎样都会通过 / 符号区分它们
        /// 2. Limit the folder/file name to 64 characters.
        ///    A path composed of folder and file names may be greater than 64 characters.
        /// 2. 限制每个名字都在 64 个字符以内，每个路径总长度没有限制
        /// 3. Names may not start with "." 
        /// 4. Names may not contain any of the following characters: < > : " / \ | ? *
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool CanBeFileName(this string name)
        {
#if DEBUG
            if (name == null)
                throw new ArgumentNullException("BFileSystem_IsValid(): name can not be null.");
#endif

            if (String.IsNullOrWhiteSpace(name))
                return false;

            try
            {
                var ns = name.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                if (ns.First((s) => { return s.Length > 64; }) != null)
                    return false;
            }
            catch { }

            if (name.StartsWith("."))
                return false;

            if (name.TakeWhile((c) => { return !InvalidChars.Contains(c); }).Count() != name.Length)
                return false;

            return true;
        }

        private static readonly char[] InvalidChars = new char[]
        {
            '<', '>', ':', '"', '/', '\\', '|', '?', '*'
        };

        /// <summary>
        /// 使用 '/' 分隔符切割字符串并返回不包含空字符串的字符串数组
        /// </summary>
        /// <param name="path64"></param>
        /// <returns></returns>
        public static string[] SplitePath64(this string path64)
        {
            return path64.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// 获取多个等级的 Path64
        /// <para/>/yvuqgtpKTy2h22thLcRtPw/cC8K2spWSCO1BFlpEAW-kA/yvuqgtpKTy2h22thLcRtPw 返回以下数组：
        /// <para/>1. /yvuqgtpKTy2h22thLcRtPw
        /// <para/>2. /yvuqgtpKTy2h22thLcRtPw/cC8K2spWSCO1BFlpEAW-kA
        /// </summary>
        /// <param name="path64"></param>
        /// <returns></returns>
        public static string[] LevelPath64(this string path64)
        {
            var all = path64.SplitePath64();

            List<string> lis = new List<string>();
            for (int i = 0; i < all.Length - 1; i++)
                lis.Add("/" + String.Join("/", all, 0, i + 1));

            return lis.ToArray();
        }

        #endregion

        #region byte[]

        public static string ToStr(this byte[] bytes)
        {
            return UTF8Encoding.UTF8.GetString(bytes, 0, bytes.Length);
        }

        #endregion

        #region date time

        /// <summary>
        /// linq 数据库最小时间为 1753/1/1-12:0:0
        /// 这里选取文件系统还没诞生的 1900 年
        /// </summary>
        private static readonly DateTime MinDateTime = new DateTime(1900, 1, 1, 0, 0, 0);
        /// <summary>
        /// 获取合法的 linq 数据库时间
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime LinqTime(this DateTime dt)
        {
            if (dt < MinDateTime)
                return MinDateTime;
            else
                return dt;
        }

        public static DateTime ToLocalTime(long time)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                .AddMilliseconds(time).ToLocalTime();
        }

        #endregion

        #region long

        private static readonly string[] units = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        /// <summary>
        /// 格式化文件大小
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string FileSizeFormat(this long size)
        {
#if DEBUG
            if (size < 0)
                throw new ArgumentException("size must >= 0.");
#endif
            if (size <= 0)
                return "0";

            if (size < 1024)
                return size + " B";

            double k = size;
            int level = 0;
            while (k > 1024)
            {
                k /= 1024;
                level++;
            }
            return string.Format("{0:0.00} " + units[level], k);
        }

        #endregion

        /// <summary>
        /// 为每一个 FilsSystem 配置 Parent
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="parent"></param>
        public static void From(this IEnumerable<BFileSystem> collection, BObject parent)
        {
            foreach (var i in collection)
                i.From(parent);
        }

        /// <summary>
        /// #if DEBUG : 
        /// if obj == null, throw new ArgumentNullException();
        /// #else : 
        /// if obj == null, return true
        /// </summary>
        /// <param name="obj"></param>
        public static bool CheckNotNull(this object obj)
        {
            if (obj == null)
            {
#if DEBUG
                throw new ArgumentNullException();
#else
                return true;
#endif
            }
            return false;
        }
        /// <summary>
        /// #if DEBUG : 
        /// if obj == null or empty, throw new ArgumentException();
        /// #else : 
        /// if obj == null or empty, return true
        /// </summary>
        /// <param name="obj"></param>
        public static bool CheckNotEmpty(this string obj)
        {
            if (string.IsNullOrEmpty(obj))
            {
#if DEBUG
                throw new ArgumentNullException();
#else
                return true;
#endif
            }
            return false;
        }

        public static Visibility ConvToVisibility(this bool value)
        {
            if (value) return Visibility.Visible;
            else return Visibility.Collapsed;
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            var list = source.ToList();
            list.ForEach(action);
            return list;
        }
        /// <summary>
        /// 从 System.Collections.Generic.ICollection 中移除特定对象的第一个匹配项。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="items"></param>
        public static void Remove<T>(this ICollection<T> source, IEnumerable<T> items)
        {
            foreach (var i in items) source.Remove(i);
        }

        public static bool IsAllowStreamFile(this BObject obj)
        {
            var fs = obj as BFileSystem;
            if (fs == null) return false;

            if (fs.IsFolder) return false;

            var ext = fs.Extension.ToLower();

            switch (ext)
            {
                case "avi":
                case "mp4":
                case "mp3":
                    return true;
            }

            return false;
        }
    }
}
