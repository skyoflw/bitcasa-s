﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BObj
{
    /// <summary>
    /// 好像不大对劲，参考
    /// http://msdn.microsoft.com/zh-cn/library/system.data.linq.mapping.inheritancemappingattribute(v=vs.90).aspx
    /// </summary>
    [Table]
    [Index(Columns="Name")]
    [InheritanceMapping(Code = BObject.ObjectTypeType.FileSystem, 
        Type = typeof(BFileSystem), IsDefault = true)]
    [InheritanceMapping(Code = BObject.ObjectTypeType.Root, Type = typeof(BRoot))]
    [DataContract]
    public abstract class BObject : INotifyDataChanged<BObject>
    {
        #region member

        [DataMember(Name = "mtime")]
        private long __mtime;
        private DateTime __modifyTime;
        [Column(Name = "ModifyTime")]
        public DateTime ModifyTime
        {
            get
            {
                if (__mtime > 0) // 说明是刚从网络上传输过来的
                {
                    ModifyTime = ExtMethod.ToLocalTime(__mtime);
                    __mtime = 0;
                }
                return __modifyTime.LinqTime();
            }
            set { __modifyTime = value; }
        }

        [DataMember(Name = "category")]
        private string __category_str;
        private CategoryType __category;
        [Column(Name = "Category")]
        public CategoryType Category
        {
            get
            {
                if (__category_str != null)
                {
                    this.Category = CategoryTypeFromString(__category_str);
                    __category_str = null;
                }
                return __category;
            }
            set { __category = value; }
        }

        [Column(Name = "Name")]
        [DataMember(Name = "name")]
        public string Name;

        [Column(Name = "IsMirrored")]
        [DataMember(Name = "mirrored")]
        public bool IsMirrored;

        /// <summary>
        /// Path base64。
        /// 对象的主键。
        /// <para/>类似于 /yvuqgtpKTy2h22thLcRtPw/cC8K2spWSCO1BFlpEAW-kA
        /// </summary>
        [Column(Name = "Path64", IsPrimaryKey = true, CanBeNull = false)]
        [DataMember(Name = "path")]
        public string Path64;

        [Column(Name = "Type")]
        [DataMember(Name = "type")]
        public FileSystemType Type;

        public bool IsFolder { get { return Type == FileSystemType.Folder; } }

        public bool IsFile { get { return Type == FileSystemType.File; } }

        #endregion

        #region db about

        [Column(IsDiscriminator = true)]
        public ObjectTypeType ObjectType;

        public enum ObjectTypeType : int
        {
            Root = 0, 
            FileSystem = 1
        }

        #endregion

        private static CategoryType CategoryTypeFromString(string str)
        {
            switch (str)
            {
                case "folders":
                    return CategoryType.Folders;
                case "documents":
                    return CategoryType.Documents;
                case "photos":
                    return CategoryType.Photos;
                case "icons":
                    return CategoryType.Icons;
                case "musics":
                    return CategoryType.Music;
                case "videos":
                    return CategoryType.Videos;
                case "other":
                    return CategoryType.Other;
#if DEBUG
                default:
                    throw new NotImplementedException();
#endif
            }
            return CategoryType.Other;
        }

        public enum CategoryType : int
        {
            Folders = 0,

            Documents = 1,

            Photos = 2,

            Icons = 3,

            Music = 4,

            Videos = 5,

            Other = 6
        }

        protected void OnDataChanged()
        {
            if (DataChanged != null)
                DataChanged(this, null);
        }
        public event EventHandler DataChanged;

        public void Update(BObject obj)
        {
            this.Category = obj.Category;
            this.Name = obj.Name;
            this.IsMirrored = obj.IsMirrored;
            this.ModifyTime = obj.ModifyTime;
            this.Path64 = obj.Path64;
            this.Type = obj.Type;

            OnDataChanged();
        }
    }
}
