﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BObj
{
    public interface INotifyDataChanged<T>
    {
        event EventHandler DataChanged;

        void Update(T item);
    }
}
