﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BObj
{
    public enum FileSystemType : int
    {
        File = 0,
        Folder = 1
    }
}
