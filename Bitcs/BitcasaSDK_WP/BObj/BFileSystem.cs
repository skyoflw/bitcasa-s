﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace BitcasaSDK_WP.BObj
{
    [DataContract]
    [Index(Columns = "Parent64")]
    [Index(Columns = "Id")]
    public class BFileSystem : BObject, INotifyDataChanged<BFileSystem>
    {
        #region birth time

        [DataMember(Name = "birth_time")]
        private long __birth_time;
        private DateTime __createTime;
        /// <summary>
        /// from birth time, for when the filesystem be create
        /// </summary>
        [Column(Name = "CreateTime", CanBeNull = true)]
        public DateTime CreateTime
        {
            get
            {
                if (__birth_time > 0)
                {
                    CreateTime = ExtMethod.ToLocalTime(__birth_time);
                    __birth_time = 0;
                }
                return __createTime.LinqTime();
            }
            set { __createTime = value; }
        }

        #endregion

        #region ctime

        [DataMember(Name = "ctime")]
        private long __ctime;
        private DateTime __changeTime;
        /// <summary>
        /// 获取或设置内容更改时间
        /// </summary>
        [Column(Name = "ChangeTime", CanBeNull = true)]
        public DateTime ChangeTime
        {
            get
            {
                if (__ctime > 0) // 说明是刚从网络上传输过来的
                {
                    ChangeTime = ExtMethod.ToLocalTime(__ctime);
                    __ctime = 0;
                }
                return __changeTime.LinqTime();
            }
            set { __changeTime = value; }
        }

        #endregion
        
        /// <summary>
        /// Parent Path base64
        /// </summary>
        [Column(Name = "Parent64")]
        public string Parent64;

        #region just file member

        /// <summary>
        /// [文件专属]
        /// </summary>
        [Column(Name = "Album")]
        [DataMember(Name = "album")]
        public string Album;

        /// <summary>
        /// [文件专属]
        /// </summary>
        [Column(Name = "Extension")]
        [DataMember(Name = "extension")]
        public string Extension;

        /// <summary>
        /// [文件专属]
        /// </summary>
        //[Column(Name = "duplicates")]
        [DataMember(Name = "duplicates")]
        private List<object> __duplicates;

        /// <summary>
        /// [文件专属]
        /// </summary>
        [Column(Name = "ManifestName")]
        [DataMember(Name = "manifest_name")]
        public string ManifestName;

        /// <summary>
        /// [文件专属]
        /// </summary>
        [DataMember(Name = "mime")]
        private string __mime;

        private string __id;
        /// <summary>
        /// [文件专属] 文件在云端的唯一 Id
        /// </summary>
        [Column(Name = "Id")]
        [DataMember(Name = "id")]
        public string Id
        {
            get { return __id; }
            set
            {
                if (__id != value)
                {
                    __id = value;
                    OnIdChanged();
                }
            }
        }

        public bool IsListenIdChanged = false;
        private void OnIdChanged()
        {
            if (IsListenIdChanged && IdChanged != null)
                IdChanged(this, null);
        }
        /// <summary>
        /// Id changed mean you should delete file!
        /// </summary>
        public event EventHandler IdChanged;

        /// <summary>
        /// [文件专属] get is incomplete
        /// </summary>
        [Column(Name = "Incomplete", CanBeNull = true)]
        [DataMember(Name = "incomplete")]
        public bool IsIncomplete;

        /// <summary>
        /// [Only File] get file size
        /// </summary>
        [Column(Name = "Size", CanBeNull = true)]
        [DataMember(Name = "size")]
        public long Size;

        #endregion

        public void Update(BFileSystem fs)
        {
            this.CreateTime = fs.CreateTime;
            this.ChangeTime = fs.ChangeTime;

            this.Album = fs.Album;
            this.Extension = fs.Extension;
            this.ManifestName = fs.ManifestName;
            this.Id = fs.Id;
            this.IsIncomplete = fs.IsIncomplete;
            this.Size = fs.Size;

            base.Update(fs);
        }

        #region valid

        /// <summary>
        /// 判断文件名/文件夹名是否有效
        /// doc from: https://developer.bitcasa.com/docs/api
        /// In the future we may relax the restrictions, 
        /// however at the moment, 
        /// the API has very strict requirements in an attempt to ensure no problems are caused in other clients.
        /// 1. All folder/file names are case insensitive, 
        ///    however the filesystem will present the folder/file name in the original case when possible.
        /// 1. 所有文件文件夹名都是不敏感的，但是不管怎样都会通过 / 符号区分它们
        /// 2. Limit the folder/file name to 64 characters.
        ///    A path composed of folder and file names may be greater than 64 characters.
        /// 2. 限制每个名字都在 64 个字符以内，每个路径总长度没有限制
        /// 3. Names may not start with "." 
        /// 4. Names may not contain any of the following characters: < > : " / \ | ? *
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool IsValid(string name)
        {
#if DEBUG
            if (name == null)
                throw new ArgumentNullException("BFileSystem_IsValid(): name can not be null.");
#endif

            if (String.IsNullOrWhiteSpace(name))
                return false;

            try
            {
                var ns = name.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                if (ns.First((s) => { return s.Length > 64; }) != null)
                    return false;
            }
            catch { }

            if (name.StartsWith("."))
                return false;

            if (name.TakeWhile((c) => { return !InvalidChars.Contains(c); }).Count() != name.Length)
                return false;

            return true;
        }
        private static readonly char[] InvalidChars = new char[]
        {
            '<', '>', ':', '"', '/', '\\', '|', '?', '*'
        };

        #endregion

        public void From(BObject parent)
        {
            this.Parent64 = parent.Path64;
        }
    }
}
