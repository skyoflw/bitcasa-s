﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BObj
{
    [DataContract]
    public class BUserProfile
    {
        public BUserProfile()
        {
            Storage = new BStorage();
        }

        [DataMember(Name = "storage")]
        public BStorage Storage { get; set; }

        [DataMember(Name = "referral_link")]
        public string ReferralLink { get; set; }

        [DataMember(Name = "display_name")]
        public string DisplayName { get; set; }

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataContract]
        public class BStorage
        {
            [DataMember(Name = "total")]
            public long Total { get; set; }

            [DataMember(Name = "display")]
            public string Display { get; set; }

            [DataMember(Name = "used")]
            public long Used { get; set; }

            /// <summary>
            /// 是否无限
            /// </summary>
            public bool IsInfinite
            {
                get
                {
                    if (Display == null) return false;
                    return Display.Contains("Infinite");
                }
            }

            /// <summary>
            /// 使用百分比
            /// </summary>
            public double UsedPercent
            {
                get
                {
                    if (Total <= 0 || Used <= 0) return 0;
                    return Convert.ToDouble(Used) / Convert.ToDouble(Total);
                }
            }
        }
    }
}
