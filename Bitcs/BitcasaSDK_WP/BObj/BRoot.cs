﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BObj
{
    [DataContract]
    public class BRoot : BObject, INotifyDataChanged<BRoot>
    {
        #region member

        [DataMember(Name = "mount_point")]
        [Column(Name = "MountPoint")]
        public string MountPoint;

        [DataMember(Name = "deleted")]
        [Column(Name = "IsDeleted", CanBeNull = true)]
        public bool IsDeleted;

        [DataMember(Name = "origin_device")]
        [Column(Name = "OriginDevice")]
        public string OriginDevice;

        [DataMember(Name = "origin_device_id")]
        [Column(Name = "OriginDeviceId")]
        public string OriginDeviceId;

        [DataMember(Name = "sync_type")]
        private string __sync_type;
        private SyncTypeType __syncType;
        [Column(Name = "SyncType", CanBeNull = true)]
        public SyncTypeType SyncType
        {
            get
            {
                if (__sync_type != null)
                {
                    SyncType = SyncTypeTypeFromString(__sync_type);
                    __sync_type = null;
                }
                return __syncType;
            }
            set { __syncType = value; }
        }

        #endregion

        private static SyncTypeType SyncTypeTypeFromString(string str)
        {
            switch (str)
            {
                case "backup":
                    return SyncTypeType.Backup;
                case "infinite drive":
                    return SyncTypeType.InfiniteDrive;
                case "regular":
                    return SyncTypeType.Regular;
                case "sync":
                    return SyncTypeType.Sync;
                case "mirrored_folder":
                    return SyncTypeType.MirroredFolder;
                case "device":
                    return SyncTypeType.Device;
                default:
                    throw new NotImplementedException("BRoot_SyncTypeTypeFromString()");
            }
        }

        public enum SyncTypeType : int
        {
            Backup = 0,
            InfiniteDrive = 1,
            Regular = 2,
            Sync = 3,
            MirroredFolder = 4,
            Device = 5
        }

        public void Update(BRoot root)
        {
            this.MountPoint = root.MountPoint;
            this.IsDeleted = root.IsDeleted;
            this.OriginDevice = root.OriginDevice;
            this.OriginDeviceId = root.OriginDeviceId;
            this.SyncType = root.SyncType;

            base.Update(root);
        }
    }
}
