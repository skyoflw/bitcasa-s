﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BObj
{
    public static class BFileSystemHelper
    {
        /// <summary>
        /// 参数获取父目录的 PathBase64
        /// 如果没有当前 Path 没有 "/" 或只有一个 "/" 返回 null
        /// </summary>
        /// <returns></returns>
        public static string GenerateParent(this BFileSystem fs)
        {
            var index = fs.Path64.LastIndexOf('/');
            if (index > 0)
                return fs.Path64.Substring(0, index);
#if DEBUG
            throw new ArgumentException();
#else
            return null;
#endif
        }
    }
}
