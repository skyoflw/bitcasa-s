﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BObj
{
    public sealed class BitcasaUploadStream : Stream
    {
        private const string CRLF = "\r\n";
        private const string TWO_HYPHENS = "--";
        private const string BOUNDARY = "----------*****----------bitcasa";

        /// <summary>
        /// if isOwner was true, baseStream will dispose when this dispose.
        /// </summary>
        /// <param name="baseStream"></param>
        /// <param name="fileName"></param>
        /// <param name="isOwner">if true, baseStream will dispose when this dispose.</param>
        public BitcasaUploadStream(Stream baseStream, string fileName, bool isOwner)
        {
            if (!baseStream.CanRead)
                throw new ArgumentException("baseStream can not read");

            this.Boundary = BOUNDARY;

            this.Header = new MemoryStream();
            {
                byte[] tmp;

                tmp = (TWO_HYPHENS + Boundary + CRLF).ToUtf8();
                Header.Write(tmp, 0, tmp.Length);

                tmp = "Content-Disposition: form-data; name=\"file\"; filename=\"".ToUtf8();
                Header.Write(tmp, 0, tmp.Length);

                tmp = fileName.ToUtf8();
                Header.Write(tmp, 0, tmp.Length);

                tmp = ("\"" + CRLF).ToUtf8();
                Header.Write(tmp, 0, tmp.Length);

                tmp = (CRLF).ToUtf8();
                Header.Write(tmp, 0, tmp.Length);
            }

            this.Tail = new MemoryStream();
            {
                byte[] tmp;

                tmp = (CRLF).ToUtf8();
                Tail.Write(tmp, 0, tmp.Length);

                tmp = (TWO_HYPHENS + Boundary + TWO_HYPHENS + CRLF).ToUtf8();
                Tail.Write(tmp, 0, tmp.Length);
            }

            this.InnerStream = baseStream;

            this.IsOwner = isOwner;
        }

        public string Boundary { get; private set; }

        private bool IsOwner { get; set; }

        private MemoryStream Header;

        private Stream InnerStream;

        private MemoryStream Tail;

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void Flush()
        {
            throw new NotSupportedException();
        }

        public override long Length
        {
            get { return Header.Length + InnerStream.Length + Tail.Length; }
        }

        private long _position;
        public override long Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int totolRead = 0;

            if (this.Position > this.Header.Length + this.InnerStream.Length)
            {
                totolRead += this.Tail
                        .Read(buffer, offset + totolRead, count - totolRead);
            }
            else if (this.Position > this.Header.Length)
            {
                totolRead += this.InnerStream
                        .Read(buffer, offset + totolRead, count - totolRead);

                if (totolRead < count)
                    totolRead += this.Tail
                        .Read(buffer, offset + totolRead, count - totolRead);
            }
            else
            {
                totolRead += this.Header
                        .Read(buffer, offset + totolRead, count - totolRead);

                if (totolRead < count)
                    totolRead += this.InnerStream
                        .Read(buffer, offset + totolRead, count - totolRead);

                if (totolRead < count)
                    totolRead += this.Tail
                        .Read(buffer, offset + totolRead, count - totolRead);
            }

            return totolRead;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            try
            {
                this.Header.Dispose();
                if (this.IsOwner)
                    this.InnerStream.Dispose();
                this.Tail.Dispose();
            }
            catch (Exception)
            {
#if DEBUG
                throw;
#endif
            }
        }
    }
}
