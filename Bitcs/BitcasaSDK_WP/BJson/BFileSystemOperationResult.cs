﻿using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    public class BFileSystemOperationResult : BFileSystem
    {
        [DataMember(Name = "status")]
        private string __status;
        /// <summary>
        /// 获取操作结果的状态
        /// </summary>
        public StatusType Status
        {
            get
            {
                switch (__status)
                {
                    case "existing":
                        return StatusType.Existing;
                    case "created":
                        return StatusType.Created;
                    default:
                        throw new NotImplementedException(
                            "BFileSystemOperationResult_Status");
                }
            }
        }
    }

    public enum StatusType
    {
        /// <summary>
        /// 表示已经存在
        /// </summary>
        Existing,

        /// <summary>
        /// 表示创建成功
        /// </summary>
        Created
    }
}
