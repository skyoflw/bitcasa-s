﻿using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    internal class BDeleteFileSystemJson
        : BitcasaJson<BDeleteFileSystemJson, BDeleteFileSystemJson.Content>
    {
        [DataContract]
        internal class Content
        {
            [DataMember(Name = "deleted")]
            public DeleteContent Deleted { get; private set; }

            [DataMember(Name = "items")]
            public List<BFileSystem> Items { get; private set; }

            [DataContract]
            public class DeleteContent
            {
                [DataMember(Name = "num_objects")]
                public int ObjectsCount { get; private set; }

                [DataMember(Name = "size")]
                public long Size { get; private set; }
            }
        }
    }
}
