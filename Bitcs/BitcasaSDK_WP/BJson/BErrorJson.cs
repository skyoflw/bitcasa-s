﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    public class BErrorJson
    {
        [DataMember(Name = "code")]
        public int Code;

        [DataMember(Name = "message")]
        public string Message;
    }
}
