﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    internal abstract class BaseJson<T> : BaseJson
    {
        public static T FromJsonString(string text)
        {
            return ReadFromStringToObject<T>(text);
        }

        public static T FromJsonByte(byte[] json)
        {
            return ReadFromStringToObject<T>(json);
        }
    }
}
