﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    public abstract class BaseJson
    {
        [DataMember(Name = "error")]
        public BErrorJson Error;

        public override string ToString()
        {
            return WriteFromObject(this);
        }

        public byte[] ToBytes()
        {
            return Encoding.UTF8.GetBytes(this.ToString());
        }

        #region Json 静态方法

        protected static string WriteFromObject(object obj)
        {
            MemoryStream ms = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(obj.GetType());
            ser.WriteObject(ms, obj);
            byte[] json = ms.ToArray();
            ms.Dispose();
            return Encoding.UTF8.GetString(json, 0, json.Length);
        }

        protected static T ReadFromStringToObject<T>(string json)
        {
            return ReadFromStringToObject<T>(Encoding.UTF8.GetBytes(json));
        }

        protected static T ReadFromStringToObject<T>(byte[] json_utf8)
        {
            MemoryStream ms = new MemoryStream(json_utf8);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            T obj = (T)ser.ReadObject(ms);
            ms.Dispose();
            return obj;
        }

        protected static T ReadFromResponseToObject<T>(WebResponse response)
        {
            string json;
            using (StreamReader stream = new StreamReader(response.GetResponseStream()))
            {
                json = stream.ReadToEnd();
            }
            return ReadFromStringToObject<T>(json);
        }

        #endregion
    }
}
