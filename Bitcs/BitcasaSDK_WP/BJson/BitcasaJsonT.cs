﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    internal abstract class BitcasaJson<TJson, TResult> : BaseJson<TJson>
    {
        [DataMember(Name = "result")]
        public TResult Result;
    }
}
