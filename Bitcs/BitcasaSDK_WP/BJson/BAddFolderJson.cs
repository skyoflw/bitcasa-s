﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    internal class BAddFolderJson : BitcasaJson<BAddFolderJson, BAddFolderJson.Content>
    {
        [DataContract]
        internal class Content
        {
            [DataMember(Name = "items")]
            public List<BFileSystemOperationResult> Items;
        }
    }
}
