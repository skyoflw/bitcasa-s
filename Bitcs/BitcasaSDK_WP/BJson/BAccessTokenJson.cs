﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BJson
{
    [DataContract]
    internal class BAccessTokenJson : BitcasaJson<BAccessTokenJson, BAccessTokenJson.Content>
    {
        [DataContract]
        internal class Content
        {
            [DataMember(Name = "access_token")]
            public string AccessToken;
        }
    }
}
