﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP
{
    public interface ICancelled
    {
        event EventHandler Cancelled;
    }
}
