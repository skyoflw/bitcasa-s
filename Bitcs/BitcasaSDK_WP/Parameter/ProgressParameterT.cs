﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.Parameter
{
    public class ProgressParameter<T> : BParameter<T>
    {
        public Action<long> Callback_BytesChanged;
    }
}
