﻿using BitcasaSDK_WP.BConst;
using BitcasaSDK_WP.BException;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using BitcasaSDK_WP.BRequest;
using BitcasaSDK_WP.Parameter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP
{
    public class BitcasaClient
    {
        private BitcasaClient(string clientSecret, string token)
        {
            if (clientSecret == null)
                throw new ArgumentNullException(
                    "BitcasaSDK_WP_BitcasaClient(): clientSecret can not be null.");

            if (token == null)
                throw new ArgumentNullException(
                    "BitcasaSDK_WP_BitcasaClient(): token can not be null.");

            this.ClientSecret = clientSecret;
            this.Token = token;
        }

        #region member

        public string ClientSecret { get; private set; }

        public string Token { get; private set; }

        #endregion

        #region login and logout

        /// <summary>
        /// 获取登录用的 Url
        /// get auth url
        /// </summary>
        public static string AuthUrl(string clientId, string callbackUrl)
        {
            if (clientId == null)
                throw new ArgumentNullException("BitcasaSDK_WP_AuthUrl(): clientId can not be null.");

            if (String.IsNullOrWhiteSpace(callbackUrl))
                throw new ArgumentException(
                    "BitcasaClient_AuthUrl(): callbackUrl can not be null or white space.");

            return String.Format(BUrl.AUTHENTUCATION_URL, clientId, callbackUrl);
        }

        public static void LoginAsync(
            string clientSecret,
            string authorizationCode,
            Action<BitcasaClient> callback_succeeded,
            Action<BitcasaException> callback_failed)
        {
            if (authorizationCode == null)
                throw new ArgumentNullException(
                    "BitcasaSDK_WP_BitcasaClient(): authorizationCode can not be null.");

            var request = new BGrantAccessTokenRequest(
                clientSecret, authorizationCode);

            request.RequestSucceeded += (a, b) => 
            {
                if (callback_succeeded != null)
                    callback_succeeded(
                        Login(clientSecret, b)); 
            };
            request.RequestFailed += (a, b) =>
            {
                if (callback_failed != null)
                    callback_failed(b); 
            };

            request.SendRequestAsync();
        }
        public static BitcasaClient Login(string clientSecret, string token)
        {
            BitcasaClient client = 
                    new BitcasaClient(clientSecret, token);
            return client;
        }

        public bool IsLogin { get { return Token != null; } }

        public void Logout()
        {
            this.Token = null;
        }

        #endregion

        #region base request helper

        private void Request<T>(
            BAuthenticationRequest<T> request,
            BParameter<T> parameter)
        {
#if DEBUG
            if (!this.IsLogin)
                throw new ArgumentException(
                    "the client is logout!");

            if (request == null)
                throw new ArgumentNullException(
                    "request can not be null.");

            if (parameter == null)
                throw new ArgumentNullException(
                    "parameter can not be null.");
#endif

            if (parameter.CancelToken != null)
                parameter.CancelToken.Cancelled += (a, b) =>
                {
                    if (request == null)
                        return;

                    if (request.IsCancelled != true)
                        request.IsCancelled = true;
                };

            request.RequestSucceeded += (a, b) =>
            {
                if (parameter.Callback_Successed != null)
                    parameter.Callback_Successed(b);
            };
            request.RequestFailed += (a, b) =>
            {
                if (parameter.Callback_Failed != null)
                    parameter.Callback_Failed(b);
            };
            request.RequestCancelled += (a, b) =>
            {
                if (parameter.Callback_Cancelled != null)
                    parameter.Callback_Cancelled();
            };
            request.RequestCompleted += (a, b) =>
            {
                if (parameter.Callback_Completed != null)
                    parameter.Callback_Completed();
            };
            request.SendRequestAsync();
        }

        #endregion

        #region account

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="parameter"></param>
        public void GetUserProfile(BParameter<BUserProfile> parameter)
        {
            var r = new BGetUserProfileRequest(this.Token);
            Request<BUserProfile>(r, parameter);
        }

        #endregion

        #region folder requests

        public void ListRoot(BParameter<List<BRoot>> parameter)
        {
            var r = new BListRootRequest(this.Token);
            Request<List<BRoot>>(r, parameter);
        }

        public void ListRoot(
            CategoryType category,
            DepthType depth,
            int count,
            BParameter<List<BRoot>> parameter)
        {
            var r = new BListRootRequest(this.Token);
            r.Category = category;
            r.Depth = depth;
            r.Latest = count;
            Request<List<BRoot>>(r, parameter);
        }

        public void ListFolder(
            string path_base64,
            BParameter<List<BFileSystem>> parameter)
        {
            var r = new BListFolderRequest(this.Token, path_base64);
            Request<List<BFileSystem>>(r, parameter);
        }

        public void ListFolder(
            string path_base64,
            CategoryType category,
            DepthType depth,
            int count,
            BParameter<List<BFileSystem>> parameter)
        {
            var r = new BListFolderRequest(this.Token, path_base64);
            r.Category = category;
            r.Depth = depth;
            r.Latest = count;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void AddFolder(
            string path_base64,
            string newFolderName,
            BParameter<List<BFileSystemOperationResult>> parameter)
        {
            var r = new BAddFolderRequest(this.Token, path_base64, newFolderName);
            Request<List<BFileSystemOperationResult>>(
                r, parameter);
        }

        public void DeleteFolder(
            string path,
            BParameter<List<BFileSystem>> parameter)
        {
#if DEBUG
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException(
                    "path can not be null or white space.");
#endif
            var r = new BDeleteFolderRequest(this.Token, path);
            Request<List<BFileSystem>>(r, parameter);
        }

        public void RenameFolder(
            string path, string newFolderName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BRenameFolderRequest(
                this.Token, path, newFolderName);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void CopyFolder(
            string path, string targetFolderPath, 
            string newFolderName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFolderRequest(
                this.Token, path, targetFolderPath, newFolderName, false);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void MoveFolder(
            string path, string targetFolderPath,
            string newFolderName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFolderRequest(
                this.Token, path, targetFolderPath, newFolderName, true);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        #endregion

        #region file requests

        public void UploadFile(
            string dir_path,
            string file_name,
            Stream readstream,
            ProgressParameter<BFileSystem> parameter,
            ExistsStatus mode)
        {
#if DEBUG
            if (readstream == null)
                throw new ArgumentNullException("DEBUG MODE: readstream can not be null.");

            if (!readstream.CanRead)
                throw new ArgumentException("DEBUG MODE: readstream must can read.");
#endif
            var r = new BUploadFileRequest(
                this.Token,
                dir_path,
                file_name,
                readstream);
            r.Mode = mode;
            r.BytesChanged += (a, l) =>
            {
                if (parameter.Callback_BytesChanged != null)
                    parameter.Callback_BytesChanged(l);
            };
            this.Request<BFileSystem>(r, parameter);
        }

        public Uri FileUri(string path, string fileName)
        {
            var r = new BStreamFileRequest(this.Token, path, fileName);
            return r.FileUri;
        }

        public void DownloadFile(
            string path,
            string filename,
            Stream writestream,
            ProgressParameter<Stream> parameter,
            long file_length,
            long beginPos = 0,
            long endPos = 0)
        {
#if DEBUG
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException(
                    "path can not be null or white space.");
            if (String.IsNullOrWhiteSpace(filename))
                throw new ArgumentException(
                    "filename can not be null or white space.");
            if (writestream == null)
                throw new ArgumentNullException(
                    "stream can not be null.");
            if (!writestream.CanWrite)
                throw new ArgumentException(
                    "stream must can write.");
#endif
            var r = new BDownloadFileRequest(
                this.Token, path, filename, writestream);
            r.BeginPosition = beginPos;
            r.EndPosition = endPos;
            r.BytesChanged += (a, l) =>
                {
                    if (parameter.Callback_BytesChanged != null)
                        parameter.Callback_BytesChanged(l);
                };
            r.ContentLength =
                endPos > 0 ? endPos - beginPos + 1 :
                (file_length - beginPos);
            Request<Stream>(r, parameter);
        }

        public void DeleteFile(
            string path,
            BParameter<List<BFileSystem>> parameter)
        {
#if DEBUG
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException(
                    "path can not be null or white space.");
#endif
            var r = new BDeleteFileRequest(this.Token, path);
            Request<List<BFileSystem>>(r, parameter);
        }

        public void RenameFile(
            string path, string newFileName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BRenameFileRequest(
                this.Token, path, newFileName);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void CopyFile(
            string path, string targetFolderPath,
            string newFileName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFileRequest(
                this.Token, path, targetFolderPath, newFileName, false);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void MoveFile(
            string path, string targetFolderPath,
            string newFileName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFileRequest(
                this.Token, path, targetFolderPath, newFileName, true);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        #endregion
    }
}
