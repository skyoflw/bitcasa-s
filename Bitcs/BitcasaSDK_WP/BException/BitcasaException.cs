﻿using BitcasaSDK_WP.BJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK_WP.BException
{
    public class BitcasaException
    {
        public ExceptionSourceType ExceptionType { get; private set; }

        private BitcasaException(ExceptionSourceType type)
        {
            this.ExceptionType = type;
        }

        #region Web Exception

        public static BitcasaException FromWebException(WebException webException)
        {
            var exp = new BitcasaException(ExceptionSourceType.WebException);
            exp.InnerException = webException;
#if DEBUG
            if (webException.Status != WebExceptionStatus.UnknownError)
                throw new Exception("check error : ~", webException);
#endif
            return exp;
        }

        public WebException InnerException { get; private set; }

        #endregion

        #region Bitcasa Exception

        public static BitcasaException FromBitcasa(BErrorJson json)
        {
            var exp = new BitcasaException(ExceptionSourceType.FromBitcasa);
            exp.BitcasaJson = json;
            return exp;
        }

        public BErrorJson BitcasaJson { get; private set; }

        #endregion

        #region

        public static BitcasaException FromJsonParseError(string originalJson)
        {
            var exp = new BitcasaException(ExceptionSourceType.JsonParseError);
            exp.OriginalJson = originalJson;

            return exp;
        }

        public string OriginalJson { get; private set; }

        #endregion
    }

    public enum ExceptionSourceType
    {
        /// <summary>
        /// WebException.
        /// you can find more detail in InnerException.
        /// </summary>
        WebException,

        /// <summary>
        /// json parse error, can not parse json.
        /// you can find original json text in OriginalJson.
        /// </summary>
        JsonParseError,

        /// <summary>
        /// json has error value from bitcasa service.
        /// you can find more detail in BitcasaJson.
        /// </summary>
        FromBitcasa
    }

    /// <summary>
    /// Bitcasa 的异常代码
    /// </summary>
    public enum BitcasaExceptionCode : int
    {
        #region Signup and Authentication and Account Errors, 1XXX

        /// <summary>
        /// 邮箱地址已经被使用
        /// </summary>
        EmailAddressIsAlreadyInUse = 1001,

        /// <summary>
        /// 用户 UUID 不是一个有效的格式
        /// </summary>
        UserUUIDIsNotAValidFormat = 1002,

        /// <summary>
        /// 用户 UUID 已经被使用
        /// </summary>
        UserUUIDIsAlreadyInUse = 1003,

        /// <summary>
        /// 无效邮箱
        /// </summary>
        EmailIsNotValid = 1004,

        /// <summary>
        /// 邮箱地址是必要的
        /// </summary>
        EmailAddressIsRequired = 1005,

        /// <summary>
        /// 密码不匹配
        /// </summary>
        PasswordsDoNotMatch = 1006,

        /// <summary>
        /// 密码过短。必须大于 6 个字符。
        /// </summary>
        PasswordIsTooShort = 1007,

        /// <summary>
        /// 密码过长。必须小于 1024 个字符。
        /// </summary>
        PasswordIsTooLong = 1008,

        /// <summary>
        /// 密码是必要的。
        /// </summary>
        PasswordIsRequired = 1009,

        /// <summary>
        /// 密码证明是必要的
        /// </summary>
        PasswordConfirmationIsRequired = 1010,

        /// <summary>
        /// 名（姓名中的）是必要的
        /// </summary>
        FirstNameIsRequired = 1011,

        /// <summary>
        /// 姓（姓名中的）是必要的
        /// </summary>
        LastNameIsRequired = 1012,

        /// <summary>
        /// 两个挑战问题是必要的
        /// </summary>
        TwoChallengeQuestionsAreRequired = 1013,

        /// <summary>
        /// 两个挑战回答是必要的
        /// </summary>
        TwoChallengeAnswersAreRequired = 1014,

        /// <summary>
        /// 挑战问题必须不一致
        /// </summary>
        ChallengeQuestionsMustBeDifferent = 1015,

        /// <summary>
        /// 必须接受使用条款
        /// </summary>
        TermsAndConditionsMustBeAccepted = 1016,

        /// <summary>
        /// 邮箱或密码不匹配
        /// </summary>
        EmailOrPasswordDidNotMatchAnAccount = 1017,

        /// <summary>
        /// 无法创建账号
        /// </summary>
        FailedToCreateUserAccount = 1018,

        /// <summary>
        /// 用户不被认可
        /// </summary>
        UserNotRecognized = 1019,

        /// <summary>
        /// 认证码不被认可
        /// </summary>
        AuthorizationCodeNotRecognized = 1020,

        /// <summary>
        /// 客户端 Secret 不恰当
        /// </summary>
        ClientSecretIsNotCorrectForGivenClientID = 1021,

        /// <summary>
        /// 客户端 Id 不被认可
        /// </summary>
        ClientIDIsNotRecognized = 1022,

        /// <summary>
        /// 访问 token 没有找到
        /// </summary>
        AccessTokenCannotBeFound = 1023,

        /// <summary>
        /// 您的桌面客户端已经过期，请升级
        /// </summary>
        YourDesktopClientHasExpired = 1040,

        /// <summary>
        /// 您的账户已经被满了。请删除部分文件或升级。
        /// </summary>
        YourAccountIsSuspended = 1050,

        /// <summary>
        /// 账户已经被删除
        /// </summary>
        YourAccountHasBeenDeleted = 1051,

        /// <summary>
        /// 认证方法无效
        /// </summary>
        AuthenticationMethodUnavailable = 1052,

        /// <summary>
        /// 重定向 Url 不被认可
        /// </summary>
        RedirectURLIsNotRecognized = 1053,

        /// <summary>
        /// 传递了无效应答类型
        /// </summary>
        InvalidResponseTypePassed = 1054,

        /// <summary>
        /// 传递了无效授权类型
        /// </summary>
        InvalidGrantTypePassed = 1055,

        #endregion

        #region Filesystem and Folder Errors, 2XXX

        /// <summary>
        /// 清单不存在。
        /// </summary>
        ManifestDoesNotExist = 2001,

        /// <summary>
        /// 目录不存在。
        /// </summary>
        FolderDoesNotExist = 2002,

        /// <summary>
        /// 文件不存在。
        /// </summary>
        FileDoesNotExist = 2003,

        /// <summary>
        /// 无法上传。
        /// <para/>指定的目的位置为只读的
        /// </summary>
        CannotUpload = 2004,

        /// <summary>
        /// 无法移动。
        /// <para/>指定的目的位置为只读的
        /// </summary>
        CannotMove = 2005,

        /// <summary>
        /// 无法拷贝。
        /// <para/>指定的目的位置为只读的
        /// </summary>
        CannotCopy = 2006,

        /// <summary>
        /// 无法重命名。
        /// <para/>指定的本地位置为只读的
        /// </summary>
        CannotRename = 2007,

        /// <summary>
        /// 无法删除。
        /// <para/>指定的本地位置为只读的
        /// </summary>
        CannotDelete = 2008,

        /// <summary>
        /// 无法创建目录。
        /// <para/>指定的本地位置为只读的
        /// </summary>
        CannotCreateFolder = 2009,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>无法解密配置。
        /// <para/>Failed to decrypt config.
        /// </summary>
        FailedToReadFilesystem = 2010,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>无法解析配置。
        /// </summary>
        FailedToReadFilesystem_FailedToParseConfig = 2011,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>无法解密清单。
        /// </summary>
        FailedToReadFilesystem_FailedToDecryptManifest = 2012,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>无法解析清单。
        /// </summary>
        FailedToReadFilesystem_FailedToParseManifest = 2013,

        /// <summary>
        /// 创建目录时发生名字冲突
        /// </summary>
        NameConflictCreatingFolder = 2014,

        /// <summary>
        /// 上传时发生名字冲突
        /// </summary>
        NameConflictOnUpload = 2015,

        /// <summary>
        /// 重命名时发生名字冲突。
        /// </summary>
        NameConflictOnRename = 2016,

        /// <summary>
        /// 移动时发生名字冲突。
        /// </summary>
        NameConflictOnMove = 2017,

        /// <summary>
        /// 拷贝时发生名字冲突。
        /// </summary>
        NameConflictOnCopy = 2018,

        /// <summary>
        /// 无法储存更改
        /// <para/>无法创建清单。
        /// <para/>Failed to create manifest.
        /// </summary>
        FailedToSaveChanges = 2019,

        /// <summary>
        /// 无法储存更改
        /// <para/>无法创建目录。
        /// </summary>
        FailedToSaveChanges_FailedToCreateFolder = 2020,

        /// <summary>
        /// 无法储存更改
        /// <para/>无法创建文件。
        /// </summary>
        FailedToSaveChanges_FailedToCreateFile = 2021,

        /// <summary>
        /// 无法更新广播。
        /// <para/>更新公开配置失败。
        /// <para/>This error allows clients to message to users that while the changes have been made, other clients may take a while to see them.
        /// </summary>
        FailedToBroadcastUpdate_FailedToPublishConfigUpdate2 = 2022,

        /// <summary>
        /// 无法更新广播
        /// <para/>更新公开清单失败
        /// </summary>
        FailedToBroadcastUpdate_FailedToPublishManifestUpdate = 2023,

        /// <summary>
        /// 无法储存更改
        /// <para/>储存配置失败。
        /// </summary>
        FailedToSaveChanges_FailedToSaveConfig = 2024,

        /// <summary>
        /// 无法储存更改
        /// <para/>储存清单失败。
        /// </summary>
        FailedToSaveChanges_FailedToSaveManifest = 2025,

        /// <summary>
        /// 无法删除主存储器（Infinite Drive）
        /// </summary>
        CannotDeleteTheInfiniteDrive = 2026,

        /// <summary>
        /// 缺乏 "From" 参数。
        /// </summary>
        MissingFromParameter = 2027,

        /// <summary>
        /// 缺乏 "To" 参数。
        /// </summary>
        MissingToParameter = 2028,

        /// <summary>
        /// 缺乏 "Filename" 参数。
        /// </summary>
        MissingFilenameParameter = 2029,

        /// <summary>
        /// "Filename" 参数无效。
        /// </summary>
        FilenameIsInvalid = 2030,

        /// <summary>
        /// "Folder_Name" 参数无效
        /// </summary>
        FolderNameIsInvalid = 2031,

        /// <summary>
        /// 文件参数不包含一个文件
        /// </summary>
        FileParameterDoesNotContainAFile = 2032,

        /// <summary>
        /// 存在无效参数。
        /// </summary>
        ExistsParameterIsInvalid = 2033,

        /// <summary>
        /// 路径是必须的。
        /// </summary>
        APathIsRequired = 2034,

        /// <summary>
        /// 组参数无效。
        /// </summary>
        CategoryParameterIsInvalid = 2035,

        /// <summary>
        /// 指定位置为只读的。
        /// </summary>
        SpecifiedLocationIsReadOnly = 2036,

        /// <summary>
        /// 指定源为只读的。
        /// </summary>
        SpecifiedSourceIsReadOnly = 2037,

        /// <summary>
        /// 指定目的位置为只读的。
        /// </summary>
        SpecifiedDestinationIsReadOnly = 2038,

        /// <summary>
        /// 给出的路径不存在。
        /// </summary>
        PathGivenIsDoesNotExist = 2039,

        /// <summary>
        /// 权限拒绝。
        /// </summary>
        PermissionDenied = 2040,

        /// <summary>
        /// 无法重命名。
        /// <para/>权限拒绝。
        /// <para/>无法重命名清单。
        /// </summary>
        CannotRename_PermissionDenied_ManifestCannotBeRenamed = 2041,

        /// <summary>
        /// 操作时发生名字冲突。
        /// </summary>
        NameConflictOnOperation = 2042,

        /// <summary>
        /// 无法进行播客更新。
        /// <para/>更新公开配置失败。
        /// </summary>
        FailedToBroadcastUpdate_FailedToPublishConfigUpdate = 2043,

        /// <summary>
        /// 无效数据错误。
        /// </summary>
        InvalidDataError = 2044,

        /// <summary>
        /// 目录没有找到。
        /// </summary>
        FolderNotFound = 2045,

        /// <summary>
        /// 目录没有找到。
        /// <para/>清单类型无效。
        /// </summary>
        FolderNotFound_ManifestTypeInvalid = 2046,

        /// <summary>
        /// 给出的文件名包含无效字符。
        /// </summary>
        FilenameGivenContainsInvalidCharacters = 2047,

        /// <summary>
        /// 文件名过长。
        /// </summary>
        FilenameGivenTooLong = 2048,

        /// <summary>
        /// 给出的文件名包含 '.'
        /// </summary>
        FilenameGivenStartsWithADot = 2049,

        /// <summary>
        /// 给出的文件名包含无效字符。
        /// </summary>
        FilenameGivenContainsInvalidCharacter = 2050,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>无法解密数据。
        /// </summary>
        FailedToReadFilesystem_FailedToDecryptData = 2051,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>无法解密数据。
        /// </summary>
        FailedToReadFilesystem_FailedToDecryptData2 = 2052,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>遭遇块列表错误。
        /// </summary>
        FailedToReadFilesystem_BlockListErrorEncountered = 2053,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>遭遇块列表索引错误。
        /// </summary>
        FailedToReadFilesystem_BlockListIndexErrorEncountered = 2054,

        /// <summary>
        /// 无法读取文件系统。
        /// <para/>块列表为空。
        /// </summary>
        FailedToReadFilesystem_BlockListEmpty = 2055,

        /// <summary>
        /// 对给出的文件系统对象进行了无效的操作。
        /// </summary>
        InvalidOperationForGivenFilesystemElement = 2056,

        /// <summary>
        /// 文件参数是必须的。
        /// </summary>
        FileParameterIsRequired = 2057,

        #endregion

        #region File Errors, 3XXX

        /// <summary>
        /// 文件没有找到
        /// </summary>
        FileNotFound = 3001,

        /// <summary>
        /// 文件 Id 无效
        /// </summary>
        FileIdIsInvalid = 3002,

        /// <summary>
        /// 文件块没有找到
        /// </summary>
        FileBlockNotFound = 3003,

        /// <summary>
        /// 请求的范围无效
        /// </summary>
        RangeRequestIsInvalid = 3004,

        /// <summary>
        /// 输出格式不支持
        /// </summary>
        OutputFormatNotSupported = 3005,

        /// <summary>
        /// 请求的分辨率不被支持
        /// <para/>Bitcasa 没有该分辨率的缩略图及预览
        /// <para/>Indicates we do not thumbnail/preview to the request size.
        /// </summary>
        OutputResolutionNotSupported = 3006,

        /// <summary>
        /// 遇到一个传输错误。
        /// <para/>
        /// <para/>A transcoding job error has been encountered.
        /// </summary>
        ATranscodingErrorHasBeenEncountered = 3012,

        /// <summary>
        /// 遇到一个传输错误。
        /// <para/>
        /// </summary>
        ATranscodingErrorHasBeenEncountered_ATranscodingCommandErrorHasBeenEncountered = 3013,

        /// <summary>
        /// 遇到一个传输错误。
        /// <para/>一个 DRM 错误发生。
        /// </summary>
        ATranscodingErrorHasBeenEncountered_ADRMErrorHasBeenEncountered = 3014,

        #endregion

        #region Share Errors, 4XXX

        /// <summary>
        /// 发生了一个未指定的分享错误
        /// </summary>
        AnUnspecifiedSharingErrorOccurred = 4000,

        /// <summary>
        /// 被选择的文件没有找到
        /// </summary>
        AFileWithinTheSelectionNotFound,

        /// <summary>
        /// 密码不正确
        /// </summary>
        PasswordIncorrect,

        /// <summary>
        /// 无法删除。
        /// <para/>用户不是所有者。
        /// </summary>
        CannotDelete_UserDoesNotOwnShare,

        #endregion

        #region GeneralErrors, 9XXX
        
        /// <summary>
        /// 未认证的。用户必须登录。
        /// <para/>error code : 9001
        /// </summary>
        Unauthorized = 9001,

        /// <summary>
        /// 禁止的，用户不能进行此活动
        /// </summary>
        Forbidden = 9002,

        /// <summary>
        /// 数据库无法到达
        /// </summary>
        DatabaseUnreachable = 9003,

        /// <summary>
        /// 缓存无法到达
        /// </summary>
        CacheUnreachable = 9004,

        /// <summary>
        /// 存储器无法到达
        /// </summary>
        StorageUnreachable = 9005,

        /// <summary>
        /// API 限制到达
        /// </summary>
        ApiRateLimitReached = 9006,

        /// <summary>
        /// 应用程序没有认证去访问此活动
        /// </summary>
        ApplicationNotAuthorizedToPerformThisAction = 9007,

        /// <summary>
        /// 一个为桌面客户端保留的错误代码
        /// </summary>
        CatchAll = 9999,

        #endregion
    }
}
