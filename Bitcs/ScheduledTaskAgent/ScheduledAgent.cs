﻿using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Scheduler;
using AutoUpload;
using BitcasaSDK_WP;
using BSetting;
using System.Threading;
using BitcasaSDK_WP.BRequest;

namespace ScheduledTaskAgent
{
    public class ScheduledAgent : Microsoft.Phone.Scheduler.ScheduledTaskAgent
    {
        /// <remarks>
        /// ScheduledAgent 构造函数，初始化 UnhandledException 处理程序
        /// </remarks>
        static ScheduledAgent()
        {
            // 订阅托管的异常处理程序
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        /// 出现未处理的异常时执行的代码
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // 出现未处理的异常；强行进入调试器
                Debugger.Break();
            }
        }

        /// <summary>
        /// 运行计划任务的代理
        /// </summary>
        /// <param name="task">
        /// 调用的任务
        /// </param>
        /// <remarks>
        /// 调用定期或资源密集型任务时调用此方法
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            //TODO: 添加用于在后台执行任务的代码
            var token = Setting.Token;
            if (token == null)
            {
                Setting.IsEnableAutoUpload = false;
                Abort();
                return;
            }
            var path = Setting.AutoUploadFolder;
            if (path == null)
            {
                NotifyComplete();
                return;
            }

            Setting.AutoUploadAsSystemSeriveRunTime++;
            var waiter = new AutoResetEvent(false);
            var client = BitcasaClient.Login(AppInfo.ClientSecret, token);
            UploadMode mode = Setting.IsAutoUploadAllPhoto ?
                UploadMode.AllPhoto : UploadMode.OnlyCameraRoll;
            var m = new AutoUploadManager(client, mode, path);
            m.ExistsMode = (ExistsStatus)Setting.UploadOption.UploadOptionExistsMode;
            m.ManagerStop += (a, b) => { waiter.Set(); };
            m.RunAsync();
            waiter.WaitOne();

            NotifyComplete();
        }
    }
}