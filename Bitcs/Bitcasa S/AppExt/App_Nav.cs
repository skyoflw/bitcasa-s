﻿using Bitcasa_S.Model;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Bitcasa_S
{
    public partial class App : Application
    {
        public static void NavToMain()
        {
            App.RootFrame.Navigate(new Uri("/Page/MainPage.xaml", UriKind.Relative));
        }

        public static void NavToDoor()
        {
            App.RootFrame.Navigate(new Uri("/Page/SelectAuthPage.xaml", UriKind.Relative));
        }

        public static void NavToAuth()
        {
            App.RootFrame.Navigate(new Uri("/Page/AuthPage.xaml", UriKind.Relative));
        }

        public static void NavToOption()
        {
            App.RootFrame.Navigate(new Uri("/Page/OptionPage.xaml", UriKind.Relative));
        }
    }
}
