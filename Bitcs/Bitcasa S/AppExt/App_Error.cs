﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Coding4Fun.Toolkit.Controls;
using Bitcasa_S.Resources;
using System.Threading;
using BitcasaSDK_WP.BException;

namespace Bitcasa_S
{
    public partial class App : Application
    {
        /// <summary>
        /// 应用的错误处理器
        /// </summary>
        public class ErrorHandler
        {
            /// <summary>
            /// 默认的处理错误方案
            /// 逻辑：
            /// 1. 如果是认证错误，跳转到认证页面
            /// 2. 显示错误信息
            /// </summary>
            public static void DefaultErrorHandler(BitcasaException e, string operationName, bool isMessageBox = false)
            {
#if DEBUG
                if (operationName == null) throw new ArgumentNullException();
#endif
                if (CatchAuthError(e)) return;

                ShowToast(e, operationName, isMessageBox);
            }

            /// <summary>
            /// 获取是否认证错误。
            /// <para/>如果是，返回 true，并且此方法能自动跳转到登录界面，
            /// <para/>否则返回 false
            /// </summary>
            /// <param name="e"></param>
            /// <returns></returns>
            public static bool CatchAuthError(BitcasaException e)
            {
                if (e == null) return false;

                if (e.ExceptionType != ExceptionSourceType.FromBitcasa) return false;

                if ((BitcasaExceptionCode)e.BitcasaJson.Code == BitcasaExceptionCode.Unauthorized)
                {
                    BSetting.Setting.Account.IsLastLogin = true;
                    App.ShowToast(e.BitcasaJson.Message);
                    //App.ShowMessageBox(e.BitcasaJson.Message);
                    App.RootFrame.Dispatcher.BeginInvoke(() => NavToAuth());
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 委托显示 Toast
            /// </summary>
            /// <param name="e"></param>
            /// <param name="operation">操作名</param>
            /// <param name="isMessageBox">是否使用 MessageBox 显示</param>
            public static void ShowToast(BitcasaException e, string operation, bool isMessageBox = false)
            {
#if DEBUG
                if (operation == null) throw new ArgumentNullException();
#else
                if (operation == null) return;
#endif
                string msg = null;

                if (e != null)
                {
                    if (e.ExceptionType == ExceptionSourceType.FromBitcasa)
                    {
                        msg = e.BitcasaJson.Message;
#if DEBUG
                        throw new NotImplementedException();
#endif
                    }
                    else if (e.ExceptionType == ExceptionSourceType.JsonParseError)
                        msg = AppResources.ErrorMessage_DataError;
                    else
                        msg = AppResources.ErrorMessage_NetworkError;
                }
                else
                    msg = AppResources.ErrorMessage_NetworkError;

                var resultMsg = String.Format(AppResources.ErrorMessage_Title, operation, msg);

                if (isMessageBox) App.ShowMessageBox(resultMsg);
                else App.ShowToast(resultMsg);
            }
        }

        /// <summary>
        /// 委托显示 Toast
        /// </summary>
        /// <param name="message">正文</param>
        public static void ShowToast(string message)
        {
            if (App.RootFrame == null) return;

            App.RootFrame.Dispatcher.BeginInvoke(() =>
                {
                    ToastPrompt p = new ToastPrompt();
                    p.Message = message;
                    p.Show();
                });
        }
        /// <summary>
        /// 委托显示 Toast
        /// </summary>
        /// <param name="caption">标题</param>
        /// <param name="message">正文</param>
        public static void ShowToast(string caption, string message)
        {
            if (App.RootFrame == null) return;

            App.RootFrame.Dispatcher.BeginInvoke(() =>
                {
                    ToastPrompt p = new ToastPrompt();
                    p.Title = caption;
                    p.Message = message;
                    p.Show();
                });
        }

        /// <summary>
        /// 委托显示只有确定按钮的 MessageBox
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowMessageBox(string msg)
        {
            if (App.RootFrame == null) return;

            App.RootFrame.Dispatcher.BeginInvoke(() =>
                MessageBox.Show(msg)
            );
        }

        /// <summary>
        /// 显示没有实现的对话框
        /// </summary>
        public static void ShowNotRealizeError()
        {
            ShowMessageBox(AppResources.ErrorMessage_NotRealize);
        }
    }
}
