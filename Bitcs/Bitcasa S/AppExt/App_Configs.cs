﻿using Bitcasa_S.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Bitcasa_S
{
    public partial class App : Application
    {
        public static void ConfigPage(PhoneApplicationPage page)
        {
            ConfigTiltEffect(page); // 开启按钮效果
            UpdateAppBarButtonText(page);
        }

        /// <summary>
        /// 本地化应用栏
        /// </summary>
        /// <param name="page"></param>
        public static void UpdateAppBarButtonText(PhoneApplicationPage page)
        {
            // 更新应用栏按钮文本
            if (page.ApplicationBar == null)
                return;

            var bs = page.ApplicationBar.Buttons;
            for (int i = 0; i < bs.Count; i++)
            {
                var b = bs[i] as ApplicationBarIconButton;
                switch (b.Text)
                {
                    case "upload photo":
                        b.Text = AppResources.ApplicationBar_Button_UploadPhoto;
                        break;
                    case "create folder":
                        b.Text = AppResources.ApplicationBar_Button_CreateFolder;
                        break;
                    case "delete":
                    case "delete select":                    
                        b.Text = AppResources.ApplicationBar_Button_DeleteSelect;
                        break;
                    default:
                        break;
                }
            } 
            
            var mis = page.ApplicationBar.MenuItems;
            for (int i = 0; i < mis.Count; i++)
            {
                var m = mis[i] as ApplicationBarMenuItem;
                switch (m.Text)
                {
                    case "refresh":
                        m.Text = AppResources.ApplicationBar_MenuItem_Refresh;
                        break;
                    case "clear select":
                        m.Text = AppResources.ApplicationBar_MenuItem_ClearSelect;
                        break;
                    case "copy select":
                        m.Text = AppResources.ApplicationBar_MenuItem_CopySelect;
                        break;
                    case "cut select":
                        m.Text = AppResources.ApplicationBar_MenuItem_CutSelect;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 配置按钮倾斜特效
        /// </summary>
        /// <param name="obj"></param>
        public static void ConfigTiltEffect(DependencyObject obj)
        {
            Microsoft.Phone.Controls.TiltEffect.SetIsTiltEnabled(obj, true); // 开启按钮效果
        }
    }
}
