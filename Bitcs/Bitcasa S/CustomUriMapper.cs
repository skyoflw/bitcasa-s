﻿using BitcasaSDK_WP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Bitcasa_S
{
    public class CustomUriMapper : UriMapperBase
    {
        public override Uri MapUri(Uri uri)
        {
#if DEBUG
            //return new Uri(AppLockPage, UriKind.Relative);
#endif
            if (AppCore.Password.Islock) return new Uri(AppLockPage, UriKind.Relative);

            string tempUri = uri.ToString();
            //string mappedUri = null;

            if (AppCore.Token == null)
            {
                var mappedUri1 = "/Page/SelectAuthPage.xaml";
                var mappedUri2 = "/Page/AuthPage.xaml";
                if (uri.OriginalString != mappedUri1 &&
                    uri.OriginalString != mappedUri2)
                    return new Uri(mappedUri1, UriKind.Relative);

                return uri;
            }

            if (AppCore.Client == null)
                AppCore.Client = BitcasaClient.Login(AppCore.ClientSecret, AppCore.Token);

            // 当程序从照片库中点击共享时为真
            //if ((tempUri.Contains("ShareContent")) && (tempUri.Contains("FileId")))
            //{
            //    mappedUri = tempUri.Replace("AuthPage", "SyncFolderPage");
            //    return new Uri(mappedUri, UriKind.Relative);
            //}

            // Otherwise perform normal launch.

            if (tempUri.Contains("?Action=ConfigurePhotosUploadSettings"))
            {
                // 从照片自动上载页面点击进来
                IsFrom_ConfigurePhotosUploadSettings = true;
                return new Uri(Option.AutoUploadUrl, UriKind.Relative);
            }

            return uri;
        }

        private const string AccountPage = "/Page/AccountPage.xaml";

        private const string AppLockPage = "/Page/AppLockPage.xaml";

        private const string FolderPage = "/Page/FolderPage.xaml";

        private const string Format1 = "{0}?{1}={2}";

        private const string ArgsPath = "path";

        public static void NavToXaml(string url)
        {
            App.RootFrame.Navigate(new Uri(url, UriKind.Relative));
        }

        /// <summary>
        /// 导航到上传下载任务管理页面
        /// </summary>
        public static void NavToTaskManagerPage()
        {
            var taskManagerPage = "/Page/TaskManagerPage.xaml";
            NavToXaml(taskManagerPage);
        }

        /// <summary>
        /// 导航到缓存页面
        /// </summary>
        public static void NavToCachePage()
        {
            var cachePage = "/Page/CachePage.xaml";
            NavToXaml(cachePage);
        }
        
        /// <summary>
        /// 导航到账户页面
        /// </summary>
        public static void NavToAccountPage()
        {
            NavToXaml(AccountPage);
        }

        public static string GetFolderPageUrl(string path64)
        {
            if (path64.CheckNotEmpty()) return null;

            return String.Format(Format1, FolderPage, ArgsPath, path64);
        }

        public static void NavToFolderPage(string path)
        {
            var url = GetFolderPageUrl(path);

            if (path.CheckNotNull()) return;

            App.RootFrame.Navigate(new Uri(url, UriKind.Relative));
        }

        /// <summary>
        /// 是否从照片自动上传页面进来
        /// </summary>
        public static bool IsFrom_ConfigurePhotosUploadSettings;

        public class Option
        {
            public const string GeneralUrl = "/Page/Option/GeneralPage.xaml";

            public const string UploadPageUrl = "/Page/Option/UploadPage.xaml";
            
            public const string AutoUploadUrl = "/Page/Option/AutoUploadPage.xaml";

            public const string AboutUrl = "/Page/Option/AboutPage.xaml";
        }
    }
}
