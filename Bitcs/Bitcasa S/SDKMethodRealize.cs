﻿using Bitcasa_S.Resources;
using BitcasaSDK_WP;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Bitcasa_S
{
    public class SDKMethodRealize
    {
        public bool InvokeUIDispatcher(Action action)
        {
            if (App.RootFrame == null)
                return false;

            App.RootFrame.Dispatcher.BeginInvoke(action);
            return true;
        }

        public string LocalString(string text)
        {
            switch (text)
            {
                case "Folder":
                    return AppResources.SDKLocalString_Folder;
                default:
                    return text;
            }
        }

        
    }
}
