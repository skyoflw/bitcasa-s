﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Bitcasa_S
{
    public partial class App : Application
    {
        public static void NavToMain()
        {
            App.RootFrame.Navigate(new Uri("/Page/MainPage.xaml", UriKind.Relative));
        }
    }
}
