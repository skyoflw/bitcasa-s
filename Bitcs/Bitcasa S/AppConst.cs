﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S
{
    public class AppConst
    {
        public const string AuthorEmail = "mailto:skyoflw+BitcasaS@gmail.com";

        public const string GuidInStore = "ea39015e-9f7c-4f8e-a8cf-77c17a82bf85";

        public const string StoreLink = "http://www.windowsphone.com/s?appid=" + GuidInStore;
    }
}
