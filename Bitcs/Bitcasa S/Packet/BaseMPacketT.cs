﻿using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Packet
{
    public abstract class BaseMPacket<Tkey, TValue> : Packet<TValue>
        where TValue : INotifyDataChanged<TValue>
    {
        public BaseMPacket(Tkey key, TValue item)
        {
            this.Context = new Dictionary<Tkey, TValue>();
            this.AddItem(key, item);
        }

        public void AddItem(Tkey key, TValue item)
        {
            lock (Context)
            {
                Context[key] = item;
                base.ConnectionObj(item);
            }
        }

        public Dictionary<Tkey, TValue> Context { get; private set; }
    }
}
