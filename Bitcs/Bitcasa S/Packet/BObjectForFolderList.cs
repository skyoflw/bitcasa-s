﻿using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP;
using Bitcasa_S.Resources;
using System.Windows.Media.Imaging;
using BDB2;
using BCache;
using System.Windows.Media;

namespace Bitcasa_S.Packet
{
    public class BObjectForFolderList : BasePacket<BObject>, IDisposable
    {
        private BObjectForFolderList(BObject obj)
            : base(obj)
        {
        }

        protected override void LoadFromSource()
        {
            base.LoadFromSource();

            this.Name = this.Context.Name;

            //this.IconUri = Source.

            var fs = this.Context as BFileSystem;
            if (fs != null)
            {
                if (fs.IsFile)
                {
                    this.Extension = fs.Extension;
                    this.FileSize = fs.Size.FileSizeFormat();
                    this.IsIncomplete = fs.IsIncomplete;

                    this.backgroundcolor = GetBackgroundColor(fs.Extension);
                }
                else // this.Context is folder
                {
                    this.FileSize = AppResources.SDKLocalString_Folder;

                    this.backgroundcolor = FolderColor;
                }
            }
            else // this.Context is BRoot
            {
                this.FileSize = AppResources.SDKLocalString_Folder;

                this.backgroundcolor = FolderColor;
            }
        }

        private string __name;
        public string Name
        {
            get { return __name; }
            set
            {
                if (__name != value)
                {
                    __name = value;
                    OnNotifyPropertyChanged("Name");
                }
            }
        }

        private string __extension;
        /// <summary>
        /// [文件专属]
        /// </summary>
        public string Extension
        {
            get { return __extension; }
            set
            {
                if (__extension != value)
                {
                    __extension = value;
                    OnNotifyPropertyChanged("Extension");
                }
            }
        }

        private string __iconUrl;
        public string IconUrl
        {
            get { return __iconUrl; }
            set
            {
                if (__iconUrl != value)
                {
                    __iconUrl = value;
                    OnNotifyPropertyChanged("IconUrl");
                }
            }
        }

        private string __fileSize;
        public string FileSize
        {
            get { return __fileSize; }
            set
            {
                if (__fileSize != value)
                {
                    __fileSize = value;
                    OnNotifyPropertyChanged("FileSize");
                }
            }
        }

        private bool __incomplete;
        public bool IsIncomplete
        {
            get { return __incomplete; }
            set
            {
                if (__incomplete != value)
                {
                    __incomplete = value;
                    OnNotifyPropertyChanged("IsIncomplete");
                }
            }
        }

        private bool __isCached;
        public bool IsCached
        {
            get { return __isCached; }
            set
            {
                if (__isCached != value)
                {
                    __isCached = value;
                    OnNotifyPropertyChanged("IsCached");
                }
            }
        }

        private bool __isCaching;
        public bool IsCaching
        {
            get { return __isCaching; }
            set
            {
                if (__isCaching != value)
                {
                    __isCaching = value;
                    OnNotifyPropertyChanged("IsCaching");
                }
            }
        }

        private bool __isCanDeleteCache;
        /// <summary>
        /// 指示是否启用 “删除 Cache” 菜单项
        /// </summary>
        public bool IsCanDeleteCache
        {
            get { return __isCanDeleteCache; }
            set
            {
                if (__isCanDeleteCache != (IsCaching || IsCached))
                {
                    __isCanDeleteCache = (IsCaching || IsCached);
                    OnNotifyPropertyChanged("IsCanDeleteCache");
                }
            }
        }

        private Color? backgroundcolor;

        public Brush Background
        {
            get
            {
                if (backgroundcolor == null)
                    return (Brush)App.RootFrame.Resources["PhoneAccentBrush"];
                else
                    return new SolidColorBrush((Color)backgroundcolor);
            }
        }

        /// <summary>
        /// 指示对象是否能够流处理
        /// </summary>
        public bool CanStream
        {
            get { return this.Context.IsAllowStreamFile(); }
        }

        public static BObjectForFolderList From(BObject obj)
        {
            return new BObjectForFolderList(obj);
        }

        public static IEnumerable<BObjectForFolderList> From(IEnumerable<BObject> source)
        {
            return source.Select(obj => From(obj));
        }

        public override bool Equals(BObject obj)
        {
            return this.Context.Path64 == obj.Path64;
        }

        public bool Equals(BObjectForFolderList obj)
        {
            return this.Context.Path64 == obj.Context.Path64;
        }

        public BitmapImage GetThumbnailAsync(BFileSystem fs)
        {
            return null;
            //UIInvoke(true
            //if (fs.IsFolder)
            //    return null;

            //if (fs.Extension.ToLower() != "jpg" &&
            //    fs.Extension.ToLower() != "png")
            //    return null;

            //CacheFile cf = null;
            //if (!AppCore.DB.CacheFilesDic.TryGetValue(fs.Guid, out cf))
            //    return null;

            //if (cf.Status != CacheStatus.Completed)
            //    return null;

            //try
            //{
            //    BitmapImage img = new BitmapImage();
            //    img.SetSource(CacheRoot.OpenRead(cf));
            //    return img;
            //}
            //catch { return null; }
        }

        public static void QueryIsCachedOrCachingAsync(
            IEnumerable<BObjectForFolderList> items)
        {
            System.Threading.Tasks.Task.Run(() =>
                {
                    items = items
                        .Where(z => z.Context is BFileSystem)
                        .Where<BObjectForFolderList>(z => z.Context.IsFile);

                    var cache = CFDB.FromId(items.Select(z => (z.Context as BFileSystem).Id));

                    CacheFile cf = null;

                    foreach (var item in items)
                    {
                        item.IsCached = false;
                        item.IsCaching = false;

                        if (cache.TryGetValue((item.Context as BFileSystem).Id, out cf))
                        {
                            if (cf.Status == CacheStatus.Completed)
                                item.IsCached = true;
                            else if (cf.Status == CacheStatus.InCompleted)
                                item.IsCaching = true;

                            item.IsCanDeleteCache = true;
                        }
                    }

                    return false;
                });
        }

        public void Dispose()
        {
            
        }

        public static Color? GetBackgroundColor(string ext)
        {
            byte a = 255;

            Func<byte, byte, byte, Color> RGB =
                (r, g, b) => Color.FromArgb(a, r, g, b);;

            switch (ext.ToLower())
            {
                // office
                case "doc":
                case "docx":
                    return RGB(41, 84, 151);

                case "xls":
                case "xlsx":
                    return RGB(30, 30, 30);

                case "ppt":
                case "pptx":
                    return RGB(209, 68, 36);

                // other doc
                case "pdf":
                    return RGB(210, 71, 38);

                // video
                case "avi":
                    return RGB(163, 26, 158);
                case "mkv":
                    return RGB(67, 156, 55);
                case "flv":
                    return RGB(70, 87, 109);
                case "mp4":
                    return RGB(168, 56, 69);
                
                // music

                // pic
                case "jpg":
                    return RGB(76, 136, 98);
            }

            return null;
        }

        public readonly static Color FolderColor = Color.FromArgb(255, 239, 216, 111);
    }
}
