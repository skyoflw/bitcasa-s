﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Packet
{
    public abstract class CNotifyPropertyChanged : INotifyPropertyChanged
    {
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnNotifyPropertyChanged(string propertyName)
        {
            UIInvoke(false, () => { OnPropertyChanged(propertyName); });
        }

        /// <summary>
        /// 委托 UI 线程执行委托
        /// </summary>
        /// <param name="a"></param>
        protected void UIInvoke(bool mustUI, Action a)
        {
            App.UIInvoke(mustUI, a);
        }
    }
}
