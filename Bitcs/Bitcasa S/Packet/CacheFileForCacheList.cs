﻿using BCache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using BitcasaSDK_WP;

namespace Bitcasa_S.Packet
{
    public class CacheFileForCacheList : BasePacket<CacheFile>, IDisposable
    {
        private CacheFileForCacheList(CacheFile obj)
            : base(obj)
        {
        }

        protected override void LoadFromSource()
        {
            base.LoadFromSource();

            this.Name = this.Context.FileName;

            if (this.Name != null)
            {
                this.Extension = System.IO.Path.GetExtension(this.Name);
                if (this.Extension.Length > 0 && this.Extension[0] == '.')
                {
                    this.Extension = this.Extension.Substring(1);
                }
            }
            else
                this.Extension = null;

            this.FileSize = this.Context.FileSize.FileSizeFormat();

            this.backgroundcolor = BObjectForFolderList.GetBackgroundColor(this.Extension);

            this.IsCached = this.Context.Status == CacheStatus.Completed;
            this.IsCaching = this.Context.Status != CacheStatus.Completed;

            this.LinkCounter = this.Context.LinkCounter.ToString();
        }

        private string __name;
        public string Name
        {
            get { return __name; }
            set
            {
                if (__name != value)
                {
                    __name = value;
                    OnNotifyPropertyChanged("Name");
                }
            }
        }

        private string __extension;
        public string Extension
        {
            get { return __extension; }
            set
            {
                if (__extension != value)
                {
                    __extension = value;
                    OnNotifyPropertyChanged("Extension");
                }
            }
        }

        private string __fileSize;
        public string FileSize
        {
            get { return __fileSize; }
            set
            {
                if (__fileSize != value)
                {
                    __fileSize = value;
                    OnNotifyPropertyChanged("FileSize");
                }
            }
        }

        private bool __isCached;
        public bool IsCached
        {
            get { return __isCached; }
            set
            {
                if (__isCached != value)
                {
                    __isCached = value;
                    OnNotifyPropertyChanged("IsCached");
                }
            }
        }

        private bool __isCaching;
        public bool IsCaching
        {
            get { return __isCaching; }
            set
            {
                if (__isCaching != value)
                {
                    __isCaching = value;
                    OnNotifyPropertyChanged("IsCaching");
                }
            }
        }

        private string __linkCounter;
        public string LinkCounter
        {
            get { return __linkCounter; }
            set
            {
                if (__linkCounter != value)
                {
                    __linkCounter = value;
                    OnNotifyPropertyChanged("LinkCounter");
                }
            }
        }

        private Color? backgroundcolor;

        public Brush Background
        {
            get
            {
                if (backgroundcolor == null)
                    return (Brush)App.RootFrame.Resources["PhoneAccentBrush"];
                else
                    return new SolidColorBrush((Color)backgroundcolor);
            }
        }

        public static CacheFileForCacheList From(CacheFile obj)
        {
            return new CacheFileForCacheList(obj);
        }

        public static IEnumerable<CacheFileForCacheList> From(IEnumerable<CacheFile> source)
        {
            return source.Select(obj => From(obj));
        }

        public override bool Equals(CacheFile obj)
        {
            return this.Context.Id == obj.Id;
        }

        public void Dispose()
        {
            
        }
    }
}
