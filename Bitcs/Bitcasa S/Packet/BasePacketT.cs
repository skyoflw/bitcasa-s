﻿using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Packet
{
    public abstract class BasePacket<T> : Packet<T> where T : INotifyDataChanged<T>
    {
        public BasePacket(T item)
        {
            this.Context = item;
            base.ConnectionObj(item);
            LoadFromSource();
        }

        public T Context { get; private set; }

        public void UpdateFromNew(T source)
        {
            Context.Update(source);
        }
    }
}
