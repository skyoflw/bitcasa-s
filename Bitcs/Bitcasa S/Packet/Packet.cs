﻿using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Packet
{
    public abstract class Packet<T> : CNotifyPropertyChanged where T : INotifyDataChanged<T>
    {
        protected void ConnectionObj(T obj)
        {
            obj.DataChanged += (a, b) => { LoadFromSource(); };
        }

        protected virtual void LoadFromSource()
        {
        }

        /// <summary>
        /// 实现包与内容的相等
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public abstract bool Equals(T obj);
    }
}
