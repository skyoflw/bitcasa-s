﻿using Bitcasa_S.Resources;
using Bitcasa_S.Task;
using BitcasaSDK_WP.BJson;
using BitcasaSDK_WP.BObj;
using BitcasaSDK_WP.BRequest;
using BitcasaSDK_WP.Parameter;
using BSetting;
using Ex2.Portable.Collections.Generic;
using BDll;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bitcasa_S.Packet;
using BDB2;
using BitcasaSDK_WP;
using BCache;

namespace Bitcasa_S.Model
{
    public class FolderModel : FileSystemModel
    {
        public FolderModel(string path64)
        {
            if (path64 == null) throw new ArgumentNullException();

            RealList = new List<BObjectForFolderList>();
            showList = new ObservableCollection<BObjectForFolderList>();

            this.Path64 = path64;

            Comparison = (a, b) => SortItems(a.Context, b.Context, this.SortMode);

            Where = (z) => FilterItems(z.Context, this.ViewMode);
        }

        #region member

        /// <summary>
        /// 构造此 FolderModel 的参数
        /// </summary>
        public string Path64 { get; private set; }

        /// <summary>
        /// 当前目录的依赖项
        /// </summary>
        public BObject Folder { get; private set; }

        private string folderName;
        /// <summary>
        /// 当前文件夹名
        /// </summary>
        public string FolderName
        {
            get { return folderName; }
            set
            {
                if (folderName != value)
                {
                    folderName = value;
                    OnNotifyPropertyChanged("FolderName");
                }
            }
        }

        private string parentDirectory;
        /// <summary>
        /// 父目录路径
        /// </summary>
        public string ParentDirectory
        {
            get { return parentDirectory; }
            set
            {
                if (parentDirectory != value)
                {
                    parentDirectory = value;
                    OnNotifyPropertyChanged("ParentDirectory");
                }
            }
        }

        #endregion

        #region 完整列表及显示列表

        private ObservableCollection<BObjectForFolderList> showList;
        /// <summary>
        /// 用于绑定在界面上的列表
        /// </summary>
        public ObservableCollection<BObjectForFolderList> ShowList
        {
            get { return showList; }
            protected set
            {
                if (showList != value)
                {
                    showList = value;
                    OnNotifyPropertyChanged("ShowList");
                }
            }
        }

        public List<BObjectForFolderList> RealList { get; private set; }

        /// <summary>
        /// 将对象添加到列表 RealList 及 ShowList 中。
        /// 返回值为被构造的包装
        /// </summary>
        /// <param name="items"></param>
        private List<BObjectForFolderList> ListAddItem(IEnumerable<BObject> items)
        {
            var l = BObjectForFolderList.From(items).ToList();

            if (Comparison != null) l.Sort(Comparison);

            RealList.AddRange(l);

            if (Where != null) l = l.Where(Where).ToList();

            UIInvoke(false, () => l.ForEach(z => ShowList.Add(z)));

            BObjectForFolderList.QueryIsCachedOrCachingAsync(l);

            return l;
        }
        /// <summary>
        /// 将对象添加到列表 RealList 及 ShowList 中。
        /// 返回值为被构造的包装
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private BObjectForFolderList ListAddItem(BFileSystem item)
        {
            return ListAddItem(new BFileSystem[] { item }).First();
        }

        /// <summary>
        /// 从列表 RealList 及 ShowList 中的列表移除项
        /// </summary>
        /// <param name="item"></param>
        private void ListRemoveItem(BFileSystem item)
        {
            if (item == null) return;

            var del = GetPacket(item);

            RealList.Remove(del);

            //RealList.RemoveAll(z => z.Equals(item));
            //var del = ShowList.Where(z => z.Equals(item)).FirstOrDefault();
            UIInvoke(false, () => ShowList.Remove(del));
        }
        /// <summary>
        /// 从列表 RealList 及 ShowList 中的列表移除项
        /// </summary>
        /// <param name="items"></param>
        private void ListRemoveItem(IEnumerable<BFileSystem> items)
        {
            var del = GetPacket(items);

            if (this.RealList != null)
            {
                lock (this.RealList)
                {
                    this.RealList.Remove(del);
                }
            }

            UIInvoke(false, () => ShowList.Remove(del));
        }

        /// <summary>
        /// 将列表 RealList 及 ShowList 中的值更新为给定的对象的值
        /// </summary>
        /// <param name="item"></param>
        private void ListUpdateItem(BFileSystem item)
        {
            if (item == null)
                return;

            var pack = GetPacket(item);

            pack.UpdateFromNew(item);
        }
        /// <summary>
        /// 将列表 RealList 及 ShowList 中的值更新为给定的对象的值
        /// </summary>
        /// <param name="items"></param>
        private void ListUpdateItem(IEnumerable<BFileSystem> items)
        {
            if (this.RealList != null)
            {
                lock (this.RealList)
                {
                    BObjectForFolderList r = null;
                    var dic = this.RealList.ToDictionary(z => z.Context.Path64);
                    foreach (var i in items)
                        if (dic.TryGetValue(i.Path64, out r))
                            r.UpdateFromNew(i);
                }
            }
        }

        /// <summary>
        /// 获取相关的封包
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private BObjectForFolderList GetPacket(BFileSystem item)
        {
            if (item == null) return null;

            BObjectForFolderList result = null;

            if (RealList != null)
            {
                lock (RealList)
                    result = RealList.Where(z => z.Context.Path64 == item.Path64).FirstOrDefault();
            }

            return result;
        }
        /// <summary>
        /// 获取相关的封包
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private List<BObjectForFolderList> GetPacket(IEnumerable<BFileSystem> items)
        {
            var result = new List<BObjectForFolderList>();

            if (RealList != null)
            {
                lock (RealList)
                {
                    result.AddRange(from BObjectForFolderList p in RealList
                                    from BFileSystem i in items
                                    where p.Context.Path64 == i.Path64
                                    select p);
                }
            }

            return result;
        }

        /// <summary>
        /// 定制的筛选器
        /// </summary>
        public Func<BObjectForFolderList, bool> Where { get; protected set; }

        /// <summary>
        /// 定制的比较器
        /// </summary>
        public Comparison<BObjectForFolderList> Comparison { get; protected set; }

        #endregion        

        /// <summary>
        /// 刷新并排序显示列表
        /// </summary>
        /// <param name="comparison"></param>
        protected void ModelRefresh()
        {
            var l = RealList.ToList();

            if (Comparison != null) l.Sort(Comparison);

            if (Where != null) l = l.Where(Where).ToList();

            ShowList = new ObservableCollection<BObjectForFolderList>();
        }

        #region 项目的添加、移除及更新操作

        /// <summary>
        /// 对从请求中获取到的新对象的操作封装
        /// </summary>
        /// <param name="items"></param>
        private void ModelAddItem(IEnumerable<BFileSystem> items)
        {
            items.InsertToDB(Folder);

            ListAddItem(items);
        }
        /// <summary>
        /// 对从请求中获取到的被移除的对象的操作封装
        /// </summary>
        /// <param name="items"></param>
        private void ModelDeleteItem(IEnumerable<BFileSystem> items)
        {
            base.LocalDeleteObject(items);
            
            ListRemoveItem(items);
        }
        /// <summary>
        /// 对从请求中获取到的对象的更新的操作封装
        /// </summary>
        /// <param name="items"></param>
        private void ModelUpdateItem(BFileSystem item)
        {
            var objid = item.UpdateToDB();

            ListUpdateItem(item);

            if (objid == null) return;

            var cf = CFDB.DeleteFromDB(objid);
            if (cf == null) return;

            cf.DeleteFile();
        }
        /// <summary>
        /// 对从请求中获取到的对象的更新的操作封装
        /// </summary>
        /// <param name="items"></param>
        private void ModelUpdateItem(IEnumerable<BFileSystem> items)
        {
            var objid = items.UpdateToDB();

            ListUpdateItem(items);

            if (objid.Count == 0) return;

            var cfs = CFDB.DeleteFromDB(objid);
            if (cfs.Count == 0) return;

            cfs.DeleteFileAsync();
        }

        #endregion

        #region show tag

        /// <summary>
        /// 获取一个值，指示是否应该显示选择自动上传文件夹的按钮
        /// </summary>
        public bool IsShowChooseAutoUploadFolder
        {
            get { return Setting.IsEnableAutoUpload && Setting.AutoUploadFolder == null; }
        }

        public void SetAsAutoUploadFolder()
        {
            if (Setting.IsEnableAutoUpload && Setting.AutoUploadFolder == null)
            {
                Setting.SetAutoUploadFolder(
                    Path64, this.ParentDirectory + "\\" + this.FolderName);
                AppCore.TryInitAutoUploadManager();
            }
        }

        /// <summary>
        /// 手动配置此值为真会引发异常
        /// </summary>
        public bool IsShowPaste
        {
            get { return AppCore.AppClipboard.HaveValue; }
        }

        public void Paste()
        {
            if (!AppCore.AppClipboard.HaveValue)
                return;

            var fss = AppCore.AppClipboard.Paste();
            OnNotifyPropertyChanged("IsShowPaste");

            PasteAsBatch(fss);
        }

        private void PasteAsBatch(IEnumerable<BFileSystem> fs)
        {
            //var p = new BParameter<BatchOpera[], BatchOpera[]>();
            //p.Callback_Successed = (e) =>
            //{
            //    foreach (var i in e)
            //        if (i.Type == BatchOpera.OperaType.Move)
            //        {
            //        }
            //};

            //BatchOpera[] bo = AppCore.AppClipboard.IsMove ?
            //    BatchOpera.Move(fs, this.CurrentPath) :
            //    BatchOpera.Copy(fs, this.CurrentPath);

            //BatchOpera.RunAsync(bo, p);
        }

        #endregion

        #region 排序及筛选模式

        private ViewModeType __viewMode;
        public ViewModeType ViewMode
        {
            get { return __viewMode; }
            set
            {
                if (__viewMode != value)
                {
                    __viewMode = value;
                    ModelRefresh();
                }
            }
        }

        public enum ViewModeType : int
        {
            Normal = 0
        }

        private SortModeType __sortMode = SortModeType.Name_A_Z;
        public SortModeType SortMode
        {
            get { return __sortMode; }
            set
            {
                if (__sortMode != value)
                {
                    __sortMode = value;
                }
            }
        }

        /// <summary>
        /// 排序模式
        /// </summary>
        public enum SortModeType : int
        {
            Name_A_Z = 0,
            Name_Z_A = 1,
            CreateDate_Old_New = 2,
            CreateDate_New_Old = 3,
            ChangeTime_Old_New = 4,
            ChangeTime_New_Old = 5,
            ModifyTime_Old_New = 6,
            ModifyTime_New_Old = 7
        }

        protected static bool FilterItems(BObject item, ViewModeType mode)
        {
            switch (mode)
            {
                case ViewModeType.Normal:
                    return true;
#if DEBUG
                default:
                    throw new NotImplementedException();
#endif
            }
            return true;
        }

        protected static int SortItems(BObject a, BObject b, SortModeType mode)
        {
            switch (mode)
            {
                case SortModeType.Name_A_Z:
                    if (a.IsFile == b.IsFile) 
                        return a.Name.CompareTo(b.Name); 
                    return a.IsFile ? 1 : -1; 
                case SortModeType.Name_Z_A:
                    if (a.IsFile == b.IsFile) 
                        return b.Name.CompareTo(a.Name); 
                    return a.IsFile ? 1 : -1; 
                //case SortModeType.CreateDate_New_Old:
                //    if (a.IsFile == b.IsFile) 
                //        return b.CreateTime.CompareTo(a.CreateTime); 
                //    return a.IsFile ? 1 : -1;
                //case SortModeType.CreateDate_Old_New:
                //    if (a.IsFile == b.IsFile) 
                //        return a.CreateTime.CompareTo(b.CreateTime); 
                //    return a.IsFile ? 1 : -1; 
                //case SortModeType.ChangeTime_New_Old:
                //    if (a.IsFile == b.IsFile) 
                //        return b.ChangeTime.CompareTo(a.ChangeTime); 
                //    return a.IsFile ? 1 : -1; 
                //case SortModeType.ChangeTime_Old_New:
                //    if (a.IsFile == b.IsFile) 
                //        return a.ChangeTime.CompareTo(b.ChangeTime); 
                //    return a.IsFile ? 1 : -1; 
                //case SortModeType.ModifyTime_New_Old:
                //    if (a.IsFile == b.IsFile) 
                //        return b.ModifyTime.CompareTo(a.ModifyTime); 
                //    return a.IsFile ? 1 : -1; 
                //case SortModeType.ModifyTime_Old_New:
                //    if (a.IsFile == b.IsFile) 
                //        return a.ModifyTime.CompareTo(b.ModifyTime); 
                //    return a.IsFile ? 1 : -1; 
#if DEBUG
                default:
                    throw new NotImplementedException();
#endif
            }
            return 0;
        }

        #endregion

        private void OnObjectNotFound()
        {
            if (ObjectNotFound != null)
                ObjectNotFound(this, this.Path64);
        }
        /// <summary>
        /// 当前 Model 依赖的对象没有找到
        /// </summary>
        public event EventHandler<string> ObjectNotFound;

        protected override void Load()
        {
            List<BObject> items = new List<BObject>();

            if (this.Folder == null)
            {
                BObject obj = null;
                items.AddRange(FSDB.FromParent(this.Path64, out obj));
                this.Folder = obj;
            }
            else
                items.AddRange(FSDB.FromParent(this.Path64));

            if (this.Folder == null)
            {
                IsLoadDataing = false;
                OnObjectNotFound();                
                return;
            }

            var root = this.Folder as BRoot;
            if (root != null)
            {
                if (root.SyncType == BRoot.SyncTypeType.InfiniteDrive)
                {
                    var roots = FSDB.ListRoot();
                    items.AddRange(roots.Where(z => z.SyncType != BRoot.SyncTypeType.InfiniteDrive));
                }
            }

            this.FolderName = this.Folder.Name;

            if (this.Folder as BRoot != null)
                this.ParentDirectory = String.Empty;
            else
                this.ParentDirectory = (this.Folder as BFileSystem).GetParentHumanPath();

            this.RealList.Clear();
            this.UIInvoke(false, this.ShowList.Clear);

            this.ListAddItem(items);
            this.OnScrollToEvent();

            var parameter = new BParameter<List<BFileSystem>>();

            parameter.Callback_Successed = list =>
                {
                    this.IsLoadDataCompleted = true;

                    var compare = new ListCompare<BFileSystem>(
                        (r1, r2) => r1.Path64.CompareTo(r2.Path64),
                        FSDB.FromParent(this.Path64), list);

                    compare.Compare();

                    // add
                    ModelAddItem(compare.OnlyList2);

                    // update
                    ModelUpdateItem(compare.CommonItems2);
                    
                    // del
                    ModelDeleteItem(compare.OnlyList1);
                };
            parameter.Callback_Failed = (e) =>
                {
                    App.ErrorHandler.DefaultErrorHandler(e, AppResources.FolderModel_Opeartion_UpdateList);
                };
            parameter.Callback_Completed = () =>
                {
                    IsLoadDataing = false;
                    this.RemoveStatusText(AppResources.FolderModel_Message_LoadingData);
                };

            this.AddStatusText(AppResources.FolderModel_Message_LoadingData);
            AppCore.Client.ListFolder(this.Path64, parameter);
        }

        /// <summary>
        /// 异步根据 Mode 的排序属性排序并刷新列表
        /// </summary>
        public void SortAsync()
        {
            //base.ModelRefresh();
            //System.Threading.Tasks.Task.Run(() => Sort());
        }

        public void Rename(BFileSystem fs, string newName)
        {
            var parameter = new BParameter<List<BFileSystem>>();

            parameter.Callback_Successed = (l) =>
                {
                    App.ShowMessageBox(AppResources.FolderModel_Rename_Successed_Message);
                    ModelUpdateItem(l);
                };

            parameter.Callback_Failed = (e) =>
                {
                    App.ErrorHandler.DefaultErrorHandler(e, AppResources.FolderPage_ContextMenu_Rename);
                };

            if (fs.IsFile)
            {
                AppCore.Client.RenameFile(
                    fs.Path64, newName, parameter,
                    ExistsOperationType.Rename);
            }
            else
            {
                AppCore.Client.RenameFolder(
                    fs.Path64, newName, parameter,
                    ExistsOperationType.Rename);
            }
        }

        public void CreateNewFolder(string name)
        {
            var parameter = new BParameter<List<BFileSystemOperationResult>>();

            parameter.Callback_Successed = (l) =>
                {
                    var newitem = (from BFileSystemOperationResult f in l
                                   where f.Status == StatusType.Created
                                   select f).FirstOrDefault();

                    if (newitem != null)
                    {
                        BFileSystem folder = new BFileSystem();
                        folder.Update(newitem);

                        List<BFileSystem> list = new List<BFileSystem>() { folder };
                        ModelAddItem(list);
                    }
                };
            parameter.Callback_Failed = (e) =>
                {
                    App.ErrorHandler.DefaultErrorHandler(e, AppResources.FolderModel_Opeartion_CreateFolder);
                };
            parameter.CancelToken = this;

            AppCore.Client.AddFolder(Path64,name,parameter);
        }

        public void UploadFile(Stream stream, string filename)
        {
            UploadFile(this.Path64, stream, filename);
        }
        public void UploadFile(string parentPath, Stream stream, string filename)
        {
            if (!BFileSystem.IsValid(filename))
            {
                App.ShowMessageBox("file name is invalid");
                return;
            }

            var task = new UploadFileTask(
                parentPath,
                stream,
                filename);
            task.TaskSucceeded += (a, b) =>
            {
                string f = "the file [{0}] was upload succeeded.";
                App.ShowToast(String.Format(f, b.Name));

                var b2 = new List<BFileSystem>();
                b2.Add(b);
                ModelAddItem(b2);
            };

            AppCore.UploadFileManager.AddTask(task);
        }

        /// <summary>
        /// 从 Bitcasa 删除一个 FileSystem
        /// </summary>
        /// <param name="fs"></param>
        public void DeleteFileSystemFromBitcasa(BFileSystem fs)
        {
            var p = new BParameter<List<BFileSystem>>();
            p.Callback_Successed = (f) =>
            {
                string msg_f = AppResources.FileSystemModel_Delete_Success_Message;
                string msg = (f.Count == 1 ?
                    String.Format(msg_f, f[0].Name) : "the items has been delete.");
#if DEBUG
                if (f.Count > 1) throw new NotImplementedException();
#endif
                App.ShowToast(
                    AppResources.FileSystemModel_Delete_Success_Title, msg);

                this.LocalDeleteObject(f);
                this.ListRemoveItem(f.FirstOrDefault());

            };
            p.Callback_Failed = (e) =>
            {
                App.ErrorHandler.DefaultErrorHandler(e, AppResources.FileSystemModel_Opeartion_Delete);
            };

            if (fs.IsFile)
                AppCore.Client.DeleteFile(fs.Path64, p);
            else
                AppCore.Client.DeleteFolder(fs.Path64, p);
        }

        public void ScrollTo(string path64)
        {
            this.ScrollToPath64 = path64;
        }
        private string ScrollToPath64 { get; set; }
        private void OnScrollToEvent()
        {
            if (ScrollToPath64 == null) return;

            var obj = this.RealList.Where(z => z.Context.Path64 == ScrollToPath64).FirstOrDefault();
            if (obj == null) return;

            ScrollToPath64 = null;
            if (ScrollToEvent != null)
                ScrollToEvent(this, obj);
        }
        public event EventHandler<BObjectForFolderList> ScrollToEvent;
    }
}
