﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP.BObj;
using BitcasaSDK_WP.Parameter;
using Bitcasa_S.Resources;
using BSetting;
using BDB2;
using BCache;
using BitcasaSDK_WP.BException;

namespace Bitcasa_S.Model
{
    public class AccountModel : Model
    {
        public AccountModel()
        {
            AccountChanged += (a, b) =>
                {
                    AppDataContext2.Rebuild();
                    CacheFilesHelper.ClearAllFolder();
                };
        }

        private BUserProfile __userProfile;
        public BUserProfile UserProfile
        {
            get { return __userProfile; }
            set
            {
                if (__userProfile != value)
                {
                    __userProfile = value;
                    OnNotifyPropertyChanged("UserProfile");
                }
            }
        }

        protected override void Load()
        {
            if (Setting.Account.Id != null)
            {
                if (!Setting.Account.IsLastLogin)
                {
                    var f = new BUserProfile();
                    f.Id = Setting.Account.Id;
                    f.DisplayName = Setting.Account.DisplayName;
                    f.ReferralLink = Setting.Account.ReferralLink;
                    f.Storage.Total = Setting.Account.StorageTotal;
                    f.Storage.Used = Setting.Account.StorageUsed;
                    f.Storage.Display = Setting.Account.StorageDisplay;
                    this.UserProfile = f;
                    this.IsLoadDataCompleted = true;
                }
            }

            var p = new BParameter<BUserProfile>();

            BitcasaException exc = null;

            p.Callback_Successed = f =>
            {
                if (Setting.Account.Id != f.Id)
                {
                    if (AccountChanged != null) AccountChanged(this, null);
                }

                UserProfile = f;

                Setting.Account.IsLastLogin = false;
                Setting.Account.DisplayName = f.DisplayName;                
                Setting.Account.Id = f.Id;
                Setting.Account.ReferralLink = f.ReferralLink;
                Setting.Account.StorageTotal = f.Storage.Total;
                Setting.Account.StorageUsed = f.Storage.Used;
                Setting.Account.StorageDisplay = f.Storage.Display;

                this.IsLoadDataCompleted = true;
            };

            p.Callback_Failed = e =>
            {
                exc = e;

                App.ErrorHandler.DefaultErrorHandler(e, 
                    AppResources.AccountModel_Opeartion_GetAccountData);
            };
            
            p.Callback_Completed = () =>
            {
                IsLoadDataing = false;

                if (LoadEnd != null) LoadEnd(this, exc);
            };

            // 如果已经刷新成功一次，那么就可以取消，否则不允许取消
            if (this.IsLoadDataCompleted) p.CancelToken = this;
            
            AppCore.Client.GetUserProfile(p);
        }

        /// <summary>
        /// AccountModel 数据联网载入结束。
        /// 在载入数据之后检查字段 IsLastLogin 可以检索当前账号信息是否以前的用户的账号信息。
        /// </summary>
        public event EventHandler<BitcasaException> LoadEnd;

        /// <summary>
        /// 账号变更。此时应该删除所有数据。
        /// </summary>
        public event EventHandler AccountChanged;
    }
}
