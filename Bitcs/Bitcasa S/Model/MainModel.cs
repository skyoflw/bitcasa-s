﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP;
using BitcasaSDK_WP.BObj;
using Ex2.Portable.Collections.Generic;
using Bitcasa_S.Resources;
using System.Windows;
using BitcasaSDK_WP.Parameter;
using BDll;
using Bitcasa_S.Packet;
using System.Collections.ObjectModel;
using BCache;
using BDB2;

namespace Bitcasa_S.Model
{
    public class MainModel : Model
    {
        public MainModel()
        {
            RootList = new List<BRoot>();
        }

        private List<BRoot> RootList { get; set; }

        protected override void Load()
        {
            // 标志位，指示是否在结束之后重新载入
            bool isRetry = false;

            RootList.Clear();
            RootList.AddRange(FSDB.ListRoot());

            lock (RootList)
                if (RootList.Count > 0)
                    this.IsLoadDataCompleted = true;

            IsLoadDataing = true;

            var parameter = new BParameter<List<BRoot>>();

            parameter.Callback_Successed = (list) =>
                {
                    RootList.Clear();
                    RootList.AddRange(list);

                    var compare = new ListCompare<BRoot>(
                        (r1, r2) => r1.Path64.CompareTo(r2.Path64),
                        FSDB.ListRoot(), list);

                    compare.Compare();
                    
                    // 将新的更新到数据库
                    compare.CommonItems2.UpdateToDB();

                    // 将新的插入数据库
                    compare.OnlyList2.InsertToDB();

                    // 将只在本地不在云端的对象删除
                    base.LocalDeleteObject(compare.OnlyList1);

                    if (!this.IsLoadDataCompleted)
                    {
                        this.IsLoadDataCompleted = true;
                        App.ShowToast(AppResources.MainModel_First_RefreshDataCompleted);                        
                    }
                };

            parameter.Callback_Failed = (e) =>
                {
                    if (!App.ErrorHandler.CatchAuthError(e))
                    {
                        if (!this.IsLoadDataCompleted)
                        {
                            App.UIInvoke(true, () =>
                                {
                                    if (MessageBox.Show(
                                        AppResources.MainModel_Error_CanNotGetData,
                                        "", MessageBoxButton.OKCancel)
                                        == MessageBoxResult.OK)
                                    {
                                        isRetry = true;
                                    }
                                });
                        }
                        else
                            App.ErrorHandler.ShowToast(
                                e, AppResources.MainModel_Opeartion_UpdateList);
                    }
                };

            parameter.Callback_Completed = () =>
                {
                    IsLoadDataing = false;
                    this.RemoveStatusText(AppResources.MainModel_Message_LoadingRootData);

                    // 如果重试，则重载
                    if (isRetry) this.Load();
                };

            parameter.CancelToken = this;

            this.AddStatusText(AppResources.MainModel_Message_LoadingRootData);

            AppCore.Client.ListRoot(parameter);
        }

        #region Tag

        /// <summary>
        /// 指示是否成功刷新了列表
        /// </summary>
        public bool IsRefreshRootSuccessed
        {
            get
            {
                if (RootList == null) return false;
                return RootList.Count > 0;
            }
        }

        #endregion

        public void NavToDrive()
        {
            if (RootList == null) return;

            BRoot root = RootList
                .Where(z => z.SyncType == BRoot.SyncTypeType.InfiniteDrive)
                .FirstOrDefault();

            if (root != null)
                CustomUriMapper.NavToFolderPage(root.Path64);
        }
    }
}
