﻿using BDB2;
using Bitcasa_S.Packet;
using Bitcasa_S.Resources;
using BitcasaSDK_WP;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCache;

namespace Bitcasa_S.Model
{
    public abstract class Model : CNotifyPropertyChanged, ICancelled
    {
        public Model()
        {
            __isLoadDataing = false;
            IsLoadDataCompleted = false;

            statusTextStack = new List<string>();
        }

        /// <summary>
        /// 参数为是否强制刷新
        /// </summary>
        /// <param name="isRefreshForce"></param>
        public async void LoadAsync(bool isRefreshForce = false)
        {
            // 如果已经存在数据，不是强制刷新的话不会刷新
            if (IsLoadDataCompleted && !isRefreshForce) return;

            lock (this) // 如果正在刷新，则不会刷新
            {
                if (IsLoadDataing) return;
                IsLoadDataing = true;
            }
            
            await System.Threading.Tasks.Task.Run(() => { Load(); });
        }

        protected abstract void Load();

        private bool __isLoadDataing;
        /// <summary>
        /// 获取或设置一个值，指示是否正在刷新
        /// 此项会提供通知
        /// </summary>
        public bool IsLoadDataing
        {
            get { return __isLoadDataing; }
            protected set
            {
                if (__isLoadDataing != value)
                {
                    __isLoadDataing = value;
                    OnNotifyPropertyChanged("IsLoadDataing");
                }
            }
        }

        private bool isLoadDataCompleted;
        /// <summary>
        /// 【标志位】
        /// 获取或设置数据是否刷新完成（至少一次，包括从数据库中刷新）
        /// <para/>
        /// 本标志位用于标志数据是否处于可用状态。
        /// </summary>
        public bool IsLoadDataCompleted
        {
            get { return isLoadDataCompleted; }
            protected set
            {
                if (isLoadDataCompleted != value)
                {
                    isLoadDataCompleted = value;
                    OnNotifyPropertyChanged("IsLoadDataCompleted");
                }
            }
        }

        #region status text
        
        /// <summary>
        /// 状态栏文本堆栈
        /// </summary>
        private List<string> statusTextStack;
        /// <summary>
        /// 绑定到状态栏的文本
        /// </summary>
        public string StatusText
        {
            get
            {
                var text = statusTextStack.LastOrDefault();
                return String.IsNullOrWhiteSpace(text) ?
                    AppResources.Global_AppTitle : text;
            }
        }
        /// <summary>
        /// 添加状态栏文本，并将新添加的项目设置为当前状态文本
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public void AddStatusText(string text)
        {
            statusTextStack.Add(text);
            OnNotifyPropertyChanged("StatusText");
        }
        /// <summary>
        /// 清空状态栏文本
        /// </summary>
        /// <param name="text"></param>
        public void RemoveStatusText(string text)
        {
            statusTextStack.Remove(text);
            OnNotifyPropertyChanged("StatusText");
        }

        #endregion

        /// <summary>
        /// 取消当前 Model 的刷新，如果当前没有刷新，则不会执行
        /// </summary>
        public virtual void CancelLoad()
        {
            if (!IsLoadDataing) return;

            if (Cancelled != null)
                Cancelled(this, null);
        }
        public event EventHandler Cancelled;


        #region 为操作提供方法支持

        /// <summary>
        /// 本地删除对象。
        /// 从数据库中删除项，并删除对应的文件(如果必要的话)。
        /// </summary>
        /// <param name="fss"></param>
        protected virtual void LocalDeleteObject(IEnumerable<BObject> objs)
        {
            var list = objs.ToList();

            var objid = list.DeleteFromDB();
            if (objid.Count == 0) return;

            var cfs = CFDB.DeleteFromDB(objid);
            if (cfs.Count == 0) return;

            cfs.DeleteFileAsync();
        }

        public virtual void LaunchFile(CacheFile file)
        {
            if (file.IsExist() && file.Status == CacheStatus.Completed)
            {
                file.LaunchFile();
                try { file.UpdateToDB(); }
                catch { } // 可能导致数据库错误
            }
        }

        #endregion
    }
}
