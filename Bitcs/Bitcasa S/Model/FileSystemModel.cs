﻿using Bitcasa_S.Resources;
using Bitcasa_S.Task;
using BitcasaSDK_WP.BObj;
using BitcasaSDK_WP.Parameter;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.System;
using BDll;
using BCache;
using Bitcasa_S.Packet;
using BDB2;
using BitcasaSDK_WP;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Media.PhoneExtensions;
using System.IO;

namespace Bitcasa_S.Model
{
    /// <summary>
    /// 提供 FileSystem 的操作方法
    /// </summary>
    public abstract class FileSystemModel : Model
    {
        public virtual void OpenFile(BFileSystem fs)
        {
#if DEBUG
            if (fs.IsFolder)
                throw new ArgumentException("fs can not be folder");
#else
            if (fs.IsFolder) return;
#endif

            var cf = CFDB.GetOrCreate(fs.Id, fs.Name);

            this.LaunchFile(cf);
        }

        public virtual GetFileTask DownloadFile(BFileSystem fs)
        {
#if DEBUG
            if (fs.IsFolder) throw new ArgumentException("fs can not be folder.");
            if (fs.Id == null) throw new ArgumentNullException();
#else
            if (fs.IsFolder) return null;
            if (fs.Id == null) return null;
#endif

            GetFileTask task = AppCore.DownloadFileManager.TryGetTask(fs.Id);

            if (task == null)
            {
                task = new GetFileTask(fs);
                task.IsBackgroundTask = false;
                AppCore.DownloadFileManager.AddTask(task);
            }
            else if (task.IsBackgroundTask)
                task.IsBackgroundTask = false;

            task.RunAsync();
            return task;
        }

        public virtual void StreamFile(BFileSystem fs)
        {
            if (!fs.IsAllowStreamFile()) return;

            MediaPlayerLauncher mediaPlayerLauncher = new MediaPlayerLauncher();
            mediaPlayerLauncher.Media = AppCore.Client.FileUri(fs.Path64, fs.Name);
            mediaPlayerLauncher.Location = MediaLocationType.Data;
            mediaPlayerLauncher.Controls = MediaPlaybackControls.All;
            mediaPlayerLauncher.Show();
        }

        #region delete

        /// <summary>
        /// 删除缓存（不删除 FileSystem）
        /// </summary>
        /// <param name="fs"></param>
        public void LocalDeleteCache(BObjectForFolderList obj)
        {
            var fs = obj.Context as BFileSystem;
            if (fs == null) return;
            if (fs.IsFolder) return;
            var cf = CFDB.DeleteFromDB(fs.Id);

            if (cf != null)
                cf.DeleteFile();

            obj.IsCaching = false;
            obj.IsCached = false;
        }

        #endregion

        /// <summary>
        /// 在执行前务必准备好文件的缓存或文件夹的子文件系统数目，否则什么都不会发生
        /// </summary>
        /// <param name="fs"></param>
        public virtual void PinToStart(BFileSystem fs, int fsCountInDir = 0)
        {
            ShellTileData data = null;

            bool supportsWideTile = false;
            string url = null;

            if (fs.IsFolder)
            {
                url = CustomUriMapper.GetFolderPageUrl(fs.Path64);

                // 样式参考
                // http://msdn.microsoft.com/zh-cn/library/windowsphone/develop/jj207009(v=vs.105).aspx
                data = new IconicTileData()
                {
                    Title = fs.Name,
                    //Count = fs_count_in_dir,
                    //WideContent1 = "[1st row of content]",
                    //WideContent2 = "[2nd row of content]",
                    //WideContent3 = "[3rd row of content]",
                    //SmallIconImage = [small Tile size URI],
                    //IconImage = [medium/wide Tile size URI],
                    //BackgroundColor = [.NET color type of Tile]
                };
            }
            else
            {
                //if (!fs.IsInCache)
                //    return;

                //sb.Append("OpenFile=");

                //data = new FlipTileData()
                //{
                    //Title = fs.Name,
                    //BackTitle = "[back of Tile title]",
                    //BackContent = "[back of medium Tile size content]",
                    //WideBackContent = "[back of wide Tile size content]",
                    //Count = [count],
                    //SmallBackgroundImage = [small Tile size URI],
                    //BackgroundImage = [front of medium Tile size URI],
                    //BackBackgroundImage = [back of medium Tile size URI],
                    //WideBackgroundImage = [front of wide Tile size URI],
                    //WideBackBackgroundImage = [back of wide Tile size URI],
                //};
            }

            if (url != null)
            {
                Uri uri = new Uri(url, UriKind.Relative); ;
                ShellTile.Create(uri, data, supportsWideTile);
            }
        }

        /// <summary>
        /// [未完成方法]
        /// </summary>
        /// <param name="fs"></param>
        public void PinToStart(BFileSystem fs)
        {
            StandardTileData tile = new StandardTileData();
            ShellTile.Create(new Uri(""), tile, false);
        }
        
        #region save

        public static bool CanSaveToRingtone(BFileSystem fs)
        {
            if (fs.IsFolder) return false;

            if (fs.Size > 30 * 1024 * 1024) return false;

            var name = fs.Name.ToLower();
            if (name != ".mp3" || name != ".wma" || name != ".m4r")
                return false;

            return true;
        }

        public virtual void SaveToRingtone(CacheFile file)
        {
            var task = new SaveRingtoneTask();

            task.Completed += (a, e) =>
                {
                    switch (e.TaskResult)
                    {
                        //Logic for when the ringtone was saved successfully
                        case TaskResult.OK:
                            MessageBox.Show("Ringtone saved.");
                            break;

                        //Logic for when the task was cancelled by the user
                        case TaskResult.Cancel:
                            MessageBox.Show("Save cancelled.");
                            break;

                        //Logic for when the ringtone could not be saved
                        case TaskResult.None:
                            MessageBox.Show("Ringtone could not be saved.");
                            break;
                    }
                };

            task.Source = new Uri("isostore:/" + file.FilePath);
            task.DisplayName = file.FileName;

            task.Show();
        }

        public virtual void SaveImageToLibrary(CacheFile file)
        {
            using (var stream = file.OpenFile())
            {
                if (stream == null) return;

                using (var lib = new MediaLibrary())
                    lib.SavePicture(file.FileName, stream);
            }
        }

        public virtual void SaveSongToLibrary(CacheFile file)
        {
            if (!file.IsExist()) return;

            using (var lib = new MediaLibrary())
            {
                // http://social.msdn.microsoft.com/Forums/wpapps/en-US/f5fa73da-176b-4aaa-8960-8f704236bda5/medialibrary-savesong-method?forum=wpdevelop
                lib.SaveSong(
                    new Uri(file.FilePath, UriKind.RelativeOrAbsolute),
                    null,
                    SaveSongOperation.CopyToLibrary);
            }
        }

        #endregion        
    }
}
