﻿using Bitcasa_S.Packet;
using Bitcasa_S.Resources;
using BSetting;
using Microsoft.Phone.Scheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Model
{
    public class AutoUploadModel : CNotifyPropertyChanged
    {
        public const string AgentName = "Bitcasa S Service";

        public AutoUploadModel()
        {
            Agent = new AgentModel();
        }

        /// <summary>
        /// 获取已经存在的系统后台代理
        /// </summary>
        /// <returns></returns>
        public ScheduledTask GetExistAgent()
        {
            return ScheduledActionService.Find(AgentName) as ScheduledTask;
        }

        public string TotalUpdatePhotoCount
        {
            get { return BSetting.Setting.AllUploadPhotoCount.ToString(); }
        }

        /// <summary>
        /// 启用自动上传后台代理。
        /// 参数指示是否应该关闭后再启用。
        /// <para/>若不关闭后再启用，则当代理存在时不再启用。
        /// <para/>返回值为 false 表示后台没有代理且启用代理失败。
        /// </summary>
        /// <param name="reset"></param>
        /// <returns></returns>
        public bool Enable(bool reset = false)
        {
            var task = GetExistAgent();

            if (task != null && reset) Disable();

            task = GetExistAgent();

            if (task == null)
            {
                var agent = new ResourceIntensiveTask(AgentName)
                {
                    Description = AppResources.AgentModel_Agent_Description
                };

                try
                {
                    ScheduledActionService.Add(agent);
                    Agent.Update(agent);
                    return true;
                }
                catch (Exception exception)
                {
                    OnEnableError(exception.Message);
                    //if (UserDisableAgent != null &&
                    //    exception.Message.Contains("BNS Error: The action is disabled"))
                    //    UserDisableAgent(this, null);
                    //else if (OutOfMaxAgentCount != null &&
                    //    exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                    //    OutOfMaxAgentCount(this, null);
                }
                return false;
            }
            return true;
        }

        private void OnEnableError(string msg)
        {
            if (EnableError != null)
                EnableError(this, msg);
        }
        public event EventHandler<string> EnableError;

        public bool Disable()
        {
            try
            {
                ScheduledActionService.Remove(AgentName);
                Agent.Update(null);
                return true;
            }
            catch (Exception) { }
            return false;
        }        

        public AgentModel Agent { get; private set; }

        /// <summary>
        /// 重置 CameraRoll 最后自动上传时间，如此一来所有照片都会被重新上载
        /// </summary>
        public void ResetCameraRollPhotoTime()
        {
            Setting.LastAutoUploadCameraRollPhotoTime = DateTime.MinValue;
        }

        /// <summary>
        /// 重置 Other Photo 最后自动上传时间，如此一来所有照片都会被重新上载
        /// </summary>
        public void ResetOtherPhotoTime()
        {
            Setting.LastAutoUploadOtherPhotoTime = DateTime.MinValue;
        }

        public class AgentModel : CNotifyPropertyChanged
        {
            public AgentModel() { }

            public void Update(ScheduledTask task)
            {
                CurrentAgent = task;
                OnNotifyPropertyChanged("IsCurrentAgentEnable");
                if (IsCurrentAgentEnable)
                {
                    CurrentAgentName = CurrentAgent.Name;
                    CurrentAgentLastScheduledTime = CurrentAgent.LastScheduledTime.ToString();
                    CurrentAgentExpirationTime = CurrentAgent.ExpirationTime.ToString();
                    CurrentAgentIsScheduled = CurrentAgent.IsScheduled.ToString();
                    CurrentAgentLastExitReason = CurrentAgent.LastExitReason.ToString();
                    OnNotifyPropertyChanged("CurrentAgentRunTime");
                }
                //else
                //{
                //    CurrentAgentName = null;
                //    CurrentAgentLastScheduledTime = null;
                //    CurrentAgentExpirationTime = null;
                //    CurrentAgentIsScheduled = null;
                //    CurrentAgentLastExitReason = null;
                //}
            }

            private ScheduledTask CurrentAgent;

            /// <summary>
            /// 返回一个值指示是否已经计划了代理
            /// </summary>
            public bool IsCurrentAgentEnable
            {
                get { return CurrentAgent != null; }
            }

            private string __currentAgentName;
            /// <summary>
            /// 获取或设置代理的名称
            /// </summary>
            public string CurrentAgentName
            {
                get { return __currentAgentName; }
                set
                {
                    if (__currentAgentName != value)
                    {
                        __currentAgentName = value;
                        OnNotifyPropertyChanged("CurrentAgentName");
                    }
                }
            }

            private string __currentAgentLastScheduledTime;
            public string CurrentAgentLastScheduledTime
            {
                get { return __currentAgentLastScheduledTime; }
                set
                {
                    if (__currentAgentLastScheduledTime != value)
                    {
                        __currentAgentLastScheduledTime = value;
                        OnNotifyPropertyChanged("CurrentAgentLastScheduledTime");
                    }
                }
            }

            private string __currentAgentExpirationTime;
            public string CurrentAgentExpirationTime
            {
                get { return __currentAgentExpirationTime; }
                set
                {
                    if (__currentAgentExpirationTime != value)
                    {
                        __currentAgentExpirationTime = value;
                        OnNotifyPropertyChanged("CurrentAgentExpirationTime");
                    }
                }
            }

            private string __currentAgentLastExitReason;
            public string CurrentAgentLastExitReason
            {
                get { return __currentAgentLastExitReason; }
                set
                {
                    if (__currentAgentLastExitReason != value)
                    {
                        __currentAgentLastExitReason = value;
                        OnNotifyPropertyChanged("CurrentAgentLastExitReason");
                    }
                }
            }

            private string __currentAgentIsScheduled;
            public string CurrentAgentIsScheduled
            {
                get { return __currentAgentIsScheduled; }
                set
                {
                    if (__currentAgentIsScheduled != value)
                    {
                        __currentAgentIsScheduled = value;
                        OnNotifyPropertyChanged("CurrentAgentIsScheduled");
                    }
                }
            }

            public string CurrentAgentRunTime
            {
                get { return BSetting.Setting.AutoUploadAsSystemSeriveRunTime.ToString(); }
            }
        }
    }
}
