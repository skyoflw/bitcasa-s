﻿using Bitcasa_S.Packet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCache;
using BDB2;

namespace Bitcasa_S.Model
{
    public class CacheModel : Model
    {
        public CacheModel()
        {
            showList = new ObservableCollection<CacheFileForCacheList>();
        }

        private ObservableCollection<CacheFileForCacheList> showList;
        /// <summary>
        /// 用于绑定在界面上的列表
        /// </summary>
        public ObservableCollection<CacheFileForCacheList> ShowList
        {
            get { return showList; }
            protected set
            {
                if (showList != value)
                {
                    showList = value;
                    OnNotifyPropertyChanged("ShowList");
                }
            }
        }

        protected override void Load()
        {
            ShowList = new ObservableCollection<CacheFileForCacheList>(
                CacheFileForCacheList.From(
                    CFDB.ListAll()));
            this.IsLoadDataing = false;
            return;
        }

        /// <summary>
        /// 强制删除缓存
        /// </summary>
        /// <param name="fs"></param>
        public void ForceDeleteCache(CacheFileForCacheList obj)
        {
            var fs = obj.Context as CacheFile;
            if (fs == null) return;
            var cf = CFDB.ForceDeleteFromDB(fs.ObjId);

            if (cf != null) cf.DeleteFile();
        }

        /// <summary>
        /// 强制删除缓存
        /// </summary>
        /// <param name="fs"></param>
        public void ForceDeleteCache(IEnumerable<CacheFileForCacheList> obj)
        {
            var objIds = obj.Select(z => z.Context.ObjId);

            var cf = CFDB.ForceDeleteFromDB(objIds);

            cf.DeleteFileAsync();
        }
    }
}
