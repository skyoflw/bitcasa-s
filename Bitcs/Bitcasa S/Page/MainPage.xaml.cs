﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BitcasaSDK_WP;
using Bitcasa_S.Resources;
using Bitcasa_S.Model;
using Microsoft.Phone.Marketplace;
using BSetting;
using BitcasaSDK_WP.BException;

namespace Bitcasa_S.Page
{
    public partial class MainPage : PhoneApplicationPage
    {
#if DEBUG
        private const string ChangeLicense = "DEBUG: license";
#endif
        public MainModel MainModel;

        public MainPage()
        {
            InitializeComponent();
            App.ConfigPage(this);

            this.MainModel = AppCore.Data.MainModel;

            #region category

            var mainCategoryText = new List<string>(new string[]
            {
                AppResources.MainPage_CategoryText_Drive
#if DEBUG
                ,
                AppResources.MainPage_CategoryText_Photo,
                AppResources.MainPage_CategoryText_Documents,
                AppResources.MainPage_CategoryText_Music,
                AppResources.MainPage_CategoryText_Video
#endif
            });
            if (AppCore.IsPaid)
            {
                
            }
            this.CategoryList.ItemsSource = mainCategoryText;

            #endregion

            #region manager

            var mainAdvancedText = new List<string>(new string[]
            {
                AppResources.MainPage_AdvancedText_UploadAndDownload,
                AppResources.MainPage_AdvancedText_Cache
            });
            if (AppCore.IsPaid)
            {

            }
            this.AdvancedList.ItemsSource = mainAdvancedText;

            #endregion

            #region other

            var mainOtherText = new List<string>(new string[]
            {
                AppResources.MainPage_OtherText_Account,
                AppResources.MainPage_OtherText_Option
            });
            if (AppCore.IsPaid)
            {

            }
#if DEBUG
            mainOtherText.Add(ChangeLicense);
#endif
            this.OtherList.ItemsSource = mainOtherText;

            #endregion

            this.BGBorder_NavToInner.Completed += (a, b) =>
                {
                    if (BGBorder_NavToInner_Completed_Callback != null)
                        BGBorder_NavToInner_Completed_Callback();
                };
            this.BGBorder_NavFromInner.Completed += (a, b) =>
                {
                    if (BGBorder_NavFromInner_Completed_Callback != null)
                        BGBorder_NavFromInner_Completed_Callback();
                };
        }

        #region anime

        private Action BGBorder_NavToInner_Completed_Callback;

        /// <summary>
        /// 执行导航到其它页面的动画，动画完成后执行 action
        /// </summary>
        /// <param name="action"></param>
        private void NavToInner(Action action)
        {
            BGBorder_NavToInner_Completed_Callback = action;
            this.BGBorder_NavToInner.Begin();
        }

        private Action BGBorder_NavFromInner_Completed_Callback;

        /// <summary>
        /// 执行导航到此页面的动画，动画完成后执行 action
        /// </summary>
        /// <param name="action"></param>
        private void NavFromInner(Action action)
        {
            BGBorder_NavFromInner_Completed_Callback = action;
            this.BGBorder_NavFromInner.Begin();
        }

        #endregion

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (!AppCore.UploadFileManager.CheckUploading())
            {
                e.Cancel = true;
            }

            base.OnBackKeyPress(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            while (App.RootFrame.BackStack.Count() > 0)
                App.RootFrame.RemoveBackEntry();

            base.OnNavigatedTo(e);

            #region WallpaperSettings

            // 如果用户在手机锁定屏幕设置屏幕中，将您的应用设置为默认锁定屏幕背景图像的提供程序，他们可能点按“打开应用”按钮
            string lockscreenKey = "WallpaperSettings";
            string lockscreenValue = "0";

            bool lockscreenValueExists = NavigationContext.QueryString.TryGetValue(lockscreenKey, out lockscreenValue);

            if (lockscreenValueExists)
            {
                // Navigate the user to your app's lock screen settings screen here, 
                // or indicate that the lock screen background image is updating.
            }

            #endregion

            this.NavFromInner(null);

            // 重新载入账户信息
            if (Setting.Account.IsLastLogin)
            {
                this.DisablePage(AppResources.MainPage_LoadingAccountInfo);
                
                AppCore.Data.AccountModel.LoadEnd += LoadAccountEnd;
                AppCore.Data.AccountModel.LoadAsync();
            }            

            AppCore.Data.MainModel.LoadAsync();

            AppCore.InitSeriveAsync();

#if DEBUG
            // 用来测试方法的位置

            AppCore.Client.ListRoot(
                BitcasaSDK_WP.BRequest.CategoryType.PhotoAlbums,
                BitcasaSDK_WP.BRequest.DepthType.Infinite,
                10, new BitcasaSDK_WP.Parameter.BParameter<List<BitcasaSDK_WP.BObj.BRoot>>());
#endif
        }

        private void LoadAccountEnd(object sender, BitcasaException e)
        {
            if (Setting.Account.IsLastLogin)
            {
                if (e == null || !App.ErrorHandler.CatchAuthError(e))
                {
                    this.Dispatcher.BeginInvoke(() =>
                        {
                            if (MessageBox.Show(AppResources.MainPage_CanNotLoadAccountInfo)
                                == MessageBoxResult.OK)
                            {
                                AppCore.Data.AccountModel.LoadAsync(true);
                            }
                            else App.Current.Terminate();
                        });                    
                }
            }
            else
            {
                AppCore.Data.AccountModel.LoadEnd -= LoadAccountEnd;
                this.EnablePage();
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (this.MainModel != null)
                this.MainModel.CancelLoad();
            base.OnNavigatedFrom(e);
        }

        private void TapCategoryItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var s = sender as FrameworkElement;
            if (s == null) return;
            var t = s.DataContext as string;
            if (t == null) return;
            
            if (t == AppResources.MainPage_CategoryText_Drive)
            {
                if (MainModel.IsRefreshRootSuccessed)
                    NavToInner(MainModel.NavToDrive);
                else
                    MessageBox.Show(AppResources.MainPage_TapMessage_WaitRootData);
            }
            else if (t == AppResources.MainPage_CategoryText_Photo)
                App.ShowNotRealizeError();
            else if (t == AppResources.MainPage_CategoryText_Documents)
                App.ShowNotRealizeError();
            else if (t == AppResources.MainPage_CategoryText_Music)
                App.ShowNotRealizeError();
            else if (t == AppResources.MainPage_CategoryText_Video)
                App.ShowNotRealizeError();
#if DEBUG
            else
                throw new NotImplementedException("MainPage_TapCategoryItem(): t=" + t);
#endif
        }

        private void TapAdvancedItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var s = sender as FrameworkElement;
            if (s == null) return;
            var t = s.DataContext as string;
            if (t == null) return;

            if (t == AppResources.MainPage_AdvancedText_UploadAndDownload)
                NavToInner(CustomUriMapper.NavToTaskManagerPage);
            if (t == AppResources.MainPage_AdvancedText_Cache)
                NavToInner(CustomUriMapper.NavToCachePage);
#if DEBUG
            else
                throw new NotImplementedException();
#endif
        }

        private void TapOtherItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var s = sender as FrameworkElement;
            if (s == null) return;
            var t = s.DataContext as string;
            if (t == null) return;

            if (t == AppResources.MainPage_OtherText_Option)
                NavToInner(() => App.NavToOption());
            else if (t == AppResources.MainPage_OtherText_Account)
                NavToInner(() => CustomUriMapper.NavToAccountPage());
#if DEBUG
            else if (t == ChangeLicense)
                AppCore.ChangedLicense();
            else
                throw new NotImplementedException();
#endif
        }

        private void DisablePage(string msg)
        {
            this.IsEnabled = false;

            this.MaskText.Text = msg;
            this.MaskGrid.Visibility = System.Windows.Visibility.Visible;
        }

        private void EnablePage()
        {
            this.Dispatcher.BeginInvoke(() =>
                {
                    this.IsEnabled = true;

                    this.MaskGrid.Visibility = System.Windows.Visibility.Collapsed;
                });
        }
    }
}