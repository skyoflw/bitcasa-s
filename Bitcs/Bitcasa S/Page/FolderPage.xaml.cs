﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Bitcasa_S.Model;
using BitcasaSDK_WP.BObj;
using Bitcasa_S.Ctrl;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using Bitcasa_S.Resources;
using Bitcasa_S.Packet;
using BitcasaSDK_WP;
using BCache;
using BDB2;

namespace Bitcasa_S.Page
{
    public partial class FolderPage : PhoneApplicationPage, IPageExt
    {
        private const string ScrollToPath64 = "ScrollToPath64";

        /// <summary>
        /// 正常应用栏
        /// </summary>
        private ApplicationBar NormalApplicationBar
        {
            get { return this.Resources["NormalApplicationBar"] as ApplicationBar; }
        }
        /// <summary>
        /// 付费用户的多选应用栏
        /// </summary>
        private ApplicationBar PaidMultiSelectApplicationBar
        {
            get { return this.Resources["PaidMultiSelectApplicationBar"] as ApplicationBar; }
        }

        FolderModel CurrentFolderModel;

        public FolderPage()
        {
            InitializeComponent();

            // 设置动画完结事件
            this.OpenFolderOut.Completed += (a, b) =>
                {
                    //UpdateModel();
                    this.OpenFolderIn.Begin();
                };
            this.OpenFolderIn.Completed += (a, b) =>
                {
                };

            this.CloseFolderOut.Completed += (a, b) =>
                {
                    //UpdateModel();
                    this.CloseFolderIn.Begin();
                };
            this.CloseFolderIn.Completed += (a, b) =>
                {
                    CloseFolderOutRunning = false;
                };

            this.ApplicationBar = NormalApplicationBar;

            App.ConfigPage(this);
        }

        #region tag

        public bool IsOpenPopup;

        private bool IsMultiSelectStatus = false;

        private bool IsCoverUpContentListSelectionChanged = false;

        #endregion

        #region nav

        private bool CloseFolderOutRunning = false;

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (IsMultiSelectStatus)
            {
                ClearSelect(null, null);
                e.Cancel = true;
                return;
            }

            if (IsOpenPopup) return;

            //if (CloseFolderOutRunning)
            //{
            //    e.Cancel = true;
            //    return;
            //}

            base.OnBackKeyPress(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (this.CurrentFolderModel != null)
                this.CurrentFolderModel.CancelLoad();

            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            
            string path64;
            object scrollTo; // string

            if (NavigationContext.QueryString.TryGetValue("path", out path64))
            {
                CurrentFolderModel = new FolderModel(path64);

                CurrentFolderModel.ScrollToEvent += (a, b) =>
                    {
                        this.Dispatcher.BeginInvoke(() => { this.ContentList.ScrollTo(b); });
                    };

                if (this.State.TryGetValue(ScrollToPath64, out scrollTo))
                {
                    var scrollToStr = scrollTo as string;
                    if (scrollToStr != null)
                    {
                        CurrentFolderModel.ScrollTo(scrollToStr);
                    }
                }

                CurrentFolderModel.ObjectNotFound += (a, b) =>
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        if (this.NavigationService.CanGoBack)
                            this.NavigationService.GoBack();
                        else
                            App.Current.Terminate();
                    });
                };

                this.ChooseAutoUploadFolder.Visibility =
                    CurrentFolderModel.IsShowChooseAutoUploadFolder.ConvToVisibility();
                this.ChoosePaste.Visibility =
                    AppCore.AppClipboard.HaveValue.ConvToVisibility();

                this.DataContext = CurrentFolderModel;

                if (!CurrentFolderModel.IsLoadDataCompleted)
                    CurrentFolderModel.LoadAsync();
            }
        }

        #endregion

        private void TapFileSystem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var obj = (sender as FrameworkElement).DataContext as BObjectForFolderList;

            var fs = obj.Context as BFileSystem;

            if (fs != null && fs.IsFile)
            {
                if (obj.IsCached)
                {
                    // 暂存页面状态
                    this.State[ScrollToPath64] = obj.Context.Path64;

                    CurrentFolderModel.OpenFile(fs);
                }
                else
                {
                    var task = CurrentFolderModel.DownloadFile(fs);
#if DEBUG
                    if (task == null) throw new ArgumentNullException();
#else
                    if (task == null) return;
#endif
                    task.TaskSucceeded += (a, cf) =>
                        {
                            obj.IsCaching = false;
                            obj.IsCached = true;

                            // 暂存页面状态
                            this.State[ScrollToPath64] = obj.Context.Path64;

                            cf.LaunchFile();
                            try { cf.UpdateToDB(); }
                            catch { } // 可能导致数据库错误
                        };
                    task.TaskCompleted += (a, b) =>
                        {
                            if (!obj.IsCached)
                                obj.IsCaching = true;
                        };

                    if (task.IsRunning || task.IsWaitForRun)
                    {
                        DisablePage();

                        DownloadPopup pop = new DownloadPopup();
                        pop.PopupClose += (a, b) => EnablePage();

                        pop.Show(task);

                        //PopupProgress p = new PopupProgress(LayoutRoot);
                        //p.PopupClose += (a, b) => { this.EnablePage(); };
                        //DisablePage();
                        //p.Show(AppResources.FolderPage_MessageBox_Download_Title,
                        //    String.Format(AppResources.FolderPage_MessageBox_Download_Message, fs.Name),
                        //    task);
                    }
                }
            }
            else // BRoot or Folder
            {
                // 取消页面载入请求
                CurrentFolderModel.CancelLoad();

                // 暂存页面状态
                this.State[ScrollToPath64] = obj.Context.Path64;

                // 开始导航
                CustomUriMapper.NavToFolderPage(obj.Context.Path64);
            }
        }

        private void CreateFolder(object sender, EventArgs e)
        {
            this.ShowPopupInput<FolderPage>
                (AppResources.FolderPage_MessageBox_CreateFolder_Title,
                 AppResources.FolderPage_MessageBox_CreateFolder_Message,
                 r => // succeed
                 {
                     if (!BFileSystem.IsValid(r))
                     {
                         MessageBox.Show("");
                         return false;
                     }
                     return true;
                 },
                 result => CurrentFolderModel.CreateNewFolder(result), null);
        }

        private void RefreshList(object sender, EventArgs e)
        {
            CurrentFolderModel.LoadAsync(true);
        }

        #region context menu operation

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            var menu = (ContextMenu)sender;
            var owner = (FrameworkElement)menu.Owner;
            if (owner.DataContext != menu.DataContext)
                menu.DataContext = owner.DataContext;

            foreach (var item in menu.Items)
            {
                var m = item as MenuItem;
                if (m == null) continue;

                var title = m.Header as string;
                if (title == null) continue;

                switch (title)
                {
                    case "delete":
                        m.Header = AppResources.FolderPage_ContextMenu_Delete;
                        break;
                    case "rename":
                        m.Header = AppResources.FolderPage_ContextMenu_Rename;
                        break;
                    case "deleteCache":
                        m.Header = AppResources.FolderPage_ContextMenu_DeleteCache;
                        break;
                    case "streamFile":
                        m.Header = AppResources.FolderPage_ContextMenu_StreamFile;
                        break;
                }
            }
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            var obj = (sender as FrameworkElement).DataContext as BObjectForFolderList;
            
            if (obj == null) return;

            var fs = obj.Context as BFileSystem;

            if (fs == null) return;

            string msg = String.Format(AppResources.FolderPage_MessageBox_DeleteOnce_Message, obj.Name);

            if (MessageBox.Show(msg, 
                AppResources.FolderPage_MessageBox_DeleteOnce_Title, 
                MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                CurrentFolderModel.DeleteFileSystemFromBitcasa(fs);
        }

        private void Rename(object sender, RoutedEventArgs e)
        {
            var obj = (sender as FrameworkElement).DataContext
                as BObjectForFolderList;
            if (obj == null) return;
            var fs = obj.Context as BFileSystem;
            if (fs == null) return;

            this.ShowPopupInput<FolderPage>
                (AppResources.FolderPage_MessageBox_Rename_Title,
                 AppResources.FolderPage_MessageBox_Rename_Message,
                 r => // succeed
                 {
                     if (!BFileSystem.IsValid(r))
                     {
                         MessageBox.Show("");
                         return false;
                     }
                     return true;
                 },
                 result => CurrentFolderModel.Rename(fs, result),
                 null, fs.Name);
        }

        private void DeleteCache(object sender, RoutedEventArgs e)
        {
            var fs = (sender as FrameworkElement).DataContext as BObjectForFolderList;
            if (fs == null)
                return;

            CurrentFolderModel.LocalDeleteCache(fs);
        }

        private void StreamFile(object sender, RoutedEventArgs e)
        {
            var obj = (sender as FrameworkElement).DataContext as BObjectForFolderList;
            if (obj == null) return;
            var fs = obj.Context as BFileSystem;
            if (fs == null) return;

            this.CurrentFolderModel.StreamFile(fs);
        }

        #endregion

        private void Upload(object sender, EventArgs e)
        {
            var task = new PhotoChooserTask();
            task.Completed += (s, a) =>
            {
                if (a.TaskResult == TaskResult.OK)
                    CurrentFolderModel.UploadFile(
                        a.ChosenPhoto, 
                        System.IO.Path.GetFileName(a.OriginalFileName));
            };
            task.ShowCamera = true;
            task.Show();
        }

        private void ContentList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            return; // 屏蔽多选

            if (IsCoverUpContentListSelectionChanged) return;
            if (!AppCore.IsPaid) return;
            
            var items = ContentList.SelectedItems;
            this.IsMultiSelectStatus = items.Count != 0;

            if (!IsMultiSelectStatus) this.ApplicationBar = NormalApplicationBar;
            else this.ApplicationBar = PaidMultiSelectApplicationBar;

            App.UpdateAppBarButtonText(this);
        }

        #region paid multi select operation

        private void DeleteSelect(object sender, EventArgs e)
        {
            App.ShowNotRealizeError();
        }

        private void CopySelect(object sender, EventArgs e)
        {
            App.ShowNotRealizeError();
        }

        private void CutSelect(object sender, EventArgs e)
        {
            App.ShowNotRealizeError();
        }

        private void ClearSelect(object sender, EventArgs e)
        {
            IsMultiSelectStatus = false;
            IsCoverUpContentListSelectionChanged = true;
            ContentList.SelectedItems.Clear();
            IsCoverUpContentListSelectionChanged = false;
            ContentList_SelectionChanged(null, null);
        }

        #endregion

        #region choose

        private void ChooseAsAutoUploadFolder(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.CurrentFolderModel.SetAsAutoUploadFolder();

            this.ChooseAutoUploadFolder.Visibility =
                CurrentFolderModel.IsShowChooseAutoUploadFolder.ConvToVisibility();
        }

        private void ChooseAsPaste(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.CurrentFolderModel.Paste();

            this.ChoosePaste.Visibility =
                AppCore.AppClipboard.HaveValue.ConvToVisibility();
        }

        #endregion

        public void DisablePage()
        {
            this.IsOpenPopup = true;
            this.ContentList.IsEnabled = false;
            //Mask.Visibility = System.Windows.Visibility.Visible;
            if (this.ApplicationBar != null)
                this.ApplicationBar.IsVisible = false;
        }

        public void EnablePage()
        {
            this.IsOpenPopup = false;
            this.Dispatcher.BeginInvoke(() =>
            {
                this.ContentList.IsEnabled = true;
                Mask.Visibility = System.Windows.Visibility.Collapsed;
                if (this.ApplicationBar != null)
                    this.ApplicationBar.IsVisible = true;
            });
        }

        public Panel GetLayoutRoot()
        {
            return this.LayoutRoot;
        }
    }
}