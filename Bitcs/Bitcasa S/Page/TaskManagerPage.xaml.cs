﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Bitcasa_S.Page
{
    public partial class TaskManagerPage : PhoneApplicationPage
    {
        public TaskManagerPage()
        {
            InitializeComponent();
            App.ConfigPage(this);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            UploadList.DataContext = AppCore.UploadFileManager;
        }
    }
}