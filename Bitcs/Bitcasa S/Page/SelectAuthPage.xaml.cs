﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;
using Microsoft.Phone.Tasks;

namespace Bitcasa_S.Page
{
    public partial class SelectAuthPage : PhoneApplicationPage
    {
        public SelectAuthPage()
        {
            InitializeComponent();

            App.ConfigPage(this);
        }

        private void TapLogin(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.NavToAuth();
        }

        private void TapSignin(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri(SignInURL, UriKind.Absolute);
            task.Show();
        }

        private const string SignInURL = "https://my.bitcasa.com/signup";

        private void TapCloseApp(object sender, RoutedEventArgs e)
        {
            App.Current.Terminate();
        }
    }
}