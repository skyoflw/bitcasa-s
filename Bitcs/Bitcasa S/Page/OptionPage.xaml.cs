﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Bitcasa_S.Resources;

namespace Bitcasa_S.Page
{
    public partial class OptionPage : PhoneApplicationPage
    {
        public OptionPage()
        {
            InitializeComponent();
            App.ConfigPage(this);

            var optionText = new Item[]
            {
#if DEBUG
                new Item(
                    AppResources.OptionPage_Option_General_Name,
                    AppResources.OptionPage_Option_General_Message),
#endif
                new Item(
                    AppResources.OptionPage_Option_Upload_Name,
                    AppResources.OptionPage_Option_Upload_Message),
                new Item(
                    AppResources.OptionPage_Option_AutoUpload_Name,
                    AppResources.OptionPage_Option_AutoUpload_Message),
                new Item(
                    AppResources.OptionPage_Option_About_Name,
                    AppResources.OptionPage_Option_About_Message)
            };
            this.OptionList.ItemsSource = optionText;
        }

        private void TapOptionItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var pan = sender as FrameworkElement;
            if (pan == null) return;
            var item = pan.DataContext as Item;
            if (item == null) return;

            if (item.Name == AppResources.OptionPage_Option_General_Name)
                CustomUriMapper.NavToXaml(CustomUriMapper.Option.GeneralUrl);

            else if (item.Name == AppResources.OptionPage_Option_Upload_Name)
                CustomUriMapper.NavToXaml(CustomUriMapper.Option.UploadPageUrl);

            else if (item.Name == AppResources.OptionPage_Option_AutoUpload_Name)
                CustomUriMapper.NavToXaml(CustomUriMapper.Option.AutoUploadUrl);

            else if (item.Name == AppResources.OptionPage_Option_About_Name)
                CustomUriMapper.NavToXaml(CustomUriMapper.Option.AboutUrl);
#if DEBUG
            else
                throw new NotImplementedException();
#endif
        }

        public class Item
        {
            public Item(string name, string msg) { this.Name = name; this.Message = msg; }
            public string Name { get; private set; }
            public string Message { get; private set; }
        }
    }
}