﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Bitcasa_S.Model;
using Bitcasa_S.Resources;

namespace Bitcasa_S.Page
{
    public partial class AccountPage : PhoneApplicationPage
    {
        private AccountModel Model;

        public AccountPage()
        {
            InitializeComponent();
            App.ConfigPage(this);
            Model = AppCore.Data.AccountModel;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.DataContext = Model;
            Model.LoadAsync();
        }

        private void Logout(object sender, RoutedEventArgs e)
        {

        }

        private void TapReferralLink(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (Model.UserProfile != null)
            {
                if (!String.IsNullOrWhiteSpace(Model.UserProfile.ReferralLink))
                {
                    Clipboard.SetText(Model.UserProfile.ReferralLink);
                    MessageBox.Show(AppResources.AccountPage_ReferralLink_Copy);
                }
            }
        }
    }
}