﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Bitcasa_S.Model;
using Bitcasa_S.Packet;
using Bitcasa_S.Resources;

namespace Bitcasa_S.Page
{
    public partial class CachePage : PhoneApplicationPage
    {
        private CacheModel Model;

        private ApplicationBarIconButton allDeleteButton;

        public CachePage()
        {
            InitializeComponent();
            App.ConfigPage(this);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            foreach (var btn in this.ApplicationBar.Buttons)
            {
                var btnIcon = btn as ApplicationBarIconButton;
                if (btnIcon != null)
                {
                    if (btnIcon.Text == AppResources.ApplicationBar_Button_DeleteSelect)
                        allDeleteButton = btnIcon;
                }
            }

            base.OnNavigatedTo(e);

            this.Model = new CacheModel();
            this.Model.LoadAsync();

            this.DataContext = this.Model;

            if (allDeleteButton != null)
            {
                var count = this.ContentList.SelectedItems.Count;
                allDeleteButton.IsEnabled = count != 0;
            }
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            var obj = (sender as FrameworkElement).DataContext as CacheFileForCacheList;

            if (obj == null) return;

            this.Model.ShowList.Remove(obj);

            if (this.Model != null)
            {
                this.Model.ForceDeleteCache(obj);
            }
        }

        private void Grid_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var obj = (sender as FrameworkElement).DataContext as CacheFileForCacheList;

            if (obj == null) return;

            if (this.Model != null)
            {
                this.Model.LaunchFile(obj.Context);
            }
        }

        private void ContentList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (allDeleteButton != null)
            {
                var count = this.ContentList.SelectedItems.Count;
                allDeleteButton.IsEnabled = count != 0;
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            var count = this.ContentList.SelectedItems.Count;

            if (count > 0)
            {
                List<CacheFileForCacheList> items = new List<CacheFileForCacheList>();

                foreach (var j in this.ContentList.SelectedItems)
                {
                    var item = j as CacheFileForCacheList;

                    if (item != null)
                    {
                        items.Add(item);
                    }
                }

                items.ForEach(z => this.Model.ShowList.Remove(z));

                this.Model.ForceDeleteCache(items);
            }
        }
    }
}