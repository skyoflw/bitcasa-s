﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Bitcasa_S.Page
{
    public interface IPageExt
    {
        void DisablePage();

        void EnablePage();

        Panel GetLayoutRoot();
    }
}
