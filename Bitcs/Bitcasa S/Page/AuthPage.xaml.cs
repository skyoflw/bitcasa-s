﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BitcasaSDK_WP;
using BitcasaSDK_WP.BConst;
using BSetting;
using Bitcasa_S.Resources;
using BitcasaSDK_WP.Parameter;
using BitcasaSDK_WP.BObj;
using Microsoft.Phone.Scheduler;

namespace Bitcasa_S.Page
{
    public partial class AuthPage : PhoneApplicationPage
    {
        private const string CallbackUrl = "http://localhost/Bitcasa_S";

        public AuthPage()
        {
            InitializeComponent();
            App.ConfigPage(this);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Refresh(null, null);
        }

        // Generate a unique name for the new notification. You can choose a
        // name that is meaningful for your app, or just use a GUID.
        private const string NotificationName = "FCD713EC-756B-42B5-8E61-59F340288616";

        private void GrantAccessToken(string oauthVerifier)
        {
            ConfigProgressBar(true, AppResources.AuthPage_Message_GettingToken);

            BitcasaClient.LoginAsync
                (
                    AppCore.ClientSecret, oauthVerifier,
                    (client) =>
                    {
                        AppCore.Client = client;
                        AppCore.Token = client.Token;
                        Setting.Token = client.Token;


                        // 为 token 过期创建提醒
                        var dt = DateTime.Now + TimeSpan.FromDays(31);

                        Reminder reminder = new Reminder(NotificationName);

                        reminder.Title = AppResources.TokenTimeoutReminder_Title;
                        reminder.Content = AppResources.TokenTimeoutReminder_Content;
                        reminder.BeginTime = DateTime.Now + TimeSpan.FromDays(30);
                        reminder.ExpirationTime = DateTime.Now + TimeSpan.FromDays(31);
                        reminder.RecurrenceType = RecurrenceInterval.None;
                        reminder.NavigationUri = new Uri("/Page/MainPage.xaml", UriKind.Relative);

                        // Register the reminder with the system.
                        if (ScheduledActionService.Find(NotificationName) != null)
                            ScheduledActionService.Replace(reminder);
                        else
                            ScheduledActionService.Add(reminder);

                        
                        // 导航到主页
                        this.Dispatcher.BeginInvoke(() => { App.NavToMain(); });
                    },
                    (excp) =>
                    {
                        ConfigProgressBar(false, null);

                        this.Dispatcher.BeginInvoke(() =>
                        {
                            if (MessageBox.Show(AppResources.AuthPage_Login_CanNotGetToken,
                                AppResources.MessageBox_Caption_Warning, MessageBoxButton.OKCancel)
                                == MessageBoxResult.OK)
                                GrantAccessToken(oauthVerifier);
                            else
                                this.WebBrowser.Visibility = System.Windows.Visibility.Visible;
                        });
                    }
                );
        }

        private void WebBrowser_Navigating(object sender, NavigatingEventArgs e)
        {
            ConfigProgressBar(true, AppResources.AuthPage_Message_Navigating);

            if (!e.Uri.IsAbsoluteUri) return;

            if (!e.Uri.AbsoluteUri.StartsWith(CallbackUrl)) return;

            var oauthVerifier = e.Uri.Query.Replace("?", "").Split('&')
                .Where(s => s.Split('=')[0] == BText.AUTHORIZATION_CODE)
                .Select(s => s.Split('=')[1])
                .FirstOrDefault();

            if (String.IsNullOrEmpty(oauthVerifier))
            {
                MessageBox.Show(AppResources.AuthPage_Message_GetCodeFailed);
                return;
            }

            this.WebBrowser.Visibility = System.Windows.Visibility.Collapsed;

            e.Cancel = true;

            GrantAccessToken(oauthVerifier);
        }

        private void WebBrowser_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            ConfigProgressBar(true, AppResources.AuthPage_Message_NavigationFailed);
        }

        private void WebBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            ConfigProgressBar(false, AppResources.AuthPage_Message_Navigated);
        }

        private void ConfigProgressBar(bool enable, string message)
        {
            this.Dispatcher.BeginInvoke(() =>
                {
                    this.SystemTrayProgress.IsIndeterminate = enable;
                    this.SystemTrayProgress.Text = message;
                });
        }

        private void Refresh(object sender, ApplicationBarStateChangedEventArgs e)
        {
            var url = BitcasaClient.AuthUrl(AppCore.ClientId, CallbackUrl);
            this.WebBrowser.Navigate(new Uri(url, UriKind.Absolute));
        }
    }
}