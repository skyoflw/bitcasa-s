﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BitcasaSDK_WP.BRequest;
using BSetting;

namespace Bitcasa_S.Page.Option
{
    public partial class UploadPage : PhoneApplicationPage
    {
        public UploadPage()
        {
            InitializeComponent();
            App.ConfigPage(this);

            switch (GetValue())
            {
                case ExistsStatus.Rename:
                    ExistsMode_Rename.IsChecked = true;
                    break;
                case ExistsStatus.Fail:
                    ExistsMode_Fail.IsChecked = true;
                    break;
                case ExistsStatus.Overwrite:
                    ExistsMode_Overwrite.IsChecked = true;
                    break;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            ExistsModeCheckChangedMask = false;
        }

        private ExistsStatus GetValue()
        {
            return (ExistsStatus)Setting.UploadOption.UploadOptionExistsMode;
        }

        private void SetValue(ExistsStatus newvalue)
        {
            Setting.UploadOption.UploadOptionExistsMode = (int)newvalue;
        }

        /// <summary>
        /// ExistsModeCheckChanged 事件屏蔽器
        /// </summary>
        private bool ExistsModeCheckChangedMask = true;

        private void ExistsModeCheckChanged(object sender, RoutedEventArgs e)
        {
            if (ExistsModeCheckChangedMask) return;

            ExistsStatus newvalue = ExistsStatus.Rename;

            if (ExistsMode_Rename.IsChecked ?? false) newvalue = ExistsStatus.Rename;
            else if (ExistsMode_Fail.IsChecked ?? false) newvalue = ExistsStatus.Fail;
            else if (ExistsMode_Overwrite.IsChecked ?? false) newvalue = ExistsStatus.Overwrite;

            SetValue(newvalue);
            if (AppCore.AutoUploadManager != null)
                AppCore.AutoUploadManager.ExistsMode = newvalue;
        }
    }
}