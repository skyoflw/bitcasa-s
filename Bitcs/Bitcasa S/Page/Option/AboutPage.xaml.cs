﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.System;
using Microsoft.Phone.Tasks;
using Bitcasa_S.Resources;

namespace Bitcasa_S.Page.Option
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        private void FeedbackByEmail(object sender, RoutedEventArgs e)
        {
            Launcher.LaunchUriAsync(new Uri(AppConst.AuthorEmail));
        }

        private void Rate(object sender, RoutedEventArgs e)
        {
            var task = new MarketplaceReviewTask();
            task.Show();
        }

        private void RecommendByWindows(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(AppResources.AboutPage_Recommend_ByWindows_Message);
        }

        private void RecommendByEmail(object sender, RoutedEventArgs e)
        {
            var task = new EmailComposeTask();
            task.Subject = AppResources.AboutPage_Recommend_ByEmail_Subject;
            task.Body = AppResources.AboutPage_Recommend_ByEmail_Body;
            task.Show();
        }

        private void RecommendBySNS(object sender, RoutedEventArgs e)
        {
            ShareLinkTask shareLinkTask = new ShareLinkTask();

            shareLinkTask.Title = AppResources.AboutPage_Recommend_BySNS_Title;
            shareLinkTask.LinkUri = new Uri(AppConst.StoreLink, UriKind.Absolute);
            shareLinkTask.Message = AppResources.AboutPage_Recommend_BySNS_Message;

            shareLinkTask.Show();
        }

        private void Buy(object sender, RoutedEventArgs e)
        {
            MarketplaceDetailTask task = new MarketplaceDetailTask();
            task.ContentIdentifier = AppConst.GuidInStore;
            task.ContentType = MarketplaceContentType.Applications;
            task.Show();
        }
    }
}