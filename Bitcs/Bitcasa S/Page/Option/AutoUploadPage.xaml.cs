﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BSetting;
using Bitcasa_S.Resources;
using Microsoft.Phone.Scheduler;
using Bitcasa_S.Model;

namespace Bitcasa_S.Page.Option
{
    public partial class AutoUploadPage : PhoneApplicationPage
    {
        private AutoUploadModel AgentModelIns;

        private bool IsFirstTimeIn = true;

        public AutoUploadPage()
        {
            InitializeComponent();
            App.ConfigPage(this);
            AgentModelIns = new AutoUploadModel();
            this.DataContext = AgentModelIns;
            //AgentInfo.DataContext = AgentModelIns.Agent;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            this.EnableAutoUpload.IsChecked = Setting.IsEnableAutoUpload;

            bool ispaid = AppCore.IsPaid;
            this.UploadMode_AsSystemService.IsEnabled = ispaid;
            this.UploadContent_All.IsEnabled = ispaid;
            if (ispaid)
            {
                if (Setting.IsAutoUploadAsSystemSerive)
                    this.UploadMode_AsSystemService.IsChecked = true;
                else
                    this.UploadMode_OnlyAsync.IsChecked = true;

                if (Setting.IsAutoUploadAllPhoto)
                    this.UploadContent_All.IsChecked = true;
                else
                    this.UploadContent_OnlyCameraRoll.IsChecked = true;
            }
            else
            {
                this.UploadMode_OnlyAsync.IsChecked = true;
                Setting.IsAutoUploadAsSystemSerive = false;
                this.UploadContent_OnlyCameraRoll.IsChecked = true;
                Setting.IsAutoUploadAllPhoto = false;
            }

            if (CustomUriMapper.IsFrom_ConfigurePhotosUploadSettings)
            {
                CustomUriMapper.IsFrom_ConfigurePhotosUploadSettings = false;
                if (AppCore.IsPaid)
                    MessageBox.Show(AppResources.AutoUploadPage_MessageBox_PleaseCheckMode);
                else
                    MessageBox.Show(AppResources.AutoUploadPage_MessageBox_OnlyPaid);
            }

            RefreshFolderName();

            if (IsFirstTimeIn)
            {
                IsFirstTimeIn = false;
                this.EnableAutoUpload.Checked += EnableAutoUploadChanged;
                this.EnableAutoUpload.Unchecked += EnableAutoUploadChanged;

                this.UploadMode_OnlyAsync.Checked += UploadModeChanged;
                //this.UploadMode_OnlyAsync.Unchecked += UploadModeChanged;

                this.UploadMode_AsSystemService.Checked += UploadModeChanged;
                //this.UploadMode_AsSystemService.Unchecked += UploadModeChanged;

                this.UploadContent_OnlyCameraRoll.Checked += UploadContentChanged;
                //this.UploadContent_OnlyCameraRoll.Unchecked += UploadContentChanged;

                this.UploadContent_All.Checked += UploadContentChanged;
                //this.UploadContent_All.Unchecked += UploadContentChanged;
            }
        }

        private bool IsBlock_EnableAutoUploadChanged = false;
        private void EnableAutoUploadChanged(object sender, RoutedEventArgs e)
        {
            if (IsBlock_EnableAutoUploadChanged) return;
            IsBlock_EnableAutoUploadChanged = true;
            bool status = this.EnableAutoUpload.IsChecked ?? false;

            if (!Result())
                this.EnableAutoUpload.IsChecked = false;

            IsBlock_EnableAutoUploadChanged = false;
        }

        private void UploadModeChanged(object sender, RoutedEventArgs e)
        {
            if (this.UploadMode_AsSystemService != null)
            {
                bool is_system_service = this.UploadMode_AsSystemService.IsChecked ?? false;
                
                if (!Result())
                {
                    this.UploadMode_AsSystemService.IsChecked = false;
                    this.UploadMode_OnlyAsync.IsChecked = true;
                }
            }
        }

        private void UploadContentChanged(object sender, RoutedEventArgs e)
        {
            if (this.UploadContent_All != null)
            {
                bool is_all = this.UploadContent_All.IsChecked ?? false;
                Setting.IsAutoUploadAllPhoto = is_all;
            }
        }

        /// <summary>
        /// 返回一个值，false 表示注册代理失败，否则为 true
        /// </summary>
        /// <returns></returns>
        private bool Result()
        {
            bool enable_status_ui = this.EnableAutoUpload.IsChecked ?? false;
            bool mode_as_serive_ui = this.UploadMode_AsSystemService.IsChecked ?? false;
            bool enable_status_setting = Setting.IsEnableAutoUpload;
            bool mode_as_serive_setting = Setting.IsAutoUploadAsSystemSerive;

            if (enable_status_ui == enable_status_setting && 
                mode_as_serive_ui == mode_as_serive_setting)
                return true;

#if DEBUG
            if (enable_status_ui != enable_status_setting &&
                mode_as_serive_ui != mode_as_serive_setting)
                throw new Exception();
#endif
            Setting.IsEnableAutoUpload = enable_status_ui;
            Setting.IsAutoUploadAsSystemSerive = mode_as_serive_ui;

            if (enable_status_ui != enable_status_setting)
            {
                if (!enable_status_ui)
                {
                    DisableAgent();
                    DisableAsync();
                    Setting.ClearAutoUploadFolder();
                }
                else
                {
                    if (mode_as_serive_ui)
                    {
                        if (!EnableAgent(true))
                        {
                            MessageBox.Show(AppResources.AutoUploadPage_MessageBox_EnableFailedForAgent);
                            Setting.IsEnableAutoUpload = false;
                            return false;
                        }
                    }
                    else EnableAsync();
                }
            }
            else
            {
                if (mode_as_serive_ui)
                {
                    DisableAsync();
                    if (!EnableAgent(true))
                    {
                        MessageBox.Show(AppResources.AutoUploadPage_MessageBox_EnableFailedForAgent);
                        Setting.IsAutoUploadAsSystemSerive = false;
                        return false;
                    }
                }
                else
                {
                    DisableAgent();
                    EnableAsync();
                }
            }

            if (Setting.AutoUploadFolder == null && enable_status_ui)
                MessageBox.Show(AppResources.AutoUploadPage_MessageBox_EnableSuccessed);

            return true;
        }

        private bool EnableAgent(bool reset = false)
        {
            return AgentModelIns.Enable(reset);
        }

        private bool DisableAgent()
        {
            return AgentModelIns.Disable();
        }

        private void EnableAsync()
        {
            AppCore.TryInitAutoUploadManager();
        }

        private void DisableAsync()
        {
            if (AppCore.AutoUploadManager != null)
            {
                AppCore.AutoUploadManager.DisableAysnc();
                AppCore.AutoUploadManager = null;
            }
        }

        private void RefreshFolderName()
        {
            if (Setting.AutoUploadFolder != null)
            {
                this.FolderName.Text =
                    Setting.AutoUploadFolderForHuman;
                this.ResetFolderButton.IsEnabled = true;
            }
            else
            {
                this.FolderName.Text = 
                    AppResources.AutoUploadPage_UploadFolder_UndefineValue;
                this.ResetFolderButton.IsEnabled = false;
            }
        }

        private void ResetFolder(object sender, RoutedEventArgs e)
        {
            Setting.ClearAutoUploadFolder();
            RefreshFolderName();
            if (Setting.IsEnableAutoUpload)
                MessageBox.Show(AppResources.AutoUploadPage_MessageBox_EnableSuccessed);
        }
    }
}