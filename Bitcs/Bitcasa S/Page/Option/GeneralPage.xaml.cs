﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Bitcasa_S.Resources;

namespace Bitcasa_S.Page.Option
{
    public partial class GeneralPage : PhoneApplicationPage
    {
        public GeneralPage()
        {
            InitializeComponent();
            App.ConfigPage(this);

            this.AppLock.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}