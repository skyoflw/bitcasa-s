﻿using Bitcasa_S.Ctrl;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Page
{
    public static class PageExt
    {
        public static void ShowPopupInput<TPage>(
            this TPage page,
            string caption, string msg,
            Func<string, bool> selector,
            Action<string> succeed,
            Action cancelled, string text = null)
            where TPage : PhoneApplicationPage, IPageExt
        {
            if (page.ApplicationBar != null)
                page.ApplicationBar.IsVisible = false;

            page.DisablePage();

            PopupInput inputbox = new PopupInput(page.GetLayoutRoot());

            if (text != null)
                inputbox.Text = text;

            inputbox.Result += (a, b) =>
            {
                bool shouldClose = false;
                if (b)
                {
                    string resultText = inputbox.Text;

                    var result = selector == null ?
                        true : selector(resultText);

                    if (result)
                    {
                        shouldClose = true;
                        if (succeed != null)
                            succeed(resultText);
                    }
                }
                else
                {
                    shouldClose = true;
                    if (cancelled != null)
                        cancelled();
                }

                if (shouldClose)
                {
                    if (page.ApplicationBar != null)
                        page.ApplicationBar.IsVisible = true;

                    inputbox.Close();
                    page.EnablePage();
                }
            };

            inputbox.Show(caption, msg);
        }
    }
}
