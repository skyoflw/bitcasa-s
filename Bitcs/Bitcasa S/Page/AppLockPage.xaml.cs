﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Bitcasa_S.Page
{
    public partial class AppLockPage : PhoneApplicationPage
    {
        public AppLockPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            this.Password.Focus();
        }

        private void Password_GotFocus(object sender, RoutedEventArgs e)
        {
            
        }

        private void Password_LostFocus(object sender, RoutedEventArgs e)
        {

        }
    }
}