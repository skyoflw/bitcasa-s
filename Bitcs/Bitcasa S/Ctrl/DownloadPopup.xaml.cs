﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Controls.Primitives;
using Bitcasa_S.Task;
using Bitcasa_S.Resources;

namespace Bitcasa_S.Ctrl
{
    public partial class DownloadPopup : UserControl
    {
        public DownloadPopup()
        {
            InitializeComponent();

            this.CloseAnimation.Completed += (a, b) => _close();
        }

        private PhoneApplicationPage parentPage;

        private Panel parentPanel;

        private Popup parentPopup;

        private bool systemTrayIsVisible;

        private GetFileTask contextTask;

        public void Show(GetFileTask task)
        {
            parentPage = App.RootFrame.Content as PhoneApplicationPage;
            if (parentPage == null) return;

            parentPanel = parentPage.Content as Panel;
            if (parentPanel == null) return;

            AppCore.Exchange.IsFromRouse = false;

            this.contextTask = task;

            this.DataContext = task;

            task.TaskFailed += (a, b) =>
                {
                    App.ErrorHandler.DefaultErrorHandler(b, "download", true);
                };

            task.TaskCompleted += (a, b) =>
                {
                    switch (task.Status)
                    {
                        case BTaskStatusType.Failed:
                            if (AppCore.Exchange.IsFromRouse == true)
                            {
                                AppCore.Exchange.IsFromRouse = false;
                                task.RunAsync();
                            }
                            else
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                    {
                                        if (MessageBox.Show("can not download file, retry?", "error", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                                            task.RunAsync();
                                        else
                                            this.Close();
                                    });
                            }
                            break;
#if DEBUG
                        case BTaskStatusType.Running:
                            throw new InvalidOperationException();
#endif
                        default:
                            this.Close();
                            break;
                    }
                };

            pageDisable();

            parentPopup = new Popup();
            parentPopup.Height = 800;
            parentPopup.Width = 480;
            parentPopup.Child = this;

            parentPanel.Children.Add(parentPopup);

            this.ShowAnimation.Begin();
            
            parentPopup.IsOpen = true;
        }

        private void pageDisable()
        {
            if (parentPage == null) return;

            parentPage.BackKeyPress += parentPage_BackKeyPress;

            systemTrayIsVisible = SystemTray.IsVisible;
            SystemTray.IsVisible = false;
        }

        private void pageEnable()
        {
            if (parentPage == null) return;

            parentPage.BackKeyPress -= parentPage_BackKeyPress;

            SystemTray.IsVisible = systemTrayIsVisible;
        }

        private void parentPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;

            this.contextTask.Cancel();
            this.Close();
        }        

        private void Close()
        {
            this.Dispatcher.BeginInvoke(this.CloseAnimation.Begin);
        }

        /// <summary>
        /// 退出动画完毕调用
        /// </summary>
        private void _close()
        {
            this.Dispatcher.BeginInvoke(() =>
                {
                    if (parentPopup != null)
                    {
                        parentPopup.IsOpen = false;

                        if (parentPanel != null)
                            parentPanel.Children.Remove(parentPopup);
                    }
                    pageEnable();
                    OnPopupClose();
                });
            ConfigApplicationIdleDetectionMode(true);
        }

        private void OnPopupClose()
        {
            if (PopupClose != null)
                PopupClose(this, null);
        }
        public event EventHandler PopupClose;

        private void LockSwitch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LockSwitch.SelectedItem == null) return;

            if (LockSwitch.SelectedItem == this.LockWillStop)
            {
                ConfigApplicationIdleDetectionMode(true);
            }
            else if (LockSwitch.SelectedItem == this.LockWillNotStop)
            {
                ConfigApplicationIdleDetectionMode(false);
            }
        }

        /// <summary>
        /// 配置空闲检测。
        /// True 表示启用空闲检测，False 表示停用。
        /// 停用（False）表示锁屏时可以继续运行。
        /// </summary>
        /// <param name="isEnable"></param>
        private static bool ConfigApplicationIdleDetectionMode(bool isEnable)
        {
            bool result = false;

            var enable = isEnable ? IdleDetectionMode.Enabled : IdleDetectionMode.Disabled;

            try
            {
                if (PhoneApplicationService.Current.ApplicationIdleDetectionMode != enable)
                    PhoneApplicationService.Current.ApplicationIdleDetectionMode = enable;

                result = true;
            }
            catch (InvalidOperationException ex)
            {

                // This exception is expected in the current release.
            }

            // Possibly use the value of didEnable to decide what to do next.
            // If it is 'true', then your app will be deactivated. 
            // If it is 'false', then your app will keep running.

            return result;
        }
    }
}
