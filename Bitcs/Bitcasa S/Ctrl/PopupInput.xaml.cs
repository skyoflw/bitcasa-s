﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Controls.Primitives;

namespace Bitcasa_S.Ctrl
{
    public partial class PopupInput : UserControl
    {
        private Popup Pop;

        private Panel Panel;

        private PhoneApplicationPage Page;

        private PopupInput()
        {
            InitializeComponent();

            App.ConfigTiltEffect(this);
        }
        public PopupInput(Panel parent)
            : this()
        {
            this.Panel = parent;
            this.OutAnimation.Completed += _Close;
        }

        public void Show(string title, string message)
        {
            Page = App.RootFrame.Content as PhoneApplicationPage;
            Page.BackKeyPress += BackKeyPress;
            this.Title.Text = title;
            this.Message.Text = message;
            Pop = new Popup();
            Pop.Child = this;
            this.Panel.Children.Add(this.Pop);
            this.InAnimation.Begin();
            Pop.IsOpen = true;
            this.InputTextBox.Focus();
        }

        private void BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.OnResult(false);
            Close();
        }

        private void OK(object sender, RoutedEventArgs e)
        {
            OnResult(true);
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            OnResult(false);
        }

        private void OnResult(bool result)
        {
            if (Result != null)
                Result(this, result);
        }
        public event EventHandler<bool> Result;

        public string Text
        {
            get { return this.InputTextBox.Text; }
            set { this.InputTextBox.Text = value; }
        }

        private void _Close(object sender, EventArgs e)
        {
            if (Pop != null)
                Pop.IsOpen = false;
            Panel.Children.Remove(Pop);
        }

        public void Close()
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                this.OutAnimation.Begin();
                Page.BackKeyPress -= BackKeyPress;
            });
        }

        private void InputBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if (String.IsNullOrEmpty(this.Text))
            {
                this.OKButton.IsEnabled = false;
            }
            else
            {
                this.OKButton.IsEnabled = true;
            }
        }
    }
}
