﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Bitcasa_S.Task;
using System.Windows.Controls.Primitives;

namespace Bitcasa_S.Ctrl
{
    public partial class PopupProgress : UserControl
    {
        private PopupProgress()
        {
            InitializeComponent();
            this.OutAnimation.Completed += _Close;
        }
        public PopupProgress(Panel parent)
            : this()
        {
            this.Panel = parent;
        }

        private Popup Pop;

        private Panel Panel;

        private PhoneApplicationPage Page;

        private GetFileTask Task;

        public void Show(string title, string message, GetFileTask task)
        {
            if (!task.IsRunning)
            {
                this.Close();
                return;
            }

            Page = App.RootFrame.Content as PhoneApplicationPage;

            this.Title.Text = title;

            this.Message.Text = message;

            this.Task = task;

            this.DataContext = task;

            task.TaskCompleted += (a, b) =>
                {
                    this.Close();
                };

            Page.BackKeyPress += BackKeyPress;

            Pop = new Popup();
            Pop.Child = this;
            this.Panel.Children.Add(this.Pop);
            this.InAnimation.Begin();
            Pop.IsOpen = true;
        }

        private void BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Task.Cancel();
            Close();
        }

        public void Close()
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                this.OutAnimation.Begin();
            });
        }

        private void _Close(object sender, EventArgs e)
        {
            if (Pop != null)
                Pop.IsOpen = false;
            Panel.Children.Remove(Pop);
            if (Page != null)
                Page.BackKeyPress -= BackKeyPress;
            OnPopupClose();
        }

        private void OnPopupClose()
        {
            if (PopupClose != null)
                PopupClose(this, null);
        }
        public event EventHandler PopupClose;
    }
}
