﻿using Bitcasa_S.Model;
using BitcasaSDK_WP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSetting;
using Bitcasa_S.Task;
using BitcasaSDK_WP.BObj;
using Microsoft.Phone.Marketplace;
using System.Windows;
using AutoUpload;
using BitcasaSDK_WP.BRequest;
using BCache;
using System.IO.IsolatedStorage;

namespace Bitcasa_S
{
    public class AppCore
    {
        /// <summary>
        /// 初始化一个新的核心，以便提供给第二位登陆者
        /// </summary>
        public static void InitNewAppCore()
        {
            Token = null;
            Client = null;
            Data.Clear();
            UploadFileManager = new UploadTaskManager();
            DownloadFileManager = new DownloadTaskManager();
            AppClipboard.Paste();
            if (AutoUploadManager != null)
            {
                AutoUploadManager.DisableAysnc();
                AutoUploadManager = null;
            }
            IsInitSeriveAsync = false;
        }

        public static string Token = Setting.Token;

        #region license

        /// <summary>
        /// 检查付费许可
        /// </summary>
        public static void CheckLicense()
        {
#if !DEBUG
            IsPaid = !(new LicenseInformation()).IsTrial();
#endif
        }

#if DEBUG
        public static void ChangedLicense()
        {
            string message = "当前程序运行在 debug 模式下。点击 确认 将配置为试用版本，点击 取消 将配置为付费版本。";
            if (MessageBox.Show(message, "许可证", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                IsPaid = false;
            else
                IsPaid = true;
        }
#endif

#if DEBUG
        public static bool IsPaid = true;
#else
        /// <summary>
        /// 如果应用程序在付费许可证下运行，则为 true；否则为 false。
        /// </summary>
        public static bool IsPaid { get; private set; }
#endif

        #endregion

        #region client info & db con str

        public const string ClientId = AppInfo.ClientId;

        public const string ClientSecret = AppInfo.ClientSecret;

        private const string DBConnectionString = AppInfo.DBConnectionString;

        #endregion

        public class Password
        {
            /// <summary>
            /// 获取一个值指示是否已经上锁
            /// 用于 UriMapper 检测
            /// </summary>
            public static bool Islock { get; private set; }

            /// <summary>
            /// 尝试上锁，如果有密码的话
            /// </summary>
            public static void Lock()
            {
                if (Setting.AppPassword == null) return;
                else Islock = true;
            }

            /// <summary>
            /// 返回一个值指示是否成功解锁
            /// </summary>
            /// <param name="password"></param>
            /// <returns></returns>
            public static bool Unlock(string password)
            {
                if (password != Setting.AppPassword) return false;
                Islock = false;
                return true;
            }

            public static bool IsEnablePassword
            {
                get { return Setting.AppPassword != null; }
            }

            /// <summary>
            /// 返回一个值指示是否成功启用密码功能
            /// </summary>
            /// <param name="password"></param>
            /// <returns></returns>
            public static bool EnablePassword(string password)
            {
                if (String.IsNullOrWhiteSpace(password))
                    return false;

                Setting.AppPassword = password;
                return true;
            }

            /// <summary>
            /// 返回一个值指示是否成功关闭密码
            /// </summary>
            /// <param name="password"></param>
            /// <returns></returns>
            public static bool DisablePassword(string password)
            {
                if (password != Setting.AppPassword) return false;
                Islock = false;
                Setting.AppPassword = null;
                return true;
            }
        }

        /// <summary>
        /// SDK 已经登录的实例
        /// </summary>
        public static BitcasaClient Client;

        /// <summary>
        /// 交换区。
        /// 此处为应用标志配置区。
        /// </summary>
        public static class Exchange
        {
            /// <summary>
            /// 获取或设置一个值指示是否刚从锁屏状态唤醒。
            /// <para/>
            /// 此项会在应用被打开的时候赋值为 false，唤醒的时候赋值为 true。
            /// 对象读取此值后请手动将其赋值为 false 以防止二次唤醒。
            /// <para/>
            /// 当前允许读取权限的有：
            /// <para/>
            /// DownloadPopup
            /// </summary>
            public static bool IsFromRouse { get; set; }
        }

        public static class Data
        {
            /// <summary>
            /// 将所有数据清空
            /// </summary>
            public static void Clear()
            {
                mainModel = null;
                accountModel = null;
            }

            private static MainModel mainModel;
            /// <summary>
            /// 存放在一个位置的 Model，避免因为页面缓存无法清空数据
            /// </summary>
            public static MainModel MainModel
            {
                get
                {
                    if (mainModel == null)
                        mainModel = new MainModel();
                    return mainModel;
                }
            }

            private static AccountModel accountModel;
            /// <summary>
            /// 存放在一个位置的 Model，避免因为页面缓存导致无法清空数据
            /// </summary>
            public static AccountModel AccountModel
            {
                get
                {
                    if (accountModel == null) 
                        accountModel = new AccountModel();
                    return accountModel;
                }
            }
        }

        #region upload & download

        public static UploadTaskManager UploadFileManager = new UploadTaskManager();

        public static DownloadTaskManager DownloadFileManager = new DownloadTaskManager();

        #endregion

        public static class AppClipboard
        {
            /// <summary>
            /// 返回值不为 null
            /// </summary>
            public static BFileSystem[] Content { get; private set; }
            /// <summary>
            /// 指示剪贴板是否有内容
            /// </summary>
            public static bool HaveValue
            {
                get
                {
                    if (Content == null) return false;
                    return Content.Length > 0;
                }
            }
            /// <summary>
            /// 指示剪贴板的内容是否移动，否则为复制
            /// </summary>
            public static bool IsMove { get; private set; }

            public static void Copy(IEnumerable<BFileSystem> fss)
            {
                Content = fss.ToArray();
                IsMove = false;
            }
            public static void Move(IEnumerable<BFileSystem> fss)
            {
                Content = fss.ToArray();
                IsMove = true;
            }
            public static BFileSystem[] Paste()
            {
                var r = Content;
                Content = new BFileSystem[] { };
                return r;
            }
        }

        public static AutoUploadManager AutoUploadManager;

        #region serive

        /// <summary>
        /// 标记是否已经开启服务
        /// </summary>
        private static bool IsInitSeriveAsync = false;
        /// <summary>
        /// 在空闲的时候手动调用此方法
        /// </summary>
        public static void InitSeriveAsync()
        {
            if (IsInitSeriveAsync) return; IsInitSeriveAsync = true;

            // auto upload photo
            TryInitAutoUploadManager();
            System.Threading.Tasks.Task.Run(() =>
                {
                    // other serives
                });
        }

        public static void TryInitAutoUploadManager()
        {
            if (AutoUploadManager != null) return;

            // auto upload photo
            if (Setting.IsEnableAutoUpload && !Setting.IsAutoUploadAsSystemSerive)
            {
                if (Setting.AutoUploadFolder != null)
                {
                    UploadMode mode = Setting.IsAutoUploadAllPhoto ?
                        UploadMode.AllPhoto : UploadMode.OnlyCameraRoll;
                    if (AutoUploadManager == null)
                    {
                        AutoUploadManager = new AutoUploadManager(
                            Client, mode, Setting.AutoUploadFolder);
                        AutoUploadManager.ExistsMode = (ExistsStatus)Setting.UploadOption.UploadOptionExistsMode;
                        AutoUploadManager.RunAsync();
                    }
                }
            }
        }

        #endregion
    }
}
