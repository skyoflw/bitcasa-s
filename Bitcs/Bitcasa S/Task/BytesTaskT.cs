﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP;

namespace Bitcasa_S.Task
{
    public abstract class BytesTask<TSucceeded, TFailed> : Task<TSucceeded, TFailed>
    {
        #region bytes member

        private long __currentBytes;
        public long CurrentBytes
        {
            get { return __currentBytes; }
            set
            {
                if (__currentBytes != value)
                {
                    __currentBytes = value;
                    OnNotifyPropertyChanged("CurrentBytes");
                    OnNotifyPropertyChanged("CurrentBytesFormat");

                    var progress =
                        this.TotalBytes > 0 ?
                    (Convert.ToDouble(value) /
                    Convert.ToDouble(this.TotalBytes) * 100) : 0;
#if DEBUG
                    if (progress < 0 || progress > 100)
                        throw new ArgumentOutOfRangeException(
                            "progress can not be the value");
#endif
                    this.Progress = 
                        progress < 0 ? 0 : 
                        (progress > 100 ? 100 : progress);
                }
            }
        }
        public string CurrentBytesFormat
        { get { return this.CurrentBytes.FileSizeFormat(); } }

        public abstract long TotalBytes { get; }
        public string TotalBytesFormat
        { get { return TotalBytes.FileSizeFormat(); } }

        #endregion

        #region status tag

        public override string WaitForRunText
        {
            get { return "waiting"; }
        }

        public override string CancelledText
        {
            get { return "cancelled"; }
        }

        public override string SucceededText
        {
            get { return "completed"; }
        }

        public override string FailedText
        {
            get { return "failed"; }
        }

        public string FailedText_Line2
        {
            get { return "retry?"; }
        }

        #endregion
    }
}
