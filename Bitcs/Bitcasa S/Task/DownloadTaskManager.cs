﻿using BCache;
using BitcasaSDK_WP.BException;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Task
{
    public class DownloadTaskManager : TaskManager<GetFileTask, CacheFile, BitcasaException>
    {
        public GetFileTask TryGetTask(string fildId)
        {
            GetFileTask task = null;

            lock (this.TaskList)
                task = this.TaskList.Where(z => z.File.Id == fildId).FirstOrDefault();

            return task;
        }
    }
}
