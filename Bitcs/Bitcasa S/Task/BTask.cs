﻿using Bitcasa_S.Packet;
using BitcasaSDK_WP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Task
{
    public abstract class BTask : CNotifyPropertyChanged, ICancelled
    {
        public BTask()
        {
            status = BTaskStatusType.IsWaitForRun;
        }

        /// <summary>
        /// 获取或设置一个值指示该任务是否为后台任务。
        /// 前台任务指用户手动点击触发的任务。
        /// </summary>
        public bool IsBackgroundTask = true;

        /// <summary>
        /// 任务等待文本
        /// </summary>
        public abstract string WaitForRunText { get; }
        /// <summary>
        /// 任务运行中文本
        /// </summary>
        public abstract string RunningText { get; }
        /// <summary>
        /// 任务取消文本
        /// </summary>
        public abstract string CancelledText { get; }
        /// <summary>
        /// 任务成功文本
        /// </summary>
        public abstract string SucceededText { get; }
        /// <summary>
        /// 任务失败文本
        /// </summary>
        public abstract string FailedText { get; }

        private BTaskStatusType status;
        public BTaskStatusType Status
        {
            get { return status; }
            protected set
            {
                if (status != value)
                {
                    status = value;
                    OnNotifyPropertyChanged("IsWaitForRun");
                    OnNotifyPropertyChanged("IsRunning");
                    OnNotifyPropertyChanged("IsCancelled");
                    OnNotifyPropertyChanged("IsSucceeded");
                    OnNotifyPropertyChanged("IsFailed");
                }
            }
        }

        #region status tag for bind

        public bool IsWaitForRun
        {
            get { return Status == BTaskStatusType.IsWaitForRun; }
        }
        public bool IsRunning
        {
            get { return Status == BTaskStatusType.Running; }
        }
        public bool IsCancelled
        {
            get { return Status == BTaskStatusType.Cancelled; }
        }
        public bool IsSucceeded
        {
            get { return Status == BTaskStatusType.Succeeded; }
        }
        public bool IsFailed
        {
            get { return Status == BTaskStatusType.Failed; }
        }        

        #endregion

        #region run and cancel

        public void RunAsync()
        {
            lock (this)
            {
                if (IsRunning) return;
                if (IsSucceeded) return;

                this.Status = BTaskStatusType.Running;
            }

            System.Threading.Tasks.Task.Run(() => { Run(); });
        }

        protected abstract void Run();

        #endregion
        
        /// <summary>
        /// 取消当前任务
        /// </summary>
        public virtual void Cancel()
        {
            this.Status = BTaskStatusType.Cancelled;

            if (Cancelled != null)
                Cancelled(this, null);
        }

        public event EventHandler Cancelled;

        #region task progress

        private double __progress;
        public double Progress
        {
            get { return __progress; }
            protected set
            {
                if (__progress != value)
                {
                    __progress = value;
                    OnNotifyPropertyChanged("Progress");
                    OnNotifyPropertyChanged("ProgressInt");
                }
            }
        }

        public int ProgressInt { get { return Convert.ToInt32(Progress); } }

        #endregion
    }
}
