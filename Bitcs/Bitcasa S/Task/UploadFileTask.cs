﻿using BitcasaSDK_WP.BObj;
using BitcasaSDK_WP.BRequest;
using BitcasaSDK_WP.Parameter;
using BSetting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Task
{
    public class UploadFileTask : BytesTask<BFileSystem, object>
    {
        public UploadFileTask(
            string dir_path,
            Stream stream, string filename)
            : base()
        {
#if DEBUG
            if (dir_path == null)
                throw new ArgumentNullException();
            if (String.IsNullOrWhiteSpace(filename))
                throw new ArgumentException();
            if (stream == null)
                throw new ArgumentNullException();
            if (!stream.CanRead)
                throw new ArgumentException();
#endif

            this.Path = dir_path;
            this.FileName = filename;
            this.Stream = stream;
            this.__totalBytes = stream.Length;
        }

        #region member

        public string Path { get; private set; }

        public string FileName { get; private set; }

        private Stream Stream;

        #endregion

        private long __totalBytes;
        public override long TotalBytes
        {
            get { return __totalBytes; }
        }

        protected override void Run()
        {
            ProgressParameter<BFileSystem> parameter =
                new ProgressParameter<BFileSystem>();

            parameter.Callback_Successed = (fs) =>
                {
#if DEBUG
                    //throw new NotImplementedException();
#endif
                    OnTaskSucceeded(fs);
                };
            parameter.Callback_Failed = (e) =>
                {
#if DEBUG
                    //throw new NotImplementedException();
#endif
                    OnTaskFailed(e);
                };
            parameter.Callback_BytesChanged = (b) =>
                {
                    this.CurrentBytes = b;
                };

            AppCore.Client.UploadFile(
                this.Path,
                this.FileName,
                this.Stream,
                parameter,
                (ExistsStatus)Setting.UploadOption.UploadOptionExistsMode);
        }

        public override string RunningText
        {
            get { return "uploading"; }
        }
    }
}
