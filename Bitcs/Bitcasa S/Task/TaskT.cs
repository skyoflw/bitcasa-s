﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP;
using Bitcasa_S.Packet;

namespace Bitcasa_S.Task
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TSucceeded">成功时回调的参数</typeparam>
    /// <typeparam name="TFailed">失败时回调的参数</typeparam>
    public abstract class Task<TSucceeded, TFailed> : BTask
    {
        protected virtual void OnTaskSucceeded(TSucceeded args)
        {
            if (IsCancelled)
            {
                OnTaskCancelled();
                return;
            }

            this.Status = BTaskStatusType.Succeeded;

            if (TaskSucceeded != null) TaskSucceeded(this, args);

            OnTaskCompleted();
        }
        public event EventHandler<TSucceeded> TaskSucceeded;

        protected virtual void OnTaskFailed(TFailed args)
        {
            if (IsCancelled)
            {
                OnTaskCancelled();
                return;
            }

            this.Status = BTaskStatusType.Failed;

            if (TaskFailed != null) TaskFailed(this, args);

            OnTaskCompleted();
        }
        public event EventHandler<TFailed> TaskFailed;

        protected virtual void OnTaskCancelled()
        {
            this.Status = BTaskStatusType.Cancelled;

            if (TaskCancelled != null) TaskCancelled(this, null);

            OnTaskCompleted();
        }
        public event EventHandler TaskCancelled;

        protected virtual void OnTaskCompleted()
        {
            if (TaskCompleted != null) TaskCompleted(this, null);

            this.TaskSucceeded = null;
            this.TaskFailed = null;
            this.TaskCancelled = null;
            this.TaskCompleted = null;
        }
        public event EventHandler TaskCompleted;
    }
}
