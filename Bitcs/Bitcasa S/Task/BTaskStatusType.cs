﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S.Task
{
    public enum BTaskStatusType
    {
        IsWaitForRun,

        Running,

        Cancelled,

        Succeeded,

        Failed
    }
}
