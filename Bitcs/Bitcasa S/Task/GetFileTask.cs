﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK_WP.BObj;
using System.IO.IsolatedStorage;
using System.IO;
using BitcasaSDK_WP;
using BitcasaSDK_WP.Parameter;
using Bitcasa_S.Packet;
using BDll;
using BCache;
using BDB2;
using BitcasaSDK_WP.BException;

namespace Bitcasa_S.Task
{
    public class GetFileTask : BytesTask<CacheFile, BitcasaException>
    {
        public GetFileTask(BFileSystem fs)
        {
            if (fs == null)
                throw new ArgumentNullException("fs can not be null.");

            this.File = fs;

            if (this.File.IsFolder) throw new ArgumentException();
        }

        #region show member

        public string ShowName { get { return this.File.Name; } }

        public override long TotalBytes
        {
            get { return this.File.Size; }
        }

        #endregion

        #region member

        public BFileSystem File;

        public CacheFile CacheFile;

        private Stream CurrentStream;

        #endregion

        protected override void Run()
        {
            if (CacheFile == null)
            {
                CacheFile = CFDB.GetOrCreate(this.File.Id, this.File.Name);

                if (CacheFile.Status == CacheStatus.Created)
                {
                    CacheFile.FileSize = File.Size;
                    CacheFile.Status = CacheStatus.NotBegin;
                    CacheFile.UpdateToDB();
                }
            }

            #region access 访问

            CacheFile.AccessTime = DateTime.UtcNow;

            long streamlength = 0;
            if (CacheFile.IsExist())
            {
                bool completed = CacheFile.Status == CacheStatus.Completed;

                if (!completed)
                {
                    CurrentStream = CacheFile.CreateOrOpenFile();
                    streamlength = CurrentStream.Length;
                    completed = streamlength == CacheFile.FileSize;
                    if (completed)
                    {
                        CacheFile.Status = CacheStatus.Completed;
                        CurrentStream.Close();
                    }
                    else
                    {
                        CurrentStream.Position = streamlength;
                        CurrentBytes = streamlength;
                    }
                }

                if (completed)
                {
                    tempToCache(CacheFile);
                    return;
                }
            }
            else
                CurrentStream = CacheFile.CreateOrOpenFile(CacheType.Temp);

            #endregion

            #region modify

            CacheFile.ModifyTime = DateTime.UtcNow;
            CacheFile.Status = CacheStatus.InCompleted;

            var parameter = new ProgressParameter<Stream>();

            parameter.Callback_Successed = (a) =>
                {
                    try { CurrentStream.Close(); } catch { }
                    CacheFile.ModifyTime = DateTime.UtcNow;
                    CacheFile.MoveFile(CacheType.Cache, this.File.Name);
                    CacheFile.Status = CacheStatus.Completed;
                    
                    OnTaskSucceeded(CacheFile);
                };
            parameter.Callback_Failed = (e) =>
                {
                    CacheFile.Status = CacheStatus.InCompleted;

                    OnTaskFailed(e);
                };
            parameter.Callback_Cancelled = () =>
                {
                    CacheFile.Status = CacheStatus.InCompleted;

                    OnTaskCancelled();
                };
            parameter.Callback_Completed = () =>
                {
                    try { CurrentStream.Close(); }
                    catch { }
                };
            parameter.CancelToken = this;
            parameter.Callback_BytesChanged = (l) =>
                {
                    CurrentBytes = streamlength + l;
                };


            if (CurrentStream.CanWrite)
                AppCore.Client.DownloadFile(
                    this.File.Path64, this.File.Name, CurrentStream,
                    parameter, File.Size, streamlength);

            #endregion
        }

        public override void Cancel()
        {
            try { CurrentStream.Close(); }
            catch { }

            if (CacheFile != null) CacheFile.UpdateToDB();

            base.Cancel();
        }

        /// <summary>
        /// 下载完成后可以调用此方法，此方法会触发 OnTaskSucceeded() 函数
        /// </summary>
        /// <param name="cf"></param>
        private void tempToCache(CacheFile cf)
        {
            if (cf.Type == CacheType.Temp)
            {
                cf.MoveFile(CacheType.Cache, this.File.Name);
                cf.UpdateToDB();
            }
            OnTaskSucceeded(cf);
        }

        public override string RunningText
        {
            get { return "downloading"; }
        }

        protected override void OnTaskCompleted()
        {
            try { CurrentStream.Close(); }
            catch { }

            if (CacheFile != null) CacheFile.UpdateToDB();

            base.OnTaskCompleted();
        }
    }
}
