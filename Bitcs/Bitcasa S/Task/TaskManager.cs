﻿using Bitcasa_S.Packet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bitcasa_S.Task
{
    public class TaskManager<T, TTaskS, TTaskF> : CNotifyPropertyChanged
        where T : Task<TTaskS, TTaskF>
    {
        private const int MaxRunCountInSameTime = 1;

        public TaskManager()
        {
            status = RunStatus.Stop;

            __taskList = new ObservableCollection<T>();
            WaitingQueue = new Queue<T>();
            RunningList = new List<T>();
            SucceededList = new List<T>();
            FailedList = new List<T>();
            CancelledList = new List<T>();
        }

        #region list column

        private ObservableCollection<T> __taskList;
        /// <summary>
        /// 显示列表
        /// </summary>
        public ObservableCollection<T> TaskList
        {
            get { return __taskList; }
            private set
            {
                if (__taskList != value)
                {
                    __taskList = value;
                    OnNotifyPropertyChanged("TaskList");
                }
            }
        }

        /// <summary>
        /// 等待队列
        /// </summary>
        private Queue<T> WaitingQueue { get; set; }
        /// <summary>
        /// 运行中列表
        /// </summary>
        private List<T> RunningList { get; set; }
        /// <summary>
        /// 成功列表
        /// </summary>
        private List<T> SucceededList { get; set; }
        /// <summary>
        /// 失败列表
        /// </summary>
        private List<T> FailedList { get; set; }
        /// <summary>
        /// 取消列表
        /// </summary>
        private List<T> CancelledList { get; set; }

        #endregion

        #region run

        public void RunAsync()
        {
            lock (this)
            {
                if (IsRunning) return;
                this.status = RunStatus.Running;
            }

            System.Threading.Tasks.Task.Run(() => { Run(); });
        }
        private void Run()
        {
            CheckList();
        }

        private RunStatus status;
        public RunStatus Status
        {
            get { return status; }
            set
            {
                if (status != value)
                {
                    status = value;
                    OnRunStatusChanged(value);
                    OnNotifyPropertyChanged("IsRunning");
                }
            }
        }

        public bool IsRunning
        {
            get { return Status == RunStatus.Running; }
        }

        private void OnRunStatusChanged(RunStatus newStatus)
        {
            if (RunStatusChanged != null)
                RunStatusChanged(this, newStatus);
        }
        /// <summary>
        /// 运行状态改变
        /// </summary>
        public event EventHandler<RunStatus> RunStatusChanged;

        #endregion

        public void AddTask(T task)
        {
            task.TaskSucceeded += (a, b) =>
            {
                lock (SucceededList) SucceededList.Add(task);
            };

            task.TaskFailed += (a, b) =>
            {
                lock (FailedList) FailedList.Add(task);
            };

            task.TaskCancelled += (a, b) =>
            {
                lock (CancelledList) CancelledList.Add(task);
            };

            task.TaskCompleted += (a, b) =>
            {
                lock (RunningList) RunningList.Remove(task);

                CheckList();
            };

            App.UIInvoke(false, () => { lock (TaskList) TaskList.Add(task); });

            lock (WaitingQueue)
                WaitingQueue.Enqueue(task);

            if (!IsRunning)
                RunAsync();
            else
                CheckList();
        }

        private void CheckList()
        {
            if (!IsRunning) return;

            T task = default(T);

            lock (RunningList)
            {
                if (RunningList.Count >= MaxRunCountInSameTime) return;

                lock (WaitingQueue)
                {
                    if (WaitingQueue.Count <= 0)
                        this.Status = RunStatus.Stop;
                    else
                    {
                        task = WaitingQueue.Dequeue();

                        RunningList.Add(task);
                    }
                }
            }

            if (task != null) task.RunAsync();
        }
    }

    public enum RunStatus
    {
        Running,

        Stop
    }
}
