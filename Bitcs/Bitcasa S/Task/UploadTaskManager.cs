﻿using Bitcasa_S.Resources;
using BitcasaSDK_WP.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Bitcasa_S.Task
{
    public class UploadTaskManager : TaskManager<UploadFileTask, BFileSystem, object>
    {
        /// <summary>
        /// 此方法仅允许 UI 线程调用！
        /// <para/>若返回 true，表示强制退出不等待上传，否则等待上传
        /// </summary>
        /// <returns>若返回 true，表示强制退出不等待上传，否则等待上传</returns>
        public bool CheckUploading()
        {
            if (this.IsRunning)
            {
                return (MessageBox.Show(AppResources.UploadTaskManager_UploadingQuitMessageText,
                    AppResources.MessageBox_Caption_Warning,
                    MessageBoxButton.OKCancel) == MessageBoxResult.OK);
            }
            return true;
        }
    }
}
