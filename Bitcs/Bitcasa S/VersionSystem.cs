﻿using BCache;
using BSetting;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitcasa_S
{
    public class VersionSystem
    {
        /// <summary>
        /// 当前版本号。
        /// n 表示执行 [setting, 1) 索引位置的 Action
        /// </summary>
        private const int CurrentVersion = 1;

        /// <summary>
        /// 检查版本。
        /// <para/>应用启动时调用。
        /// </summary>
        public static void CheckVersion()
        {
            if (Setting.LastVersion != CurrentVersion)
            {
                var source = VersionOperation();
                var list = source.Skip(Setting.LastVersion).ToList();

                VersionContextOperation = () =>
                {
                    foreach (var action in list)
                    {
                        if (action()) Setting.LastVersion++;
                        else break;
                    }
                };
            }
        }

        private static List<Func<bool>> VersionOperation()
        {
            return new List<Func<bool>>()
                {
                    () => // ver 1. clear old database & cache directory
                    {
                        using (var store7 = IsolatedStorageFile.GetUserStoreForApplication())
                        {
                            if (store7.FileExists("BS.sdf"))
                            {
                                store7.DeleteFile("BS.sdf");
                                CacheFilesHelper.ClearFolder("Temp");
                                CacheFilesHelper.ClearFolder("Cache");

                                return true;
                            }
                            else
                                return true;
                        }
                    }
                };
        }

        /// <summary>
        /// 运行版本升级代码。
        /// </summary>
        public static void Run()
        {
            if (VersionContextOperation != null) VersionContextOperation();
        }

        /// <summary>
        /// 获取或设置相关方法。
        /// <para/>如果不为 null，则应执行。
        /// </summary>
        private static Action VersionContextOperation { get; set; }
    }
}
