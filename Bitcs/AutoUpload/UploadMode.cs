﻿
namespace AutoUpload
{
    public enum UploadMode : int
    {
        AllPhoto = 0,

        OnlyCameraRoll = 1
    }
}
