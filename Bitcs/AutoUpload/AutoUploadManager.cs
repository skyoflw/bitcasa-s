﻿using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BitcasaSDK_WP;
using BitcasaSDK_WP.Parameter;
using BitcasaSDK_WP.BObj;
using BSetting;
using BitcasaSDK_WP.BRequest;
using BitcasaSDK_WP.BJson;

namespace AutoUpload
{
    public class AutoUploadManager
    {
        public AutoUploadManager(
            BitcasaClient client, 
            UploadMode mode,
            string dir_path_base64)
        {
            #region config
#if DEBUG
            if (client == null || dir_path_base64 == null)
                throw new ArgumentNullException();
#endif
            this.Client = client;
            this.Mode = mode;
            this.FolderPathBase64 = dir_path_base64;

            #endregion

            #region init tag

            this.IsRunning = false;
            this.IsDisable = false;

            #endregion
        }

        private BitcasaClient Client;

        public UploadMode Mode { get; private set; }

        public string FolderPathBase64 { get; private set; }

        public ExistsStatus ExistsMode { get; set; }

        public void RunAsync() { Task.Run(() => { Run(); }); }
        private void Run()
        {
            lock (this)
            {
                if (IsRunning) return;

                IsRunning = true;
            }

            if (!this.Client.IsLogin)
            {
                OnManagerStop(StopStatus.NotLogin);
                return;
            }

            if (this.FolderPathBase64 == null)
            {
                OnManagerStop(StopStatus.NoPath);
                return;
            }

            List<Picture> c, o;
            GetPhotoList(out c, out o);

            DateTime dt_c, dt_o;


            if (GetLastUploadDate(out dt_c, true)) c = c.Where(p => { return p.Date > dt_c; }).ToList();
            c.Sort((a, b) => { return a.Date.CompareTo(b.Date); });
            CameraRollWaitingList = new Queue<Picture>(c);

            if (Mode == UploadMode.AllPhoto)
            {
                if (GetLastUploadDate(out dt_o, false)) o = o.Where(p => { return p.Date > dt_o; }).ToList();
                o.Sort((a, b) => { return a.Date.CompareTo(b.Date); });
                OtherWaitingList = new Queue<Picture>(o);
            }
            
            CheckQueue();
        }

        public bool IsDisable { get; private set; }
        public void DisableAysnc()
        {
            if (IsDisable) return;
            IsDisable = true;
        }

        private void CheckQueue()
        {
            if (IsDisable)
            {
                OnManagerStop(StopStatus.Disable);
                return;
            }

            if (CameraRollWaitingList.Count > 0)
            {
                UploadEachOne(CameraRollWaitingList.Dequeue(), true);
            }
            else if (OtherWaitingList != null)
            {
                if (OtherWaitingList.Count > 0)
                    UploadEachOne(OtherWaitingList.Dequeue(), false);
            }
            else
                OnManagerStop(StopStatus.Completed);
        }

        #region upload one

        private const int MAX_TRY_TIME = 3;

        int TryTime = 0;

        private Dictionary<string, string> AlbumPath64 = new Dictionary<string, string>();

        private void UploadEachOne(Picture p, bool isCameraRoll)
        {
            TryTime = 0;

            if (AlbumPath64.ContainsKey(p.Album.Name))
                UploadEachOne(p, AlbumPath64[p.Album.Name], isCameraRoll);
            else
                CreateFolder(p, isCameraRoll);
        }

        private void UploadEachOne(Picture p, string path64, bool isCameraRoll)
        {
            if (AlbumPath64.ContainsKey(p.Album.Name))
            {
                var parameter = new ProgressParameter<BFileSystem>()
                {
                    Callback_Successed = f =>
                    {
                        UpdateLastUploadDate(p.Date, isCameraRoll);
                    },
                    Callback_Failed = e =>
                    {
                        TryTime++;
                        if (TryTime < MAX_TRY_TIME)
                            UploadEachOne(p, path64, isCameraRoll);
                        else
                            OnManagerStop(StopStatus.FailedAtOne);
                    }
                };
                var name = System.IO.Path.GetFileName(p.Name);
                if (name.IndexOf('.') < 0) name = name + ".jpg";
                this.Client.UploadFile(
                    path64,
                    name,
                    p.GetImage(),
                    parameter,
                    ExistsMode);
            }
        }

        private void CreateFolder(Picture p, bool isCameraRoll)
        {
            string name = p.Album.Name;

            var parameter = new BParameter<List<BFileSystemOperationResult>>();

            parameter.Callback_Successed = (l) =>
            {
                string p64 = null;
                
                foreach (var i in l)
                {
                    if (i.Name == name)
                        p64 = i.Path64;
                }

                if (p64 != null)
                {
                    AlbumPath64[name] = p64;
                    UploadEachOne(p, isCameraRoll);
                }
                else
                {
                    TryTime++;

                    if (TryTime < MAX_TRY_TIME)
                        CreateFolder(p, isCameraRoll);
                    else
                        OnManagerStop(StopStatus.CanNotCreateFolder);
                }
            };
            parameter.Callback_Failed = (e) =>
            {
                TryTime++;

                if (TryTime < MAX_TRY_TIME)
                    CreateFolder(p, isCameraRoll);
                else
                    OnManagerStop(StopStatus.CanNotCreateFolder);
            };

            Client.AddFolder(this.FolderPathBase64, name, parameter);
        }

        #endregion

        #region tag

        /// <summary>
        /// 确定管理器是否在运行
        /// </summary>
        public bool IsRunning { get; private set; }

        #endregion

        #region queue

        private Queue<Picture> CameraRollWaitingList;

        private Queue<Picture> OtherWaitingList;

        #endregion

        #region event

        private void OnManagerStop(StopStatus code)
        {
            this.IsRunning = false;

            if (ManagerStop != null)
                ManagerStop(this, code);
        }
        
        public event EventHandler<StopStatus> ManagerStop;

        public enum StopStatus : int
        {
            /// <summary>
            /// 无法创建目录
            /// </summary>
            CanNotCreateFolder,

            /// <summary>
            /// 由于不存在目标 Path64 导致无法上传的异常
            /// </summary>
            NoPath = -2,

            /// <summary>
            /// 用户尚未登录导致无法上传的异常
            /// </summary>
            NotLogin = -1,

            /// <summary>
            /// 上传完成
            /// </summary>
            Completed = 0,

            /// <summary>
            /// 终止于某次上传失败
            /// </summary>
            FailedAtOne = 1,

            /// <summary>
            /// 用户手动关闭
            /// </summary>
            Disable = 2
        }

        #endregion

        #region last upload time

        private void UpdateLastUploadDate(DateTime date, bool is_camera_roll)
        {
            if (is_camera_roll)
                Setting.LastAutoUploadCameraRollPhotoTime = date;
            else
                Setting.LastAutoUploadOtherPhotoTime = date;

            Setting.AllUploadPhotoCount++;

            CheckQueue();
            //ProgressParameter<BFileSystem> parameter =
            //    new ProgressParameter<BFileSystem>()
            //    {
            //        Callback_Successed = (f) => { CheckQueue(); },
            //        Callback_Failed = (e) => { OnManagerStop(); }
            //    };
            //throw new NotImplementedException();
        }

        /// <summary>
        /// 获取上次上传照片的最后一张照片的日期
        /// 返回一个值指示是否成功获取
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool GetLastUploadDate(out DateTime date, bool is_camera_roll)
        {
            DateTime d;
            if (is_camera_roll)
                d = Setting.LastAutoUploadCameraRollPhotoTime;
            else
                d = Setting.LastAutoUploadOtherPhotoTime;

            date = d;

            if (d == Setting.DefaultTime)
                return false;
            else
                return true;
        }

        #endregion

        #region get picture

        /// <summary>
        /// 查找所有照片
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static void GetPhotoList(out List<Picture> camera_roll, out List<Picture> other)
        {
            camera_roll = new List<Picture>();
            other = new List<Picture>();
            List<Picture> result = new List<Picture>();
            using (var lib = new MediaLibrary())
            {
                foreach (var a in lib.RootPictureAlbum.Albums)
                    if (CameraRollAlbumName == a.Name) camera_roll.AddRange(a.Pictures);
                    else other.AddRange(a.Pictures);
            }
        }

        private const string CameraRollAlbumName = "Camera Roll";

        #endregion
    }
}
