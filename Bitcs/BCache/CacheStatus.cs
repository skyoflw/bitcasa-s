﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCache
{
    public enum CacheStatus : int
    {
        /// <summary>
        /// 刚创建
        /// </summary>
        Created = -1,

        /// <summary>
        /// 下载还未开始，但是关键数据已经配置完成
        /// </summary>
        NotBegin = 0,

        /// <summary>
        /// 下载失败文件损坏（保留）
        /// </summary>
        Break = 1,

        /// <summary>
        /// 未完成
        /// </summary>
        InCompleted = 2,

        /// <summary>
        /// 已完成
        /// </summary>
        Completed = 3,
    }
}
