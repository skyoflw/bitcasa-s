﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.Data.Linq.Mapping;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using BitcasaSDK_WP;
using Windows.Storage;
using Windows.System;
using System.IO;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Linq;
using BitcasaSDK_WP.BObj;

namespace BCache
{
    [Table(Name = "CacheFiles")]
    [Index(Columns = "ObjId", IsUnique = true)]
    public class CacheFile : INotifyDataChanged<CacheFile>
    {
        /// <summary>
        ///  仅供数据库实例化调用，任何时候都不应该手动调用。
        /// </summary>
        public CacheFile()
        {
        }
        /// <summary>
        /// 需要注意的是，创建实例时，硬链接不会自动置为 1。
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="filename"></param>
        public CacheFile(string objId)
            : this()
        {
            this.Id = Guid.NewGuid();
            this.ObjId = objId;

            this.CreateTime = DateTime.UtcNow;
            this.AccessTime = DateTime.UtcNow;
            this.ModifyTime = DateTime.UtcNow;
        }

        /// <summary>
        /// [Column(Name = "Id", IsPrimaryKey = true)]
        /// 此项在任何时候都不应该改变
        /// </summary>
        [Column(Name = "Id", IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// [Column(Name = "ObjId", CanBeNull = false)]
        /// 关联对象的 Id
        /// 用来检测文件是否变更要重新下载
        /// 此项在任何时候都不应该改变
        /// </summary>
        [Column(Name = "ObjId", CanBeNull = false)]
        public string ObjId { get; set; }

        /// <summary>
        /// [Column(Name = "LinkCounter")]
        /// 硬链接计数器
        /// </summary>
        [Column(Name = "LinkCounter")]
        public int LinkCounter { get; set; }

        #region time

        private DateTime __accessTime;
        /// <summary>
        /// [Column(Name = "AccessTime")]
        /// 上次访问时间。
        /// 此项为 UTC 时间。
        /// </summary>
        [Column(Name = "AccessTime")]
        public DateTime AccessTime
        {
            get { return __accessTime.LinqTime(); }
            set { __accessTime = value; }
        }

        private DateTime __createTime;
        /// <summary>
        /// [Column(Name = "CreateTime")]
        /// 创建时间。
        /// 此项为 UTC 时间。
        /// </summary>
        [Column(Name = "CreateTime")]
        public DateTime CreateTime
        {
            get { return __createTime.LinqTime(); }
            set { __createTime = value; }
        }

        public DateTime __modifyTime;
        /// <summary>
        /// [Column(Name = "ModifyTime")]
        /// 修改文件时间，每次下载时会更新。
        /// 此项为 UTC 时间。
        /// </summary>
        [Column(Name = "ModifyTime")]
        public DateTime ModifyTime
        {
            get { return __modifyTime.LinqTime(); }
            set { __modifyTime = value; }
        }

        #endregion

        #region

        /// <summary>
        /// [Column(Name = "FilePath", CanBeNull = false)]
        /// 文件在本地的路径。此项不允许为 null，但是不能确定文件是否存在。
        /// </summary>
        [Column(Name = "FilePath", CanBeNull = false)]
        public string FilePath { get; set; }

        /// <summary>
        /// [Column(Name = "FileSize")]
        /// 文件总大小
        /// </summary>
        [Column(Name = "FileSize")]
        public long FileSize { get; set; }

        /// <summary>
        /// [Column(Name = "Status")]
        /// 文件的当前状态
        /// </summary>
        [Column(Name = "Status")]
        public CacheStatus Status { get; set; }

        /// <summary>
        /// [Column(Name = "Type")]
        /// 文件的类型
        /// </summary>
        [Column(Name = "Type")]
        public CacheType Type { get; set; }

        public void Update(CacheFile cf)
        {
            // time
            this.AccessTime = cf.AccessTime;
            this.ModifyTime = cf.ModifyTime;
            this.CreateTime = cf.CreateTime;

            // status
            this.FileSize = cf.FileSize;
            this.Status = cf.Status;
            this.Type = cf.Type;

            // file tag
            this.FilePath = cf.FilePath;
        }

        #endregion

        /// <summary>
        /// 获取当前 FilePath 匹配的文件名
        /// </summary>
        public string FileName { get { return Path.GetFileName(this.FilePath); } }

        /// <summary>
        /// 获取当前目录名
        /// </summary>
        public string Directory { get { return System.IO.Path.GetDirectoryName(this.FilePath); } }

        public event EventHandler DataChanged;
    }
}
