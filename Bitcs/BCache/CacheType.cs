﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCache
{
    public enum CacheType : int
    {
        Temp = 0,
        Thumbnail = 1,
        Cache = 2,
        Favorite = 3,
        BackgroundTransfer = 4,
    }
}
