﻿using BitcasaSDK_WP.BObj;
using BitcasaSDK_WP;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.System;

namespace BCache
{
    public static class CacheFilesHelper
    {
        #region gen path

        /// <summary>
        /// 获取 CacheType 对应的目录名
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string GetDirectoryName(this CacheType type)
        {
            return ((int)type).ToString();
        }
        /// <summary>
        /// 返回四级目录，顺序从子目录排起
        /// 如文件 {8D171DAD-4267-4D22-8CBD-E28AA14B1E11}
        /// 返回值排序如下：
        /// （不确定是 \ 还是 / ... (= =)!!!）
        /// 
        /// type\8D\17\8D171DAD-4267-4D22-8CBD-E28AA14B1E11;
        /// type\8D\17;
        /// type\8D;
        /// type;
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string[] GetDirectoryName(this CacheFile file, CacheType type)
        {
            List<string> list = new List<string>();

            string guid = file.Id.ToString().ToUpper();

            var dir1 = type.GetDirectoryName();
            var dir2 = Path.Combine(dir1, guid.Substring(0, 2));
            var dir3 = Path.Combine(dir2, guid.Substring(2, 2));
            var dir4 = Path.Combine(dir3, guid);

            list.Add(dir4);
            list.Add(dir3);
            list.Add(dir2);
            list.Add(dir1);

            return list.ToArray();
        }
        /// <summary>
        /// 为此对象生成于 type 中文件名为 fn 的路径
        /// </summary>
        /// <param name="cf"></param>
        /// <param name="type"></param>
        /// <param name="fn"></param>
        /// <returns></returns>
        public static string GenerateFilePath(this CacheFile cf, CacheType type, string fn)
        {
            return Path.Combine(
                cf.GetDirectoryName(type).First(),
                fn);
        }

        #endregion

        #region create or delete directory

        /// <summary>
        /// 尝试迭代创建目录，如果目录存在，则不作操作
        /// </summary>
        /// <param name="file"></param>
        public static void TryCreateDirectory(this CacheFile file)
        {
            file.TryCreateDirectory(file.Type);
        }
        /// <summary>
        /// 尝试迭代创建目录，如果目录存在，则不作操作
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        public static void TryCreateDirectory(this CacheFile file, CacheType type)
        {
            var dirs = file.GetDirectoryName(type);
            foreach (var dir in dirs.Reverse())
                if (!Store7.DirectoryExists(dir))
                    Store7.CreateDirectory(dir);
        }
        /// <summary>
        ///  如果没有文件，尝试递归删除目录
        /// </summary>
        /// <param name="file"></param>
        public static void TryDeleteDirectory(this CacheFile file)
        {
            file.TryDeleteDirectory(file.Type);
        }
        /// <summary>
        /// 如果没有文件，尝试递归删除目录
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        public static void TryDeleteDirectory(this CacheFile file, CacheType type)
        {
            var dirs = file.GetDirectoryName(type);

            foreach (var dir in dirs)
                if (Store7.DirectoryExists(dir))
                {
                    var p = Path.Combine(dir, "*");

                    if (Store7.GetFileNames(p).Length > 0) return;
                    if (Store7.GetDirectoryNames(p).Length > 0) return;

                    Store7.DeleteDirectory(dir);
                }
        }

        #endregion

        #region clear

        /// <summary>
        /// 递归清空一个目录下所有东西，如果目录不存在返回 true
        /// </summary>
        /// <param name="folderpath"></param>
        /// <returns></returns>
        public static bool ClearFolder(string folderpath)
        {
            try
            {
                if (!Store7.DirectoryExists(folderpath))
                    return true;

                string p = Path.Combine(folderpath, "*");

                // folder
                var dirs = Store7.GetDirectoryNames(p);
                foreach (string d in dirs)
                    ClearFolder(Path.Combine(folderpath, d));

                // file
                var files = Store7.GetFileNames(p);
                foreach (string file in files)
                    Store7.DeleteFile(Path.Combine(folderpath, file));

                Store7.DeleteDirectory(folderpath);
                return true;
            }
            catch { return false; }
        }
        /// <summary>
        /// 递归清空所有目录下所有东西，如果目录不存在返回 true
        /// </summary>
        /// <param name="folderpath"></param>
        /// <returns></returns>
        public static bool ClearAllFolder()
        {
            var all = Enum.GetValues(typeof(CacheType)) as CacheType[];

            bool result = true;

            foreach (var o in all)
                result &= ClearFolder(o.GetDirectoryName());

            return result;
        }

        #endregion

        /// <summary>
        /// 打开文件，如果文件不存在会自动创建
        /// 如果文件路径为空会自动填充
        /// 如果目录不存在会自动创建
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static IsolatedStorageFileStream CreateOrOpenFile(
            this CacheFile file)
        {
            return file.CreateOrOpenFile(file.Type);
        }
        /// <summary>
        /// 打开文件，如果文件不存在会自动创建
        /// 如果文件路径为空会自动填充
        /// 如果目录不存在会自动创建
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IsolatedStorageFileStream CreateOrOpenFile(
            this CacheFile file, CacheType type)
        {
            if (file.FilePath.CheckNotNull()) return null;

            var result = OpenFile(file);
            if (result != null)
                return result;

            file.TryCreateDirectory(type);

            return Store7.CreateFile(file.FilePath);
        }

        /// <summary>
        /// 打开文件，如果文件不存在，返回 null
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static IsolatedStorageFileStream OpenFile(this CacheFile file)
        {
            if (!file.IsExist()) return null;

            return Store7.OpenFile(file.FilePath, FileMode.Open);
        }

        /// <summary>
        /// 如果成功，会自动将对象的 CacheType 设置为 type
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool MoveFile(this CacheFile file, CacheType type, string newFileName)
        {
#if DEBUG
            if (!file.IsExist())
                throw new ArgumentException();
#else
            if (!file.IsExist()) return false;
#endif
            file.TryCreateDirectory(type);

            var fp = file.GenerateFilePath(type, newFileName);

            if (file.FilePath == fp) return true;

            if (Store7.FileExists(fp))
                return false;
            else
            {
                Store7.MoveFile(file.FilePath, fp);
                file.TryDeleteDirectory(file.Type);
                file.Type = type;
                file.FilePath = fp;
                return true;
            }
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteFile(this CacheFile file)
        {
            if (!file.IsExist()) return;

            Store7.DeleteFile(file.FilePath);
            file.TryDeleteDirectory(file.Type);
        }
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteFileAsync(this IEnumerable<CacheFile> files)
        {
            Task.Run(() =>
                {
                    foreach (var cf in files)
                        cf.DeleteFile();
                });
        }

        public static bool IsExist(this CacheFile file)
        {
            if (file.FilePath == null) return false;
            else return Store7.FileExists(file.FilePath);
        }

        /// <summary>
        /// 运行文件并更新 AccessTime
        /// </summary>
        /// <returns></returns>
        public static bool LaunchFile(this CacheFile file)
        {
#if DEBUG
            if (!file.IsExist()) throw new ArgumentException();
#else
            if (!file.IsExist()) return false;
#endif
            file.AccessTime = DateTime.UtcNow;
            LaunchFileAsync(file.FilePath);
            return true;
        }

        private static async void LaunchFileAsync(string path)
        {
            StorageFile bqfile = await Store8.GetFileAsync(path);
            await Launcher.LaunchFileAsync(bqfile);
        }

        private static readonly IsolatedStorageFile Store7 =
            IsolatedStorageFile.GetUserStoreForApplication();

        private static readonly StorageFolder Store8 =
            ApplicationData.Current.LocalFolder;
    }
}
