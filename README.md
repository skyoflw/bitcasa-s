# README #


## 上传列表

![](http://i.imgur.com/ljNEuEZ.png)

### 环形进度条实现

* [gist](https://gist.github.com/Cologler/a136750c8f5b54070916317a6f0cd03f)

### 其它说明：

1. 长按项目，如果当前为 uploading 状态，弹出 MessageBox 提示是否取消上传
2. 长按项目，如果当前为 completed 状态，自动删除任务
3. 短按项目，如果当前为 failed 状态，立即重试
4. 提供一键清空已完成条目
